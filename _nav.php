<?php
@session_start();
include_once("Model/Model.php");
include_once("Vistas/General.php");


$model = new Model();
$urlAgenteVirtual = $model->getParametroGeneral('urlAgenteVirtual', 'PORTAL USUARIO');
$r_social = $model->getDato('r_social', 'seriales', '');
$entidad = $_SESSION['entidad'];
$puntoat = $_SESSION["punto_atencion_pcte"];
$menu = $model->RSAsociativo("SELECT * from menu_portal_2 where  (('" . $entidad . "' = 'EPS037' or (" . $puntoat . " = 10 or " . $puntoat . " = 37 or " . $puntoat . " = 38)) and nueva_eps = 1) or ('" . $entidad . "' = 'RES004' and magisterio = 1 and (" . $puntoat . " <> 10 and " . $puntoat . " <> 37 and " . $puntoat . " <> 38))");
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sisma Portal</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <script defer src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="Js/jquery.1.9.1.min.js"></script>

  <!-- agregando -->
  <link href="Css/librerias/toastr.css" rel="stylesheet">
  <link href="Css/librerias/alertifyjs/alertify.css" rel="stylesheet">
  <link href="Css/librerias/alertifyjs/alertify.min.css" rel="stylesheet">
  <link href="Css/librerias/alertifyjs/alertify.rtl.css" rel="stylesheet">
  <link href="Css/librerias/alertifyjs/themes/default.css" rel="stylesheet">



  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css">
  <script defer src="https://use.fontawesome.com/releases/v5.15.3/js/all.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.15.3/js/v4-shims.js"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">


  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="Js/librerias/toastr.min.js"></script>

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <!-- <link href="css/style.css" rel="stylesheet"> -->

  <script type="text/javascript" src="Js/js.js"></script>
  <script type="text/javascript" src="Js/help.js"></script>
  <script type="text/javascript" src="Js/MyFunciones.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


  <script type="text/javascript" src="Js/librerias/moment.js"></script>
  <script type="text/javascript" src="Js/librerias/tempusdominus-bootstrap-4.min.js"></script>
  <link rel="stylesheet" href="Css/librerias/tempusdominus-bootstrap-4.min.css" />

</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">
        <i class="icofont-ui-touch-phone"></i> 305 734 1412
      </div>
      <div class="d-none d-lg-flex social-links align-items-center">
        <a href="https://mobile.twitter.com/medintegral_sa?lang=es" target="_blank" style="color: #FFF;"><i class="icofont-twitter"></i></a>
        <a href="https://www.facebook.com/empresasmedicinaintegral/" target="_blank" style="color: #FFF;"><i class="icofont-facebook"></i></a>
        <a href="https://instagram.com/empresasmedicinaintegral?utm_medium=copy_link" target="_blank" style="color: #FFF;"><i class="icofont-instagram"></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top" style="padding:14px 0px 14px 0px">
    <div class="container d-flex align-items-center">

      <a href="portal.php" class="mr-auto"><img src="Imagenes/LOGOS-MI.jpg" alt="" width="150px"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <?php
          foreach ($menu as $key => $m) {
            if ($m["mostrar_nav"] == 1) {
              if ($m["desplegable"] == 1) {
                $hijo = $model->RSAsociativo("SELECT * from menu_portal_2 where padre = " . $m["id"] . " and (('" . $entidad . "' = 'EPS037' and nueva_eps = 1) or ('" . $entidad . "' = 'RES004' and magisterio = 1))");
                echo ('
                    <li class="drop-down"><a href="' . $m["link"] . '">' . utf8_encode($m["titulo"]) . '</a>
                      <ul>
                  ');
                foreach ($hijo as $keys => $h) {
                  echo ('<li><a href="' . $h["link"] . '">' . utf8_encode($h["titulo"]) . '</a></li>');
                }
                echo ('
                      </ul>
                    </li>
                  ');
              } else {
                echo ('<li><a href="' . $m["link"] . '">' . utf8_encode($m["titulo"]) . '</a></li>');
              }
            }
          }
          ?>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2><b id="posicion" style="color:#555555"></b></h2>
          <ol>
            <li>
            </li>
          </ol>
        </div>

      </div>
    </section>

    <div class="modal fade" id="modalActPsw" tabindex="-1" style="border-radius:15px; z-index:1000000000;" role="dialog" aria-labelledby="modalActPswLabel" aria-hidden="true">
      <div class="modal-dialog" role="documen">
        <div class="modal-content" style="border-radius:15px;">
          <div class="modal-header" style="background-color: #00B1EB; border-radius:15px 15px 0px 0px;">
            <img id="imgSimbolo" src="Imagenes/simbolo.png" alt="">
            <h5 class="modal-title" style="color: #FFF;" id="modalActPswLabel">Cambiar contraseña</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="formActPsw" method="POST">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Contraseña actual:</label>
                    <input type="password" class="form-control form-control-sm" id="pswPaciente" name="pswPaciente" required>
                    <div class="invalid-feedback">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" id="divCambioPassword">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Nueva Contraseña:</label>
                    <input type="password" class="form-control form-control-sm" id="pwsCambio" name="pwsCambio" autocomplete="off" required>
                    <div class="invalid-feedback">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Confirmar Contraseña:</label>
                    <input type="password" class="form-control form-control-sm" id="pwsCambioConfirm" name="pwsCambioConfirm" numfmt_get_pattern="pwsCambioConfirm" autocomplete="off" required>
                    <div class="invalid-feedback">
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnAcñtualizarDatos" class="btn btn-portal2" onclick="fnActualizarPsw()">Actualizar</button>
          </div>
        </div>
      </div>
    </div>