<?php 
include_once('../Service.class.php');

class ZeusSaludLogsService extends Service{
	
	function __construct(Model $m){
		parent::__construct("dbo.spLogGeneral ", $m);
	}
	
	function generarLog($tabla, $columnaId, $valoresId, $detalleLog, $usuario, $tipoLog, $manejaLaTransaccion = 1){

		if(strtoupper($tipoLog) !== 'I' && strtoupper($tipoLog) !== 'U' && strtoupper($tipoLog) !== 'D'){
			throw new Exception('Tipo de log invalido. Datos permitidos: I (Insert); U (Update); D (Delete).');
		}
		
		$this->sp_params = '@Op = \'I\''
						.', @Tabla = N\''.$tabla.'\''
						.', @ColumnaId = N\''.$columnaId.'\''
						.', @TargetIds = N\''.$valoresId.'\''
						.', @DetalleLog = N\''.$detalleLog.'\''
						.', @UsuarioLog = '.(int)$usuario
						.', @TipoLog = \''.$tipoLog.'\''
						.', @manejaLaTransaccion = '.$manejaLaTransaccion;
		$statusTran = $this->execSp();
		
		if(trim($statusTran) != ''){
			throw new Exception($statusTran);
		}
		
		return true;
	}
}
?>