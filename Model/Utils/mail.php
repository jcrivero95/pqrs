<?php
include_once($_SESSION["root_vacuna"]."/Model/Model.php");
include_once("class.phpmailer.php");
include_once("class.smtp.php");

class Mail{
	
	var $DESTINATARIO;
	var $ASUNTO;
	var $MENSAJE;
	var $CC;
	var $CCO;
	var $REMITENTE;
	var $NOMBRE_REMITENTE;
	var $ADJUNTOS=array();
	
	function Mail(){
		$model=new Model();
		$this->ASUNTO=$model->getParametroGeneral('asunto_email','CORREO');
	}
	
	function sendMail($devolverMensaje = false){
		 $model=new Model();
		 $mail = new PHPMailer(); 
		 $mail->IsSMTP();
		 $mail->SMTPAuth = true; 
		 $mail->SMTPSecure = $model->getParametroGeneral('smtp_secure','CORREO');
		 $mail->Host = $model->getParametroGeneral('host_smtp','CORREO');
		 $mail->Port = $model->getParametroGeneral('puerto','CORREO'); 
		 $mail->Username = $model->getParametroGeneral('mail_from','CORREO'); 
		 $mail->Password = $model->getParametroGeneral('pass_mail_from','CORREO'); 
		 $mail->remitente = $model->getParametroGeneral('mail_remitente','CORREO'); 

		 $mail->From = $model->getParametroGeneral('mail_from','CORREO'); 
		 if(trim($this->NOMBRE_REMITENTE)==''){
		 	$mail->FromName = $model->getParametroGeneral('nombre_remitente','CORREO'); 
		 }else{
			$mail->FromName = $this->NOMBRE_REMITENTE;
		 }
		 if($mail->remitente != null && $mail->remitente != ''){
			$mail->From = $mail->remitente;
		 }
		 $mail->Subject = $this->ASUNTO;
		 $mail->AltBody = ""; 
		 $mail->MsgHTML($this->MENSAJE);
		 for($i=0;$i<count($this->ADJUNTOS);$i++){
			$mail->AddAttachment($this->ADJUNTOS[$i]);
		 }		 
		
		 $mail->AddAddress($this->DESTINATARIO, "Destinatario");
		 if(trim($this->CC)!=''){
			$copiaCC=explode(';',$this->CC);	 
			for($i=0;$i<count($copiaCC);$i++){
				$mail->AddCC($copiaCC[$i], "Destinatario");
			}
		 }
		 
		 if(trim($this->CCO)!=''){
			$copiaCCO=explode(';',$this->CCO);	 
			for($i=0;$i<count($copiaCCO);$i++){
				$mail->AddBCC($copiaCCO[$i], "Destinatario");
			}
		 }
		 
		 $mail->IsHTML(true); 
		 
		 if(!$mail->Send()) { 
		 	if(!$devolverMensaje){
		 		echo "Error: " . $mail->ErrorInfo; 
		 	}else{
				return $mail->ErrorInfo;
			}
		 } else { 
		 	if(!$devolverMensaje){
				return "Mensaje enviado correctamente"; 
			}else{
				return "success";
			}
		 }
		 
	}
	
	/*
	COPIA SEND MAIL
	function sendMail(){
		$model=new Model();
		$mail = new PHPMailer(); 
		 $mail->IsSMTP(); 
		 $mail->SMTPAuth = true; 
		 $mail->SMTPSecure = "ssl"; 
		 $mail->Host = "smtp.gmail.com"; 
		 $mail->Port = 465; 
		 $mail->Username = $model->getParametroGeneral('mail_from','CORREO'); 
		 $mail->Password = "0Redesoft";
		
		 $mail->From = $model->getParametroGeneral('mail_from','CORREO'); 
		 $mail->FromName = "Nombre"; 
		 $mail->Subject = "Asunto del Email"; 
		 $mail->AltBody = "Este es un mensaje de prueba."; 
		 $mail->MsgHTML($this->MENSAJE); 
		 $mail->AddAttachment("files/files.zip"); 
		 $mail->AddAttachment("files/img03.jpg"); 
		 $mail->AddAddress("mario_vasco@hotmail.com", "Destinatario"); 
		 $mail->IsHTML(true); 
		 if(!$mail->Send()) { 
		 echo "Error: " . $mail->ErrorInfo; 
		 } else { 
		 echo "Mensaje enviado correctamente"; 
		 }
	}*/
	
	
	function sendMail_x(){
		$model=new Model();
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		// Additional headers
		$destinos=explode(',',$this->DESTINATARIO);
		for($i=0;$i<count($destinos);$i++){
		//	$headers .= 'To: <'.$destinos[$i].'>' . "\r\n";
		}
		
		$REMITENTE=$this->REMITENTE;
		if(trim($this->REMITENTE)==''){
			$REMITENTE=$model->getParametroGeneral('mail_from','CORREO');
		}
		
		$headers .= 'To: <'.$this->DESTINATARIO.'>' . "\r\n";
		$headers .= 'From: <'.$REMITENTE.'>' . "\r\n";
		if(trim($this->CC)!=''){
			$headers .= 'Cc: '.$this->CC.'' . "\r\n";
		}
		if(trim($this->CCO)!=''){
			$headers .= 'Bcc: '.$this->CCO.'' . "\r\n";
		}
		
		return (mail($this->DESTINATARIO, $this->ASUNTO, $this->MENSAJE, $headers));

	}
	
}

?>