<?php
@session_start();
// require_once("configurar.php");
require_once("BDController.php");



define("IVA", 0);
define("CUENTA_IVA", "236580002");
define("CUENTA_DESCUENTO", "53054500");

define("CAPITADO", 1);
define("MONTO", 2);
define("EVENTO", 3);
define("CONSOLIDADO", 4);



//date_default_timezone_set('America/Bogota');

class Model {

var $statusUserLogueado = false;
var $db = NULL;
var $msgError= NULL;


function decryptClaveSoporte($string, $key) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
	  $char = substr($string, $i, 1);
	  $keychar = substr($key, ($i % strlen($key))-1, 1);
	  $char = chr(ord($char)-ord($keychar));
	  $result.=$char;
   }
   return $result;
}

function full_copy_directory( $source, $target ) {
    if ( is_dir( $source ) ) {
        @mkdir( $target );
        $d = dir( $source );
        while ( FALSE !== ( $entry = $d->read() ) ) {
            if ( $entry == '.' || $entry == '..' ) {
                continue;
            }
            $Entry = $source . '/' . $entry; 
            if ( is_dir( $Entry ) ) {
                full_copy( $Entry, $target . '/' . $entry );
                continue;
            }
            copy( $Entry, $target . '/' . $entry );
        }
 
        $d->close();
    }else {
        copy( $source, $target );
    }
}

function microtime_float()
{
 	usleep(10);
    list($usec, $sec) = explode(" ", microtime());
    return str_replace('.','',((float)$usec + (float)$sec));
}

function ValidarRestriccionMctos($CodMed,$autoid,$cantidad){
	$rs=$this->execSp("spRestriccionMctosPctos @Op='HasRestriccion',@CodMed='".$CodMed."',@Autoid='".$autoid."',@Cantidad='".$cantidad."'",NULL,1);
	$row=$this->nextRow($rs);
	return $row;
}

function ValidarRestriccionPctos($CodProc,$Tipo,$autoid,$cantidad){
	$rs=$this->execSp("spRestriccionMctosPctos @Op='HasRestriccionProc',@CodProc='".$CodProc."',@Tipo='".$Tipo."',@Autoid='".$autoid."',@Cantidad='".$cantidad."'",NULL,1);
	$row=$this->nextRow($rs);
	return $row;
}

function ValidarRestriccionPctosNuevo($CodProc,$Tipo,$autoid){
	$rs=$this->execSp("spRestriccionMctosPctos @Op='HasRestriccionProcNuevo',@CodProc='".$CodProc."',@Tipo='".$Tipo."',@Autoid='".$autoid."'",NULL,1);
	$row=$this->nextRow($rs);
	return $row;
}

function ValidarRestriccionOrdenPctosMontoPcte($CodigoActividad,$CodProcedimiento,$Contrato,$Servicio,$Autoid,$Fase,$Fecha,$Cantidad){
	$rs=$this->execSp("spPlanEspecial @Op='ValidarItemOrden',@CodigoActividad='".$CodigoActividad."',@CodProcedimiento='".$CodProcedimiento."',@Contrato='".$Contrato."',@Servicio='".$Servicio."',@Autoid=".$Autoid.",@Fase=".$Fase.",@Fecha='".$Fecha."',@Cantidad=".$Cantidad,NULL,1);
	$row=$this->nextRow($rs);
	return $row;
}

function ValidarRestriccionFactPctosMontoPcte($CodigoActividad,$CodProcedimiento,$Contrato,$Servicio,$Autoid,$Fase,$Fecha,$Cantidad){
	$rs=$this->execSp("spPlanEspecial @Op='ValidarItemFacturacion',@CodigoActividad='".$CodigoActividad."',@CodProcedimiento='".$CodProcedimiento."',@Contrato='".$Contrato."',@Servicio='".$Servicio."',@Autoid=".$Autoid.",@Fase=".$Fase.",@Fecha='".$Fecha."',@Cantidad=".$Cantidad,NULL,1);
	$row=$this->nextRow($rs);
	return $row;
}

// rellena el numero con 0 a la izquierda dependiendo del valor de $digs
function rellenar($numero, $digs=6,$caracter='0') {
	return str_pad($numero, $digs, $caracter, STR_PAD_LEFT);
}

function eliminarDir($carpeta){
	foreach(glob($carpeta . "/*") as $archivos_carpeta){
		//echo $archivos_carpeta;		 
		if (is_dir($archivos_carpeta)){
			eliminarDir($archivos_carpeta);
		}
		else{
			unlink($archivos_carpeta);
		}
	}	 
	rmdir($carpeta);
}

function ConvertirArrayArchivo($array,$separador=','){
	
}

function tiempo_transcurrido($fecha) {
	if(empty($fecha)) {
		  return "No hay fecha";
	}
   
	$intervalos = array("segundo", "minuto", "hora", "día", "semana", "mes", "año");
	$duraciones = array("60","60","24","7","4.35","12");
   
	$ahora = time();
	$Fecha_Unix = strtotime($fecha);
	
	if(empty($Fecha_Unix)) {   
		  return "Fecha incorrecta";
	}
	if($ahora > $Fecha_Unix) {   
		  $diferencia     =$ahora - $Fecha_Unix;
		  $tiempo         = "Hace";
	} else {
		  $diferencia     = $Fecha_Unix -$ahora;
		  $tiempo         = "Hace";
	}
	for($j = 0; $diferencia >= $duraciones[$j] && $j < count($duraciones)-1; $j++) {
	  $diferencia /= $duraciones[$j];
	}
   
	$diferencia = round($diferencia);
	
	if($diferencia != 1) {
		$intervalos[5].="e"; //MESES
		$intervalos[$j].= "s";
	}
   
    return "$tiempo $diferencia $intervalos[$j]";
}


function EncryptDecrypt($string){
	return str_rot13($string);
}

function styleBorderOdonto($array,$i){
global $borderRight,$borderTop,$borderLeft,$borderBottom;
$style=" style='";
	if($array[$i]==18||$array[$i]==48){
		$style.=$borderLeft;
	}	
	if($array[$i]==28||$array[$i]==11||$array[$i]==51||$array[$i]==38||$array[$i]==81||$array[$i]==41){
		$style.=$borderRight;
	}	
	if($array[$i]==18||$array[$i]==17||$array[$i]==16||$array[$i]==15||$array[$i]==14||$array[$i]==13||$array[$i]==12||$array[$i]==11||$array[$i]==21||$array[$i]==22||$array[$i]==23||$array[$i]==24||$array[$i]==25||$array[$i]==26||$array[$i]==27||$array[$i]==28||$array[$i]==85||$array[$i]==84||$array[$i]==83||$array[$i]==82||$array[$i]==81||$array[$i]==71||$array[$i]==72||$array[$i]==73||$array[$i]==74||$array[$i]==75){
	$style.=$borderTop;
		
	}	
	if($array[$i]==48||$array[$i]==47||$array[$i]==46||$array[$i]==45||$array[$i]==44||$array[$i]==43||$array[$i]==42||$array[$i]==41||$array[$i]==31||$array[$i]==32||$array[$i]==33||$array[$i]==34||$array[$i]==35||$array[$i]==36||$array[$i]==37||$array[$i]==38){
	$style.=$borderBottom;
		
	}	
	
	$style.="'";
	return $style;		

}

function posicionDiente($diente,$superficie){
		if($diente==18||$diente==17||$diente==16||$diente==15||$diente==14||$diente==13||$diente==12||$diente==11||$diente==55||$diente==54||$diente==53||$diente==52||$diente==51){
			if($superficie=='sup_sup')
				return "Cervical";
			if($superficie=='sup')
				return "Vestibular";
			if($superficie=='izq')
				return "Distal";
			if($superficie=='centro')
				return "Oclusal";
			if($superficie=='der')
				return "Mesial";
			if($superficie=='inf')
				return "Palatino";
			if($superficie=='inf_inf')
				return "cervical";		
			
		}else if($diente==21||$diente==22||$diente==23||$diente==24||$diente==25||$diente==26||$diente==27||$diente==28||$diente==61||$diente==62||$diente==63||$diente==64||$diente==65){
			if($superficie=='sup_sup')
				return "Cervical";
			if($superficie=='sup')
				return "Vestibular";
			if($superficie=='izq')
				return "Mesial";
			if($superficie=='centro')
				return "Oclusal";
			if($superficie=='der')
				return "Distal";
			if($superficie=='inf')
				return "Palatino";
			if($superficie=='inf_inf')
				return "cervical";
		}else if($diente==85||$diente==84||$diente==83||$diente==82||$diente==81||$diente==48||$diente==47||$diente==46||$diente==45||$diente==44||$diente==43||$diente==42||$diente==41){
			if($superficie=='sup_sup')
				return "Cervical";
			if($superficie=='sup')
				return "Lingual";
			if($superficie=='izq')
				return "Distal";
			if($superficie=='centro')
				return "Oclusal";
			if($superficie=='der')
				return "Mesial";
			if($superficie=='inf')
				return "Vestibular";
			if($superficie=='inf_inf')
				return "cervical";
		}else if($diente==71||$diente==72||$diente==73||$diente==74||$diente==75||$diente==31||$diente==32||$diente==33||$diente==34||$diente==35||$diente==36||$diente==37||$diente==38){
			if($superficie=='sup_sup')
				return "Cervical";
			if($superficie=='sup')
				return "Lingual";
			if($superficie=='izq')
				return "Mesial";
			if($superficie=='centro')
				return "Oclusal";
			if($superficie=='der')
				return "Distal";
			if($superficie=='inf')
				return "Vestibular";
			if($superficie=='inf_inf')
				return "cervical";
		}	
}

function is_url_exist($ruta){
	

	if (file_exists($ruta)) {
		return true;
	} else {
		return false;
	}

	/*$file = $ruta;
	// set up basic connection
	$conn_id = ftp_connect($this->getParametroGeneral('servidor_ftp_file','ARCHIVOS SISTEMA'));
	
	// login with username and password
	$login_result = ftp_login($conn_id, $this->getParametroGeneral('usuario_ftp_file','ARCHIVOS SISTEMA'), $this->getParametroGeneral('password_ftp_file','ARCHIVOS SISTEMA'));
	
	// get the size of $file
	$res = ftp_size($conn_id, $file);
	ftp_close($conn_id);
	
	if ($res != -1) {
		return true;
	} else {
		return false;
	}*/
	
	// close the connection
}

function save_image($inPath,$outPath){ 
	//Download images from remote server
    $in=    fopen($inPath, "rb");
    $out=   fopen($outPath, "wb");
    while ($chunk = fread($in,8192))
    {
        fwrite($out, $chunk, 8192);
    }
    fclose($in);
    fclose($out);
}

function buscarCadena($cadena,$palabra)
    {
        if (strstr(strtoupper($cadena),$palabra))
            return true;
        else
            return false;
    }
	
function ArrayToXml($array,$nameItem='object',$show=NULL){
	$i=0;
	$xml='';
	if($show==NULL){
		$caracterI='<';
		$caracterF='>';
	}else{
		$caracterI='&#60;';
		$caracterF='&#62;';		
	}

	foreach($array as $r=>$row){
		$xml.=$caracterI.''.$nameItem.' ';
		foreach($row as $r2=>$cel){
			$xml.=$r2."=\"".$this->replaceXmlSpecialChars($cel)."\" ";	
		}
		$i++;
		$xml.= "/".$caracterF;
	}
	if($show==NULL){
		return $xml;
	}else{
		echo $xml;
	}	
}

function replaceXmlSpecialChars($text){
	
	$text = str_replace("&", "&amp;", $text);
	$text = str_replace('"', '&quot;', $text);
	$text = str_replace("'", '&apos;', $text);
	$text = str_replace('<', '&lt;', $text);
	$text = str_replace('>', '&gt;', $text);
	
	return $text;
}

function diferenciaEntreFechas($fecha_principal, $fecha_secundaria, $obtener = 'SEGUNDOS', $redondear = false){
   $f0 = strtotime($fecha_principal);
   $f1 = strtotime($fecha_secundaria);
   if ($f0 < $f1) { $tmp = $f1; $f1 = $f0; $f0 = $tmp; }
   $resultado = ($f0 - $f1);
   switch ($obtener) {
       default: break;
       case "MINUTOS"   :   $resultado = $resultado / 60;   break;
       case "HORAS"     :   $resultado = $resultado / 60 / 60;   break;
       case "DIAS"      :   $resultado = $resultado / 60 / 60 / 24;   break;
       case "SEMANAS"   :   $resultado = $resultado / 60 / 60 / 24 / 7;   break;
   }
   if($redondear) $resultado = round($resultado);
   return $resultado;
}

public function decode($teskt){
	 	$letter='';       
        $code="";
        try{
			for($i=strlen($teskt);$i>0;$i--){
				$letter=ord(substr($teskt, $i-1))-strlen($teskt)+($i-1)."<br>";     		
				$code=$code.chr($letter);
			}
        }catch(Exception $e){}	
		return $code;
}
	
function setDateFormat($fecha,$formato){	
	$date=new DateTime($fecha);	
	return $date->format($formato);	
}

function calculaedadAnios($fechanacimiento){
    list($ano,$mes,$dia) = explode("-",$fechanacimiento);
    $ano_diferencia  = date("Y") - $ano;
    $mes_diferencia = date("m") - $mes;
    $dia_diferencia   = date("d") - $dia;
    if ($dia_diferencia < 0 || $mes_diferencia < 0)
        $ano_diferencia--;
	if($ano_diferencia<0)
		$ano_diferencia=0;
    return $ano_diferencia;
}
function file_upload_error_message($error_code) {
    switch ($error_code) { 
        case UPLOAD_ERR_INI_SIZE: 
            return 'The uploaded file exceeds the upload_max_filesize directive in php.ini'; 
        case UPLOAD_ERR_FORM_SIZE: 
            return 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        case UPLOAD_ERR_PARTIAL: 
            return 'The uploaded file was only partially uploaded'; 
        case UPLOAD_ERR_NO_FILE: 
            return 'No file was uploaded'; 
        case UPLOAD_ERR_NO_TMP_DIR: 
            return 'Missing a temporary folder'; 
        case UPLOAD_ERR_CANT_WRITE: 
            return 'Failed to write file to disk'; 
        case UPLOAD_ERR_EXTENSION: 
            return 'File upload stopped by extension'; 
        default: 
            return 'Unknown upload error'; 
    } 
}

public function Rutas(){

/*******************************************************************************/
/*	Varibles de Configuracion del Sitio										   */
/******************************************************************************/																								

$_SESSION['http_host_vacuna'] = $_SERVER['HTTP_HOST'];
$_SESSION['protocolo_c'] = '';

// se captura si el sitio esta trabajando con https
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off"){
    $_SESSION['protocolo_c'] = "s";
}

$_SESSION['Dir_app_main_vacuna'] = 'SismaSalud';
$_SESSION['Dir_app_contenedor_vacuna'] = 'Vacunacion';
$_SESSION['Document_root_vacuna']=$_SERVER['DOCUMENT_ROOT'];
$_SESSION['Name_App_vacuna']='Sisma Asistencial';

//modificado
$_SESSION['site_name_vacuna'] = "http{$_SESSION['protocolo_c']}://{$_SESSION['http_host_vacuna']}/{$_SESSION['Dir_app_main_vacuna']}/{$_SESSION['Dir_app_contenedor_vacuna']}";
$_SESSION['dominio_vacuna'] = "{$_SESSION['Dir_app_main_vacuna']}/{$_SESSION['Dir_app_contenedor_vacuna']}";
$_SESSION['path_vacuna'] = "http{$_SESSION['protocolo_c']}://{$_SESSION['http_host_vacuna']}/{$_SESSION['dominio_vacuna_vacuna']}";
$_SESSION['root_vacuna'] = "{$_SERVER['DOCUMENT_ROOT']}";


//var_dump($_SESSION['dominio_vacuna']);
/*******************************************************************************/
/*	CVaribles de Sesion			    										   */
/******************************************************************************/																								
$_SESSION['perfil_vacuna'] = "Visitante";
$_SESSION['PathReport_vacuna'] = "{$_SESSION['Document_root']}/{$_SESSION['Dir_app_main']}//{$_SESSION['Dir_app_contenedor']}//Reportes//cristalreport";
$_SESSION['PathReportTemp_vacuna'] = "{$_SESSION['Document_root']}/{$_SESSION['Dir_app_main']}//{$_SESSION['Dir_app_contenedor']}//Reportes//cristalreport";
$_SESSION['PathReportTempPreview_vacuna'] = $_SESSION['path_vacuna']."/Reportes/cristalreport";
/*******************************************************************************/
/*	 varibles de transaccion     											   */
/*******************************************************************************/
$_SESSION['tiempo_vacuna']='';
$_SESSION['Inicio_vacuna'] = getdate();
$_SESSION['Fina_vacunal']='';
$_SESSION['ref_vacuna'] = '';
//setlocale("LC_time", "es-ES");

$_SESSION["IdUsuario"] = $_SESSION["IdUsuario"];
}

function Model($host = NULL, $database = NULL, $cliente = NULL, $clave = NULL) {
	global $db;
	
	/*print ("HOST: ".$host.
		", DATABASE: ".$database.
		", USER: ".$cliente.
		", PASSWORD: ".$clave);*/
			
	// if (is_null($host)) {
		$this->Rutas();
		$dts_conexion = parse_ini_file($_SESSION['root_vacuna'].'/conf/conf_zs.ini', true);
		
		// var_dump($_SESSION['root_vacuna'].'/conf/conf_zs.ini');
		// var_dump($dts_conexion);
		// var_dump($_SESSION['host']);
		// var_dump($_SESSION['database']);
		// var_dump($_SESSION['cliente']);
		// var_dump($_SESSION['clave']);
		// die();
		$db = new BDController($dts_conexion['PQRS']['servidor'], 
								$dts_conexion['PQRS']['bd'], 
								$dts_conexion['PQRS']['usuario'], str_rot13($dts_conexion['PQRS']['password']));
	// } else {
	// 	$db = new BDController($host, $database, $cliente, $clave);
	// }
	if ($db->connect() == false) {
		print("Error de conexi\&oacute;n a la base de datos: ".$db->getMessage());
		// print($_SERVER["HTTP_HOST"].'/SismaSalud/portalUsuarios/iniciando.php');
		//header('Location: '.$_SERVER["HTTP_HOST"].'/SismaSalud/portalUsuarios/iniciando.php');
		// die();
		// $array['conexion'] = false;
		// return $array;

	}

	if (isset($_SESSION["IdUsuario"])) {
		$this->statusUserLogueado = true;
	}
}

function statusUserLogueado(){
	if (isset($_SESSION["IdUsuario"]) && $_SESSION["IdUsuario"] != "" && $_SESSION["IdUsuario"] != null && $_SESSION["IdUsuario"] != 0) {
		return true;
	}else{
		return false;
	}
}

function Params($param){
	$array=array();
	foreach($param as $r=>$row){
		//Sacamos el primer valor antes de del : para obtener la cadena que se convertira en variable
		$a=(explode(':',$row));
		//Luego de sacar la primera parte de la cadena obtenemos el resto ( valor de la variable ).
		$r=explode($a[0].":",$row);		
		//obtenemos el nombre de la que sera variable
		$nom=trim($a[0]);
		//agregamos al array la variable y asignamos su valor.
		$array[$nom]=trim($r[1]);
	}
	return $array;
}


public function SQL_injection($valor,$SP=NULL){
			$valor=utf8_encode($valor);
			$valor=str_replace("Ã'", "&Ntilde;", $valor);
			$valor=str_replace("á", "&aacute;", $valor);
			$valor=str_replace("é", "&eacute;", $valor);
			$valor=str_replace("í", "&iacute;", $valor);
			$valor=str_replace("ó", "&oacute;", $valor);
			$valor=str_replace("ú", "&uacute;", $valor);
			$valor=str_replace("Á", "&Aacute;", $valor);
			$valor=str_replace("É", "&Eacute;", $valor);
			$valor=str_replace("Í", "&Iacute;", $valor);
			$valor=str_replace("Ó", "&Oacute;", $valor);
			$valor=str_replace("Ú", "&Uacute;", $valor);
			$valor=str_replace("ñ", "&ntilde;", $valor);
			$valor=str_replace('Ñ','&Ntilde;',$valor);
			$valor=str_replace("Ã±", "&ntilde;", $valor);
			$valor=str_replace("Ã¡", "&aacute;", $valor);
			$valor=str_replace("Ã©", "&eacute;", $valor);
			$valor=str_replace("Â®", "&reg;", $valor);
			$valor=str_replace("Ã­", "&iacute;", $valor);
			$valor=str_replace("ï¿½", "&iacute;", $valor);
			$valor=str_replace("Ã³", "&oacute;", $valor);
			$valor=str_replace("Ãº", "&uacute;", $valor);
			$valor=str_replace("n~", "&ntilde;", $valor);
			$valor=str_replace("Âº", "&ordm;", $valor);
			$valor=str_replace("Âª", "&ordf;", $valor);
			$valor=str_replace("ÃƒÂ¡", "&aacute;", $valor);
			$valor=str_replace("Ã±", "&ntilde;", $valor);
			$valor=str_replace("Ã'", "&Ntilde;", $valor);
			$valor=str_replace("ÃƒÂ±", "&ntilde;", $valor);
			$valor=str_replace("n~", "&ntilde;", $valor);
			$valor=str_replace("Ãš", "&Uacute;", $valor);
			$valor=str_replace("Ã‘", "&Ntilde;", $valor);
			$valor=addslashes($valor);
			if($SP!=NULL){
				$valor=str_replace("\'", "''", $valor);	
			}
			$valor=trim($valor);
			return ($valor);
}

public function SQL_injection2($valor,$SP=NULL,$borraComillas=1,$ReemplazaComillaDoble=1){
			//$valor=utf8_encode($valor);
			/*$valor=str_replace("Ã'", "&Ntilde;", $valor);
			$valor=str_replace("á", "&aacute;", $valor);
			$valor=str_replace("é", "&eacute;", $valor);
			$valor=str_replace("í", "&iacute;", $valor);
			$valor=str_replace("ó", "&oacute;", $valor);
			$valor=str_replace("ú", "&uacute;", $valor);
			$valor=str_replace("Á", "&Aacute;", $valor);
			$valor=str_replace("É", "&Eacute;", $valor);
			$valor=str_replace("Í", "&Iacute;", $valor);
			$valor=str_replace("Ó", "&Oacute;", $valor);
			$valor=str_replace("Ú", "&Uacute;", $valor);
			$valor=str_replace("ñ", "&ntilde;", $valor);
			$valor=str_replace('Ñ','&Ntilde;',$valor);
			$valor=str_replace("Ã±", "&ntilde;", $valor);
			$valor=str_replace("Ã¡", "&aacute;", $valor);
			$valor=str_replace("Ã©", "&eacute;", $valor);
			$valor=str_replace("Â®", "&reg;", $valor);
			$valor=str_replace("Ã­", "&iacute;", $valor);
			$valor=str_replace("ï¿½", "&iacute;", $valor);
			$valor=str_replace("Ã³", "&oacute;", $valor);
			$valor=str_replace("Ãº", "&uacute;", $valor);
			$valor=str_replace("n~", "&ntilde;", $valor);
			$valor=str_replace("Âº", "&ordm;", $valor);
			$valor=str_replace("Âª", "&ordf;", $valor);
			$valor=str_replace("ÃƒÂ¡", "&aacute;", $valor);
			$valor=str_replace("Ã±", "&ntilde;", $valor);
			$valor=str_replace("Ã'", "&Ntilde;", $valor);
			$valor=str_replace("ÃƒÂ±", "&ntilde;", $valor);
			$valor=str_replace("n~", "&ntilde;", $valor);
			$valor=str_replace("Ãš", "&Uacute;", $valor);
			$valor=str_replace("Ã‘", "&Ntilde;", $valor);*/
			$valor=utf8_decode($valor);
			if($SP!=NULL){
				$valor=str_replace("'", "''", $valor);	
				if($ReemplazaComillaDoble==1){
					$valor=str_replace('"', "''", $valor);
				}
				//$valor=str_replace('°', "''", $valor);	
			}
			if($borraComillas==1){
				$valor=str_replace("'", "", $valor);	
				$valor=str_replace('"', "", $valor);	
			}
			$valor=trim($valor);
			return ($valor);
}

function altaNotificacion($estudio){
	$autoidPaci=$this->getDato("autoid","sis_maes","con_estudio=".$estudio); 
	$this->insertar("update notificacion_ordenes set alta=1 where autoid=".$autoidPaci);
	$error = ($this->getLastErrorCode() != 0);
	return $error;
}
function insertarNotificacion($nro_orden,$autoid,$tipo_proceso,$estudio=NULL,$relacion_orden=NULL){

		$campoRelacion="";
		$nro_relacion="";
		
		if($relacion_orden!=NULL){
			$campoRelacion=",orden_relacion";
			$nro_relacion=",".$relacion_orden;
		}
		$campoEstudio="";
		$nro_estudio="";
		
		if($estudio!=NULL){
			$campoEstudio=",estudio";
			$nro_estudio=",".$estudio;
		}
		
		
		$this->insertar("insert into notificacion_ordenes (nro_orden,autoid,tipo_proceso".$campoRelacion.$campoEstudio.") values (".$nro_orden.",".$autoid.",'".$tipo_proceso."'".$nro_relacion.$nro_estudio.")");	
	
}

function color($i,$tipo=NULL,$class1=NULL,$class2=NULL,$tipo_return=NULL){
	if($tipo==NULL){
		if($i%2==0){
			if($tipo_return==NULL){
				return 'bgColor="#EFEFEF"';	
			}else{
				return '#EFEFEF';		
			}
		}else{
			if($tipo_return==NULL){
				return 'bgColor="#FFFFFF"';		
			}else{
				return '#FFFFFF';			
			}
		}		
	}else if($tipo==1){
		if($i%2==0){
			return 'class="'.$class1.'"';	
		}else{
			return 'class="'.$class2.'"';	
		}		
	}
}

function notificacionRealizada($nro_orden){
	$this->insertar("update notificacion_ordenes set realizado=1,id_usuario_responsable='".$_SESSION['id_user_sistema']."' where nro_orden=".$nro_orden);
}

function revertirNotificacionRealizada($nro_orden){
	$this->insertar("update notificacion_ordenes set realizado=0 where nro_orden=".$nro_orden);
}


function validarDocumentoPaciente($tipo_doc,$fecha_naci,$clase=NULL){
	$error=false;
	$rs=$this->select("*","sis_tipo_documento","abreviatura='".$tipo_doc."' AND estado=0");
	
	if($row=$this->nextRow($rs)){
		
		if(trim($row["edad_minima"])!=""&&trim($row["edad_maxima"])!=""){
			$rs=$this->RSAsociativo("select (datediff(day,'".$fecha_naci."',getdate())/365) as edad");
			foreach ($rs as $r=>$edad) {
				
				$rs=$this->RSAsociativo("select case when (".$edad["edad"]." between '".$row["edad_minima"]."' and '".$row[	
				"edad_maxima"]."') then 'ok' else 'error' end as edad");
				foreach ($rs as $r=>$val) {
					if($val["edad"]=="error"){
						$error=true;
						if($clase!=NULL){
							$clase->msgError="Error: Verifique la edad del paciente y su tipo de documento. (Rango edad: ".$row["edad_minima"]." a&ntilde;o(s) ".$this->edadMaxima($row["edad_maxima"]).")";	
						}
					}
				}
				
			}//fin foreach superior
			
		}else if(trim($row["edad_minima"])!=""&&trim($row["edad_maxima"])==""){
			$rs=$this->RSAsociativo("select datediff(year,'".$fecha_naci."',getdate()) as edad");
			foreach ($rs as $r=>$edad) {
				
				$rs=$this->RSAsociativo("select case when (".$edad["edad"]." >= '".$row["edad_minima"]."') then 'ok' else 'error' end as edad");
				foreach ($rs as $r=>$val) {
					if($val["edad"]=="error"){
						$error=true;
						if($clase!=NULL){
							$clase->msgError="Error: Verifique la edad del paciente y su tipo de documento. (edad minima ".$row["edad_minima"]." a&ntilde;o(s))";	
						}
					}
				}
				
			}//fin foreach superior
			
		}else if(trim($row["edad_minima"])==""&&trim($row["edad_maxima"])!=""){
			$rs=$this->RSAsociativo("select datediff(year,'".$fecha_naci."',getdate()) as edad");
			foreach ($rs as $r=>$edad) {
				
				$rs=$this->RSAsociativo("select case when (".$edad["edad"]." <= '".$row["edad_maxima"]."') then 'ok' else 'error' end as edad");
				foreach ($rs as $r=>$val) {
					if($val["edad"]=="error"){
						$error=true;
						if($clase!=NULL){
							$clase->msgError="Error: Verifique la edad del paciente y su tipo de documento. (edad maxima ".$row["edad_maxima"]." a&ntilde;o(s))";	
						}
					}
				}
				
			}//fin foreach superior
			
		}
		
	}
	 	
	return $error;
}

function edadMaxima($edad){
	if($edad>100){
		return "en adelante";
	}else{
		return "y ".$edad." a&ntilde;o(s)";
	}
}


function replaceCad($cad){
	
	$cad=str_replace("&ntilde;","ñ",$cad);
	$cad=str_replace("&Ntilde;","Ñ",$cad);

	return $cad;	
}

function obtenerConsecutivoFactura($id_sede){
	global $db;	
	$nro_factura='';
	
	if($this->getParametroGeneral('factura_sede','FACTURACION')=='N'){
		$nro_factura = $model->getNumeroFactura(date("m"), date("Y"));
		$nro_factura = $model->obtenerSiguiente("nro_factura");
		$this->establecerSiguiente("nro_factura");
	}else{
		$nro_factura=$row_sede['cons_factura'];
		$sql = "UPDATE seriales SET cons_factura=cons_factura+1
		WHERE id = ".$row_sede['id'];
		$this->insertar($sql);
	}
	return $nro_factura;
	/*
	if($model->getParametroGeneral('factura_sede','FACTURACION')=='N'){
						$nro_factura = $model->getNumeroFactura(date("m"), date("Y"));
						$nro_factura = $model->obtenerSiguiente("nro_factura");
						$model->establecerSiguiente("nro_factura");
					}else{
						$nro_factura=$row_sede['cons_factura'];
						$sql = "UPDATE seriales SET cons_factura=cons_factura+1
						WHERE id = ".$row_sede['id'];
						$model->insertar($sql);
					}
	*/
}

function mostrarPresentacionMedicamento($cod_prod, $medida){
	global $db;	
	$sql = "SELECT p.unimed FROM sis_prod as p 
	where p.codigo='".$cod_prod."'";
	$rs = $db->query($sql);
	$row = $rs->nextRow();
	$array=explode('.',$medida);
	$texto=$array[0];
	if($array[1]>0){
		$texto.=' con '.$array[1].' '.$row['unimed'];
	}
	return $texto;
}
//agregado sergio 02/01/2012
function chartoascii($text,$tipo){
$strlength = strlen($text); 
		for($i = 0; $i < $strlength; $i++){ 
			switch($tipo){
				case 1 :
			$retString .= ord(substr($text,$i,1))+1; 
				case 2 :
			$retString .= ord(substr($text,$i,1))-1; 
				default:
				$retString .= ord(substr($text,$i,1)); 
				}
        } 
	return $retString;
}

function asciitochar($text,$tipo){
$strlength = strlen($text); 
		for($i = 0; $i < $strlength; $i=$i+3){ 
			switch($tipo){
				case 1 :
			$retString .= chr(substr($text,$i,3)+1); 
				case 2 :
			$retString .= chr(substr($text,$i,3)-1);
				default:
			$retString .= chr(substr($text,$i,3));
				}
        } 
	return $retString;
}

//final

function tipoproductox(){
	$tipo = "tipoproducto";
	 $tipopro = $this->getDato("valor","parametros","nombre = '$tipo'");
	 return $tipopro;	 
}
//agregado nuevo
function right($value, $count){
    return substr($value, ($count*-1));
}
 
function left($string, $count){
    return substr($string, 0, $count);
}

//-----------------------------------------
function crearGrafo($posicion,$titulos,$datos){
$pos = $this->getDato("campo1","detalleshc","estudio = '".$datos['estudio']."' and posicion='".$posicion[0]."' and codigo='".$datos['codigo']."' ");
$_SESSION['estilo'] = $datos['estilo'];
$ruta =  $_SESSION["CarpetaArchivosRead"]."/ImagenesHc/".$datos['codigo']."/".$posicion[0]."_view.jpg"; 

echo "<img src='{$ruta}' alt='No hay imagen' />";

}

//-----------------------------------------

function crearTiposHistoria() {
	global $db;	
	$sql = "SELECT codigo, detalle FROM hctiphis order by detalle";
	$rs = $db->query($sql);
	$cad = "<script language='JavaScript'>".
		"arbol = new TreeMenu('arbol', '../../Comun/treeview/images', 'arbol');\n".
		"arbol.n[0] = new TreeNode('TIPOS DE HISTORIA', 'folderazul2.gif', 
		'#', false, true, '_self');\n";
	$i = 0;
	$ayer = date("Y/m/01");
	$hoy = date("Y/m/d");
	while (($row = $rs->nextRow())) {
		$cad .= "arbol.n[0].n[".$i++."] = new TreeNode('{$row['detalle']}', 
			'folderazul1.gif', 
			'javascript:infoPaciente(\'".$row['codigo']."\', \'".$row['detalle']."\');', 
			false, true, '_self');\n";
	}
	$cad .= "arbol.drawMenu();</script>";
	$rs->free();
	return $cad;
}

function traerhorainicio($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("  cast(substring(horaini,1,2) as integer ) as hora", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1' ");
		return $hora_inicio;
}

function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0){
    $date_r = getdate(strtotime($date)); 
    $date_result = date("Y/m/d", mktime(($date_r["hours"]+$hh),($date_r["minutes"]+$mn),($date_r["seconds"]+$ss),($date_r["mon"]+$mm),($date_r["mday"]+$dd),($date_r["year"]+$yy)));
    return $date_result;
}
 

function traerhorafin($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("  cast(substring(horafin,1,2) as integer ) as hora", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1'");
		return $hora_inicio;
}

function traerminutoinicio($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("cast(substring(horaini,4,2) as integer ) as hora", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1'");
		return $hora_inicio;
}

function traerminutofin($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("cast(substring(horafin,4,2) as integer ) as hora", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1'");
		return $hora_inicio;
}

function traermeridianoinicio($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("meridianoi", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1'");
		return $hora_inicio;
}

function traermeridianofin($codmed,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("meridianof", "horariomedico"," dia = '$ndiasem' and medico = '$codmed' and estado='1'");
		return $hora_inicio;
}

/*** HORARIO QUIROFANO **/
function traerhorainicioQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("  cast(substring(horaini,1,2) as integer ) as hora", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1' ");
		return $hora_inicio;
}

function traerhorafinQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("  cast(substring(horafin,1,2) as integer ) as hora", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1'");
		return $hora_inicio;
}

function traerminutoinicioQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("cast(substring(horaini,4,2) as integer ) as hora", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1'");
		return $hora_inicio;
}

function traerminutofinQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("cast(substring(horafin,4,2) as integer ) as hora", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1'");
		return $hora_inicio;
}

function traermeridianoinicioQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("meridianoi", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1'");
		return $hora_inicio;
}

function traermeridianofinQuirofano($codQuirofano,$fecha) {
	 	$ndiasem = date("w", strtotime($fecha));
	    $hora_inicio = $this->getDato("meridianof", "horarioquirofano"," dia = '$ndiasem' and quirofano = '$codQuirofano' and estado='1'");
		return $hora_inicio;
}
/*** HORARIO QUIROFANO **/

function verEstado($hora_XX,$ndiasem,$codigo_medico){
		$estadohora = $this->getDato("count(*)", "horariomedico"," dia = '$ndiasem' and medico = '$codigo_medico' 
		and convert(datetime,'$hora_XX $meridiano',114) between convert(datetime,horaini +space(1)+ meridianoi,114) and convert(datetime,horafin +space(1)+ meridianof,114)  and estado ='2'");
		return $estadohora;
}


function busCita($estudioCita){
		$Citahora = $this->getDato("hora + meridiano", "citas"," estudio = $estudioCita");
		return $Citahora;
}

/*****************************************************************************
 *
 * Funcion crear lista con campos determinados
 *
 *****************************************************************************/
function Mylistaelem($tablas, $campos , $tipo, $where = NULL, $selected = NULL, $otro = NULL){
		global $db;
		
		if($where != NULL){
			if ($otro != NULL) {
				 $sql = "select $campos from $tablas where $where $otro";
			} else {
				 $sql = "select $campos from $tablas where $where ";
			}
		}else{
		$sql = "select $campos from $tablas ";
		}
		$result=$db->query($sql) or die("Excepcion en la Funcion: Model::MyLista Model");
		$arreglo = array();
		
		while($row = $this->nextRow($result))
		{   if($tipo == 0)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).">".trim(substr($row["nombreve"], 0, 20))."</option>");
			if($tipo == 1)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).">".trim($row["nombre"])."</option>");
			if($tipo == 2)
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".$row["nombre"]."</option>");
			if($tipo == 3)
			$arreglo = array("linea" => "<option value=".$row["codigo"].">".$row["nombre"]."</option>");
			if($tipo == 4)
			printf("<option value=".trim($row["usuario"]).">".trim($row["nombre"])."</option>");
			if($tipo == 5)
			printf("<option ".(!is_null($selected) ? ($selected == $row["fuente"] ? "selected" : "") : "")." value=".$row["fuente"].">".$row["nombre"]."</option>");
			if($tipo == 6)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["detalle"]."</option>");
			if($tipo == 7)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["descripcion"]."</option>");
			if($tipo == 8)
			printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["descripcion"]."</option>");
			if($tipo == 9)
			printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["mes"]."</option>");
			if($tipo == 10)
			printf("<option ".(!is_null($selected) ? ($selected == $row["ccosto"] ? "selected" : "") : "")." value=".$row["ccosto"].">".$row["nombre"]."</option>");
			if($tipo == 11)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).trim($row["nombre"]).">".trim($row["nombre"])."</option>");

			//printf("<option value=".$row["fuente"].">".$row["nombre"]."</option>");
			

		}
	    $this->freeResult($result);
	
return $datos;
		
	}
	
function changeDatabase($database) {
	global $db;
	$db->changeDatabase($database);
}

function getUsers($host, $database, $user, $password) {
	global $db;
	
	$rs = $db->select("usuario, nombre", "usuario");
	$usuarios = $rs->getResultToArray();
	
	$db->disconnect();
	
	return $usuarios;
}

function getNombreEmpresa() {
	return $this->getDato("r_social", "seriales", NULL);
}

function affectedRows() {
	global $db;
	return $db->affectedRows();
}

function nextRow($rs) {
	return $rs->nextRow();
}

function nextResult($rs){
	return 	$rs->nextResult();
}

function numRows($rs) {
	return $rs->numRows();
}

function freeResult($rs) {
	$rs->free();
}

function query($SQL,$show=NULL) {
	global $db;
	if($show!=NULL){
		echo $SQL;	
	}
	return $db->query($SQL);
}
function getMessage($SQL) {
	global $db;
	return $db->getMessage();
}


function getResultToArray($rs) {
	return $rs->getResultToArray();
}

function limitQuery($SQL, $index, $offset = NULL) {
	global $db;
	return $db->limitQuery($SQL, $index, $offset);
}

function CrearTablaImagenEvolucion($nEvolucion, $max = 4) {
	$tabla = "<table width='100%' align='center' class='LSPRESSE'><tr>";
	$rs = $this->select("*", "imagenes", "tabla = 'evoluciones' and idtabla = '$nEvolucion'");
	$limite = $this->numRows($rs);
	for ($cont = 0, $close_tr = false; $cont < $limite; $cont++) {
		$row = $this->nextRow($rs);
		if ($cont % $max == 0 && $close_tr == false) {
			$tabla .= "<tr>";
			$close_tr = true;
		} else if ($cont % $max == 0 && $close_tr == true) {
			$tabla .= "</tr>";
			$close_tr = false;
		}
		$tabla .= 
			"<td width='25%' style='cursor:hand' onmouseover='document.getElementById(\"vistaprevia\").src =  document.getElementById(\"imagen$cont\").src'>
			<table class='LSPRESSE' align='center'>
			<tr><td align='center'>
			<a href='{$row['ruta']}/{$_SESSION['database']}/{$row['nombre']}' target='_blank'>
			<img border='0' id='imagen$cont' name='imagen$cont' src='{$row['ruta']}/{$_SESSION['database']}/{$row['nombre']}' height='90' width='130'/>
			</a>
			</td></tr>
			<tr><td align='right'>
			<a href='{$row['ruta']}/{$_SESSION['database']}/{$row['nombre']}' target='_blank'>
			<img src='../../Imagenes/errorpages.gif' alt='Ver Imagen Real' width='13' height='13' border='0' />
			</a>
			<a href='CtrlEvolucionMedica.php?operacion=del_imagen&num_imagen={$row['id']}&estado={$_REQUEST['estado']}&estudio={$_REQUEST['estudio']}&estado_his={$_REQUEST['estado_his']}&numero={$_REQUEST['numero']}&fecha={$_REQUEST['fecha']}&hora={$_REQUEST['hora']}&peso={$_REQUEST['peso']}&ta={$_REQUEST['ta']}&fr={$_REQUEST['fr']}&temp={$_REQUEST['temp']}&fc={$_REQUEST['fc']}&codigo_medico={$_REQUEST['codigo_medico']}&medico={$_REQUEST['medico']}&codigo_diagnostico_principal={$_REQUEST['codigo_diagnostico_principal']}&codigo_diagnostico_relacionado1={$_REQUEST['codigo_diagnostico_relacionado1']}&codigo_diagnostico_relacionado2={$_REQUEST['codigo_diagnostico_relacionado2']}&codigo_diagnostico_relacionado3={$_REQUEST['codigo_diagnostico_relacionado3']}'>
			<img src='../../Imagenes/Eliminar.jpg' alt='Eliminar' width='13' height='13' border='0' />
			</a>
			</td></tr>
			</table>
			</td>";
	}		   
	$tabla .= "</tr></table>";
	return $tabla;
}

function copiarOrdenLaboratorio($list_lab){
	$rs=$this->select("od.*","ordenes_detalle od","od.id in(".$list_lab.")");
	$codMedi=$this->getDato("codigo","sis_medi","cedula=".$_SESSION['codigo_user']);
	$sql='';
	while($row=$this->nextRow($rs)){
		$numero = $this->obtenerSiguiente("numero_orden");
		$this->establecerSiguiente("numero_orden");
		
		$id_orden_detalle = $this->obtenerSiguiente("id_ordenes_detalle");
		$this->establecerSiguiente("id_ordenes_detalle");	
		
		$num_servicio = $this->obtenerSiguiente("num_servicio");
		$this->establecerSiguiente("num_servicio");
		
		$id = $this->obtenerSiguiente("id_detalle");
		$this->establecerSiguiente("id_detalle");
		
		$sql.= "insert into ordenes_detalle(numero_orden,id,codigo,detalle,iniciado) values('".$numero."','".$id_orden_detalle."','".$row["codigo"]."','".$row["detalle"]."','0');";	
		
		$rs2=$this->select("*","ordenes","numero=".$row["numero_orden"]);
		$num_servicio_padre='0';
		if($rowOrden=$this->nextRow($rs2)){
			$num_servicio_padre=$rowOrden["num_servicio"];
			$sql.= "insert into ordenes(numero,ingreso,fecha,hora,resumen,medico,cod_med,tipo,orden_receta,num_servicio,fecha_usuario,hora_usuario,nom_usuario,autoid,ResumenOrden) values('".$numero."','".$_SESSION['estudio']."','".date("Y-m-d")."','".date("H:i")."','".$rowOrden["resumen"]."','".$_SESSION["user"]."','".$codMedi."','".$rowOrden["tipo"]."','".$rowOrden["orden_receta"]."','".$num_servicio."','".date("Y-m-d")."','".date("H:i")."','".$_SESSION["user"]."','".$rowOrden["autoid"]."','".$rowOrden["ResumenOrden"]."');";
		}	
		$tipoEstudio=$this->getDato("tipo_estudio","sis_maes","con_estudio=".$_SESSION['estudio']);
		$rs3=$this->select("*","sis_deta_temp","num_servicio=".$num_servicio_padre);
		if($rowSisDeta=$this->nextRow($rs3)){
		
			$sql.= "insert into dbo.sis_deta_temp
				( id ,
				  fuente_tips ,
				  tipo_serv ,
				  tipo_estudio ,
				  num_servicio ,
				  cod_servicio ,
				  ccosto ,
				  descripcion ,
				  finalidad ,
				  cod_diap ,
				  cod_diagn1 ,
				  cod_diagn2 ,
				  cod_diagn3 ,
				  tipo_diag ,
				  vlr_servicio ,
				  cantidad ,
				  total ,
				  fecha_servicio ,
				  fecha_fin ,
				  ambito_proc ,
				  personal_ate ,
				  tipo ,
				  forma_realizacion ,
				  tipo_qx ,
				  estudio ,
				  cod_usuario ,
				  nom_usuario ,
				  cuenta ,
				  cod_medico ,
				  codigo_paquete ,
				  codigo_cirugia ,
				  cama ,
				  estado_res ,
				  fecha_estado_res ,
				  usuario_estado_res ,
				  valor_externo ,
				  valor_real ,
				  num_servicio_real ,
				  total_real ,
				  cod_servicio_cups ,
				  cobra_copago ,
				  estado_paciente_cama ,
				  vlr_produccion ,
				  comprobante ,
				  cama_seg ,
				  fact_externa ,
				  porcentaje ,
				  hora ,
				  id_externo ,
				  tipo_externo ,
				  ufuncional ,
				  tiempo_inicio ,
				  observacion ,
				  iniciado ,
				  fecha_guardado ,
				  fecha_iniciado ,
				  usuario_guarda_tarea
				)
		VALUES  ('".$id."', 
				  '".$rowSisDeta["fuente_tips"]."' , 
				  '".$rowSisDeta["tipo_serv"]."' , 
				  '".$tipoEstudio."' , 
				  '".$num_servicio."' , 
				  '".$row["codigo"]."', 
				  '".$rowSisDeta["ccosto"]."' , 
				  '".$rowSisDeta["descripcion"]."' , 
				  ".$rowSisDeta["finalidad"]." , 
				  '".$rowSisDeta["cod_diap"]."' , 
				  '".$rowSisDeta["cod_diagn1"]."' , 
				  '".$rowSisDeta["cod_diagn2"]."' , 
				  '".$rowSisDeta["cod_diagn3"]."' , 
				  '".$rowSisDeta["tipo_diag"]."' , 
				  '".$rowSisDeta["vlr_servicio"]."' , 
				  '".$rowSisDeta["cantidad"]."', 
				  '".$rowSisDeta["total"]."', 
				  '".date("Y-m-d H:i:s")."' , 
				  NULL , 
				  ".$rowSisDeta["ambito_proc"]." , 
				  NULL , 
				  '".$rowSisDeta["tipo"]."' , 
				  0 , 
				  0 , 
				  ".$_SESSION["estudio"]." , 
				  '".$_SESSION['codigo_user']."' , 
				  '".$_SESSION["user"]."' , 
				  '".$rowSisDeta["cuenta"]."' , 
				  '".$codMedi."' , 
				  '".$rowSisDeta["codigo_paquete"]."' , 
				  '".$rowSisDeta["codigo_cirugia"]."' , 
				  0 , 
				  0 , 
				  NULL , 
				  NULL , 
				  NULL , 
				  NULL , 
				  0 , 
				  NULL , 
				  '' , 
				  0 , 
				  0 , 
				  NULL , 
				  '' , 
				  0 , 
				  0 , 
				  '' ,
				  '' ,
				  '' , 
				  '' , 
				  0 , 
				  '' ,
				  '' ,
				  0 , 
				  NULL , 
				  NULL , 
				  0  
				);";
		
		}
		
	}
	$error=false;
	$this->iniciarTransaccion();
	$this->insertar($sql);
	$error = ($this->getLastErrorCode() != 0);
	$this->finTransaccion($error);
	return $error;
	
}


function CrearTablaImagenExamenesPracticados($nEvolucion, $max = 4) {
	$tabla = "<table width='100%' align='center' class='LSPRESSE'><tr>";
	$rs = $this->select("*", "imagenes", "tabla = 'evoluciones' and nro_historia = '$nEvolucion'");
	$limite = $this->numRows($rs);
	for ($cont = 0, $close_tr = false; $cont < $limite; $cont++) {
		//echo "entra";
		$row = $this->nextRow($rs);
		if ($cont % $max == 0 && $close_tr == false) {
			$tabla .= "<tr>";
			$close_tr = true;
		} else if ($cont % $max == 0 && $close_tr == true) {
			$tabla .= "</tr>";
			$close_tr = false;
		}
		$tabla .= 
			"<td width='25%' style='cursor:hand'>
			<table class='LSPRESSE' align='center'>
			<tr><td align='center'>
			<a href='{$row['ruta']}/{$row['nombre']}' target='_blank'>";
		$tipo_archivo = explode("/",$row['formato']);
		//print_r($tipo_archivo);
		if ($tipo_archivo[0]=="image"){
			$tabla .= "<img border='0' id='imagen$cont' name='imagen$cont' src='{$row['ruta']}/{$row['nombre']}' height='90' width='130'/>";
		}else if($row['formato']=="application/pdf"){
			$tabla .= "<img border='0' id='imagen$cont' name='imagen$cont' src='../../Imagenes/logo_pdf.png' height='90' width='130'/>";
		}
		
		$tabla .= "</a>
			</td></tr>
			<tr>
				<td><strong>{$row['nombre']}</strong></td>
			</tr>
			<tr><td align='right'>
			<a href='{$row['ruta']}/{$row['nombre']}' target='_blank'>
			<img src='../../Imagenes/errorpages.gif' alt='Ver Imagen Real' width='13' height='13' border='0' />
			</a>".
			(($this->esSuperUsuario($_SESSION['codigo_user']))? "<a href='CtrlExamenes.php?operacion=del_imagen&num_imagen={$row['id']}&estado={$_REQUEST['estado']}&estudio={$_REQUEST['estudio']}&estado_his={$_REQUEST['estado_his']}'>
			<img src='../../Imagenes/Eliminar.jpg' alt='Eliminar' width='13' height='13' border='0' />
			</a>" : "")
			."</td></tr>
			</table>
			</td>";
	}		   
	$tabla .= "</tr></table>";
	return $tabla;
}
/*function CrearTablaImagenExamenesPracticados($nEvolucion, $max = 4) {
	$tabla = "<table width='100%' align='center' class='LSPRESSE'><tr>";
	$rs = $this->select("*", "imagenes", "tabla = 'evoluciones' and nro_historia = '$nEvolucion'");
	$limite = $this->numRows($rs);
	for ($cont = 0, $close_tr = false; $cont < $limite; $cont++) {
		$row = $this->nextRow($rs);
		if ($cont % $max == 0 && $close_tr == false) {
			$tabla .= "<tr>";
			$close_tr = true;
		} else if ($cont % $max == 0 && $close_tr == true) {
			$tabla .= "</tr>";
			$close_tr = false;
		}
		$tabla .= 
			"<td width='25%' style='cursor:hand'>
			<table class='LSPRESSE' align='center'>
			<tr><td align='center'>
			<a href='{$row['ruta']}/{$row['nombre']}' target='_blank'>";
		$tipo_archivo = explode("/",$row['formato']);
		//print_r($tipo_archivo);
		if ($tipo_archivo[0]=="image"){
			$tabla .= "<img border='0' id='imagen$cont' name='imagen$cont' src='{$row['ruta']}/{$row['nombre']}' height='90' width='130'/>";
		}else if($row['formato']=="application/pdf"){
			$tabla .= "<img border='0' id='imagen$cont' name='imagen$cont' src='../../Imagenes/logo_pdf.png' height='90' width='130'/>";
		}
		
		$tabla .= "</a>
			</td></tr>
			<tr>
				<td><strong>{$row['nombre']}</strong></td>
			</tr>
			<tr><td align='right'>
			<a href='{$row['ruta']}/{$row['nombre']}' target='_blank'>
			<img src='../../Imagenes/errorpages.gif' alt='Ver Imagen Real' width='13' height='13' border='0' />
			</a>
			<a href='CtrlExamenes.php?operacion=del_imagen&num_imagen={$row['id']}&estado={$_REQUEST['estado']}&estudio={$_REQUEST['estudio']}&estado_his={$_REQUEST['estado_his']}'>
			<img src='../../Imagenes/Eliminar.jpg' alt='Eliminar' width='13' height='13' border='0' />
			</a>
			</td></tr>
			</table>
			</td>";
	}		   
	$tabla .= "</tr></table>";
	return $tabla;
}*/

function CrearTablaCamasDisponibles($max = 4) {
	$rs = $this->select("*", "sis_pabellon", "nombre = nombre ORDER BY codigo");
	$limite = $this->numRows($rs);
	$tabla = "";
	for ($i = 0, $close_tr = false; $i < $limite; $i++) {
		$pab = $this->nextRow($rs);
		$camas = $this->select("codigo, nombre, paciente, servicio, 
			(SELECT p.primer_nom + ' ' + p.segundo_nom + ' ' + p.primer_ape + ' ' + p.segundo_ape FROM sis_paci AS p WHERE paciente = p.autoid)", 
			"sis_cama", "pabellon = {$pab[0]}");
		$fin = $this->numRows($camas);
		if ($fin > 0) {
			$temp = $this->select("COUNT(*)", "sis_cama", "pabellon = {$pab[0]} AND paciente <> -1");
			$ocupadas = $this->nextRow($temp);
			$this->freeResult($temp);
			$tabla .= "<br>\n
				<table><tr>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.P{$pab[0]}.style.display == \"none\") { document.all.P{$pab[0]}.style.display = \"block\"; document.all.minus{$pab[0]}.style.display = \"block\"; document.all.plus{$pab[0]}.style.display = \"none\"; } else { document.all.P{$pab[0]}.style.display = \"none\"; document.all.minus{$pab[0]}.style.display = \"none\"; document.all.plus{$pab[0]}.style.display = \"block\";}'>
					<img style='display:none' name='minus{$pab[0]}' src='../../Imagenes/minus.gif' /><img style='display:block' name='plus{$pab[0]}' src='../../Imagenes/plus.gif' /></span>
				</td>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.P{$pab[0]}.style.display == \"none\") { document.all.P{$pab[0]}.style.display = \"block\"; document.all.minus{$pab[0]}.style.display = \"block\"; document.all.plus{$pab[0]}.style.display = \"none\"; } else { document.all.P{$pab[0]}.style.display = \"none\"; document.all.minus{$pab[0]}.style.display = \"none\"; document.all.plus{$pab[0]}.style.display = \"block\";}'>
					{$pab[0]} - \"{$pab[1]}\" --> CAMAS: $fin, OCUPADAS: {$ocupadas[0]}, DISPONIBLES: ".($fin - $ocupadas[0])."</span>
				</td>
				</tr></table>";
			$tabla .= "<div id='P{$pab[0]}' style='display:none; z-index: 1;'><table width='100%' align='center' class='LSPRESSE'>\n";
			$max_fila = ceil($fin / $max);
			for ($j = 0; $j < $max_fila; $j++) {
				$tabla .= "<tr>\n";
				for ($k = 0; $k < ($max < $fin ? $max : $fin); $k++) {
					$row = $this->nextRow($camas);
					if ($row != false) {
						$tabla .= "
							<td".($row[2] != -1 ? " bgcolor='#FF0000'" : " bgcolor='#55DDFF'").">
								<table width='80' height='50' class='LSPRESSE' align='center' style='cursor:hand' onMouseover='showtip2(this,event,\"{$row[4]}\")' onMouseout='hidetip2()'>
									<tr><td class='normalito' align='center'>{$row[1]}</td></tr>
								</table>
							</td>\n";
					} else {
						$tabla .= "<td>\n<table width='80%' align='center'><tr><td>&nbsp;</td></tr></table>\n</td>\n";
					}
				} 
				$tabla .= "</tr>\n";
			}
			$tabla .= "</table></div>\n";
			$this->freeResult($camas);
		}
	}
	
	return $tabla;
}


function CrearTablaCamasA($max = 4, $contrato = NULL, $uf = '') {
	$codigo_manual = $this->TxManual($contrato, "codigo");
	
	$pabellones = $this->getDato("pabellones",
		"ufuncionales", 
		"id = '{$uf}'");
	
	$rs = $this->select("*", "sis_pabellon", "codigo IN ({$pabellones})");
	$limite = $this->numRows($rs);
	$tabla = "";
	for ($i = 0, $close_tr = false; $i < $limite; $i++) {
		$pab = $this->nextRow($rs);
		$camas = $this->select("sc.codigo, sc.nombre, sc.paciente, sc.servicio, sc.Estado, EstadoCama.Color, sc.DetalleEstado", "camas_manuales AS cm, sis_cama as sc Outer Apply(Select Color From sis_cama_estados Where sc.Estado = Id) As EstadoCama", 
		"sc.codigo = cm.codigo_cama AND sc.pabellon = {$pab[0]} AND cm.manual = '$codigo_manual'");
		$fin = $this->numRows($camas);
		if ($fin > 0) {
			$ocupadas = $this->getDato("COUNT(*)", "camas_manuales AS cm, sis_cama as sc", 
				"sc.codigo = cm.codigo_cama AND pabellon = {$pab[0]} 
				AND cm.manual = '$codigo_manual' AND paciente <> -1");
			
			$tabla .= "<br>\n
				<table><tr>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.P{$pab[0]}.style.display == \"none\") { document.all.P{$pab[0]}.style.display = \"block\"; document.all.minus{$pab[0]}.style.display = \"block\"; document.all.plus{$pab[0]}.style.display = \"none\"; } else { document.all.P{$pab[0]}.style.display = \"none\"; document.all.minus{$pab[0]}.style.display = \"none\"; document.all.plus{$pab[0]}.style.display = \"block\";}'>
					<img style='display:none' name='minus{$pab[0]}' src='../../Imagenes/minus.gif' /><img style='display:block' name='plus{$pab[0]}' src='../../Imagenes/plus.gif' /></span>
				</td>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.P{$pab[0]}.style.display == \"none\") { document.all.P{$pab[0]}.style.display = \"block\"; document.all.minus{$pab[0]}.style.display = \"block\"; document.all.plus{$pab[0]}.style.display = \"none\"; } else { document.all.P{$pab[0]}.style.display = \"none\"; document.all.minus{$pab[0]}.style.display = \"none\"; document.all.plus{$pab[0]}.style.display = \"block\";}'>
					{$pab[0]} - \"{$pab[1]}\" --> CAMAS: $fin, OCUPADAS: {$ocupadas}, DISPONIBLES: ".($fin - $ocupadas)."</span>
				</td>
				</tr></table>";
			$tabla .= "<div id='P{$pab[0]}' style='display:none'><table width='100%' align='center' class='LSPRESSE'>\n";
			$max_fila = ceil($fin / $max);
			for ($j = 0; $j < $max_fila; $j++) {
				$tabla .= "<tr>\n";
				for ($k = 0; $k < ($max < $fin ? $max : $fin); $k++) {
					$row = $this->nextRow($camas);
					if ($row != false) {
						$colorOcupado = trim($row["Color"]) != "" ? $row["Color"] : "#FF0000";
						$infoAlert = (trim($row["DetalleEstado"]) != "" && is_numeric($row["Estado"])) ? $row["DetalleEstado"] : "No se puede asignar esta cama";
						$js = "updateDatos({$row[0]}, \"{$row[1]}\", \"{$pab[1]}\", \"{$row[3]}\")";
						$tabla .= "
							<td".(($row[2] != -1 || $row["Estado"] == 2 || $row["Estado"] == 3) ? " bgcolor='".$colorOcupado."'" : " bgcolor='#55DDFF'").">
								<table width='80' height='50' class='LSPRESSE' align='center' style='cursor:hand' onclick='javascript:".(($row[2] == -1 && $row["Estado"] != 2 && $row["Estado"] != 3) ? $js : " alert(\"".$infoAlert."\")")."' onMouseover='showtip2(this,event,\"".$this->getDato("primer_nom + ' ' + segundo_nom + ' ' + primer_ape + ' ' + segundo_ape", "sis_paci", "autoid = $row[2]")."\")' onMouseout='hidetip2()'>
									<tr><td class='normalito' align='center'>{$row[1]}</td></tr>
								</table>
							</td>\n";
					} else {
						$tabla .= "<td>\n<table width='80%' align='center'><tr><td>&nbsp;</td></tr></table>\n</td>\n";
					}
				} 
				$tabla .= "</tr>\n";
			}
			$tabla .= "</table></div>\n";
			$this->freeResult($camas);
		}
	}
	
	return $tabla;
}

function CrearTablaCamasReservar($max = 4) {
	
	$rs = $this->select("*", "sis_pabellon");
	$limite = $this->numRows($rs);
	$tabla = "";
	for ($i = 0, $close_tr = false; $i < $limite; $i++) {
		$pab = $this->nextRow($rs);
		$camas = $this->select("sc.codigo, sc.nombre, sc.paciente, sc.servicio, sc.Estado, EstadoCama.Color, sc.DetalleEstado", "sis_cama as sc Outer Apply(Select Color From sis_cama_estados Where sc.Estado = Id) As EstadoCama", "sc.pabellon = {$pab[0]}");
		$fin = $this->numRows($camas);
		if ($fin > 0) {
			$ocupadas = $this->getDato("COUNT(codigo)", "sis_cama as sc",  "pabellon = {$pab[0]} AND paciente <> -1");
			
			$tabla .= "<br>\n
				<table><tr>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.PR{$pab[0]}.style.display == \"none\") { document.all.PR{$pab[0]}.style.display = \"block\"; document.all.minusR{$pab[0]}.style.display = \"block\"; document.all.plusR{$pab[0]}.style.display = \"none\"; } else { document.all.PR{$pab[0]}.style.display = \"none\"; document.all.minusR{$pab[0]}.style.display = \"none\"; document.all.plusR{$pab[0]}.style.display = \"block\";}'>
					<img style='display:none' name='minusR{$pab[0]}' src='../../Imagenes/minus.gif' /><img style='display:block' name='plusR{$pab[0]}' src='../../Imagenes/plus.gif' /></span>
				</td>
				<td>
					<span class='letraDisplayNew' style='cursor:hand' onclick='if (document.all.PR{$pab[0]}.style.display == \"none\") { document.all.PR{$pab[0]}.style.display = \"block\"; document.all.minusR{$pab[0]}.style.display = \"block\"; document.all.plusR{$pab[0]}.style.display = \"none\"; } else { document.all.PR{$pab[0]}.style.display = \"none\"; document.all.minusR{$pab[0]}.style.display = \"none\"; document.all.plusR{$pab[0]}.style.display = \"block\";}'>
					{$pab[0]} - \"{$pab[1]}\" --> CAMAS: $fin, OCUPADAS: {$ocupadas}, DISPONIBLES: ".($fin - $ocupadas)."</span>
				</td>
				</tr></table>";
			$tabla .= "<div id='PR{$pab[0]}' style='display:none'><table width='100%' align='center' class='LSPRESSE'>\n";
			$max_fila = ceil($fin / $max);
			for ($j = 0; $j < $max_fila; $j++) {
				$tabla .= "<tr>\n";
				for ($k = 0; $k < ($max < $fin ? $max : $fin); $k++) {
					$row = $this->nextRow($camas);
					if ($row != false) {
						$colorOcupado = trim($row["Color"]) != "" ? $row["Color"] : "#FF0000";
						$infoAlert = (trim($row["DetalleEstado"]) != "" && is_numeric($row["Estado"])) ? $row["DetalleEstado"] : "No se puede asignar esta cama";
						$js = "fnSelectCama(".$row[0].")";
						if($row["Estado"] == 2 || $row["Estado"] == 3){
							$js2 = $js;
						}else{
							$js2 = " alert(\"".$infoAlert."\")";
						}
						
						if(!($row[2] != -1 || $row["Estado"] == 2 || $row["Estado"] == 3)){
							$infoAlert = "";
						}
						
						$tabla .= "
							<td".(($row[2] != -1 || $row["Estado"] == 2 || $row["Estado"] == 3) ? " bgcolor='".$colorOcupado."'" : " bgcolor='#55DDFF'").">
								<table id='cama_".$row[0]."' width='80' height='50' class='LSPRESSE' align='center' style='cursor:hand' onclick='javascript:".(($row[2] == -1 && $row["Estado"] != 2 && $row["Estado"] != 3) ? $js : $js2)."' title='".$infoAlert."'>
									<tr><td class='normalito' align='center'>{$row[1]}</td></tr>
								</table>
							</td>\n";
					} else {
						$tabla .= "<td>\n<table width='80%' align='center'><tr><td>&nbsp;</td></tr></table>\n</td>\n";
					}
				} 
				$tabla .= "</tr>\n";
			}
			$tabla .= "</table></div>\n";
			$this->freeResult($camas);
		}
	}
	
	return $tabla;
}


function ActualizarMovStock($servicio,$comprobante,$movi,$tipo,$ccosto,$codigo,$cantidad,$estudio){
	$cant = $movi == "EN" ? $cantidad * $this->DameDosis($codigo) : $cantidad;
	//$servicio = $this->obtenerSiguiente('id_mov_stock');
	$tabla = "movStock (servicio,comprobante,fuente,$tipo,fecha,articulo,cantidad,estudio,usuario)";
	$columnas = "'$servicio','$comprobante','$movi','$ccosto',{fn NOW()},'$codigo',$cant,$estudio,'{$_SESSION['codigo_user']}'";
	$this->insert_into($tabla,$columnas,'','','');
	//$this->establecerSiguiente('id_mov_stock');
}

function ActualizarStockDosificadado($ccosto,$codigoMedic,$cantidad,$tipo){
	$cant = $cantidad * $this->DameDosis($codigoMedic);
	$tabla = "stockDosificado (codigo,ccosto,$tipo)";
	$columnas = "'$codigoMedic','$ccosto',$cant";
	$update = "$tipo = $cant + $tipo";
	$this->insert_into($tabla,$columnas,'|',"codigo = '$codigoMedic' AND ccosto = '$ccosto'",$update);
	$this->Update("stockDosificado","existencia = entrada - (salida + debaja) ");
}


function DameDosis($codigo){
return $this->getDato("dosificacion","sis_prod","codigo = '$codigo'");
}

function getLotesArticulos($codigo, $bodega="%%", $ex_mayor="", $show = NULL) {
    global $db;
	
	if($ex_mayor!=""){
		$ex_mayor=" and e.existencia>0 ";
	}
	
	$sql="select e.existencia, e.cod_lote from existencia as e where e.cod_prod='".$codigo."' and e.cod_stock='".$bodega."' ".$ex_mayor;
		 

    $result = $db->query($sql);
    //echo $sql;
	if ($show != NULL) {
		echo "$SQL<BR>";
	}

    if( $result->numRows() > 0 )
		return $result;
    else
        return null;
}

function getFechaVenceLotes($articulo, $lote) {
    global $db;

    return $this->getDato("fecha_vence", "lote", "articulo LIKE '$articulo' AND numero LIKE '$lote'");
}

function getFormatosHistorias($show = NULL) {
    global $db;
    
    return $this->select("codigo, detalle", "hctiphis", "esFormato LIKE '1'", $show);
}

function isFormatoDeHistoria( $formato, $historia ) {
    return $this->getDato("COUNT(*)", "formatos_hist", "formato LIKE '$formato' AND historia LIKE '$historia'");
}

function insertarFormatoHistoria( $formato, $historia ) {
    if( !isFormatoDeHistoria( $formato, $historia ) ) {
        $sql = "INSERT INTO formatos_hist (formato, historia) VALUES('$formato', '$historia')";
        $this->insertar($sql);
    }
}

function getNombreTipoEstudio($tipo) {
	switch ($tipo) {
		case "A": return "CONSULTA EXTERNA";
		case "H": return "HOSPITALIZACION";
		case "U": return "URGENCIA";
	}
}

function NumeroRegistros($tabla, $where = NULL){
$sql = $where != NULL ? "select count(*) as registros from $tabla  where $where " : "select count(*) from $tabla" ;
$rs = $this->Execute($sql);
return $rs[0][0];
} 



function CrearTablaCitas($medico,$tiempo, $sql, $rs = NULL){
/*****************************************************************************************************
echo "Fecha Inicial ".date("d/m/Y H:i:s",$fecha)."<br>";
$periodo = date("d/m/Y H:i:s",strtotime("+1 minutes",$fecha));
echo "Fecha Final $periodo";
/*****************************************************************************************************/
$tabla = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
<td width="13%" height="21" align="center" class="LSBOTDHOVER"><span class="norm">Turno</span></td>
<td width="13%" height="21" align="center" class="LSBOTDHOVER"><span class="norm">Hora</span></td>
<td width="13%" height="21" align="center" class="LSBOTDHOVER"><span class="norm">Estado</span></td>
<td width="13%" height="21" align="center" class="LSBOTDHOVER"><span class="norm">Paciente - Asunto</span></td></tr>';
$i=1;
$hora ='';
$result = $rs != NULL ? $rs : $this->query($sql);
while(($row = $this->nextRow($result))){
	//echo "Entro...".$i;
	$hora   = $hora == '' ? $this->DameHora($row['hora']) : $hora + (mktime(0,15,0) - mktime(0,0,0));
	$dia    = $row['meridiano'] == 1 ? "am" : "pm" ;
	if($row['estado'] == 0)
	{ $estado =  '<a target="bottomFrame" href="../Facturacion/CtrlAdmitir.php?operacion=confirmar_cita&id='.$row['id'].'">Sin Confirmar</a>';}
	elseif($row['estado'] == 1)
	{ $estado =  '<span class="letraDisplay">Confirmada</span>';}
	elseif($row['estado'] == 2)
	{ $estado =  '<span class="letraDisplay">Atendida</span>';}
	elseif($row['estado'] == 3)
	{ $estado =  '<span class="letraDisplay">Cancelada</span>';}
	$tabla .='<tr style="cursor:hand" onmouseover="bgColor=\'#EEEEEE\'" onmouseout="bgColor=\'#FFFFFF\'" bgcolor="#FFFFFF">
	<td width="13%" height="21" align="center">'.$i.'</td>
	<td width="13%" height="21" align="center">'.$row['hora'].' '.$dia.'</td>
	<td width="13%" height="21" align="center">'.$estado.'</td>
	<td width="85%"align="center" alt="'.$row['observaciones'].'"><a href="CtrCitas.php?transaccion=Listar&id='.$row['id'].'" class="letraDisplay">'.$row['primer_nom']." ".$row['segundo_nom']." ".$row['primer_ape']." ".$row['segundo_ape']. ' - '.$row['asunto'].'</a></td>
	</tr>';
	$_SESSION['periodo'] = $row['periodo'];
	$i++;
} //onClick="javascript:window.location.href=\'CtrCitas.php?transaccion=Listar&id='.$row['id'].'\'
$tabla .= '</table>';
return $tabla;
}

function getConsecutivoCitas($medico, $fecha){
$sql = "select max(turno) from sis_citas where cod_medi = '$medico' and fecha = '$fecha' order by turno";
$row = $this->Execute($sql);
return $row;
}

function BoletoEntrada($empresa, $nit) {
	global $db;
	
	$rs = $db->query("select remoteUser, remotePassword, remoteDatabase, remoteHost, r_social from seriales where nit = '$nit'");
	if ($rs->numRows() > 0) {
		$row = $rs->nextRow();
		$_SESSION['cliente']  = $row[0];
		$_SESSION['clave']    = $row[1];
		$_SESSION['database'] = $row[2];
		$_SESSION['host']     = $row[3];
		$_SESSION['rsocial']  = $row[4];
		//echo $_SESSION['cliente']." - ".$_SESSION['clave']." - ".$_SESSION['database']." - ".$_SESSION['host'];
		$rs->free();
		$tx = true;
	} else {
		$tx = false;
	}
	$db->disconnect();
	
	$db->setHost($_SESSION['host']);
	$db->setDatabase($_SESSION['database']);
	$db->setUser($_SESSION['cliente']);
	$db->setPassword($_SESSION['clave']);
	
	$db->connect();
	
	return $tx;
}

function perfilActores($cedula) {
	$r = $this->getDato("COUNT(*)", "sis_medi", "cedula = '$cedula'");
	return $r > 0 ? "medico" : "";
}

function IsExec(){
 return	$this->affectedRows();
}

function DameFecha($f){
		$anhio = substr($f, 6, 9);
		$mes   = substr($f, 3, 2);
		$dia   = substr($f, 0, 2);
		$fecha = mktime(0, 0, 0, $mes, $dia, $anhio);
		return $fecha;
}


function DameHora($f){
		$hora     = substr($f, 0, 2);
		$minuto   = substr($f, 3, 2);
		$segundo  = substr($f, 7, 2);
		$horas    = mktime($hora, $minuto, $segundo);
		//echo "$hora - $minuto - $segundo"; 
		return $horas;
}

function Delete($tabla, $where,$show=NULL ) {
	global $db;
	$sql  = "Delete From $tabla where $where";	
	if($show==1){
		echo $sql;
		}
	return $db->query($sql) or die("Transaccion Delete : <br> No se puedo Realizar la Transaccion $sql");	
}

function DameProducto($caso = 'all', $dato = NULL){
	switch($caso){
	case 'all': 
	$sql = "select a.codigo,upper(a.descripcion) as descripcion,ffarma,f.descripcion as des_formas,dosificacion,cod_med,cod_anat,cod_adm,grupo,b.descripcion as des_grupos,linea,c.descripcion as des_lineas,marca,d.descripcion as des_marca,unimed,e.descripcion as des_medida,concentracion,tipo,modalidad,vigencia,costo,institucional,eps,iva,cif ".
		   " from sis_prod as a ,grupo as b, lineas as c, marcas as d, medidas as e, formas as f ". 
		   " where a.grupo = b.codigo and a.linea = c.codigo and a.marca = d.codigo and a.unimed = e.codigo group by a.codigo";
	break;
	case 'codigo':
	$sql = "select a.codigo,upper(a.descripcion) as descripcion,ffarma,f.descripcion as des_formas,dosificacion,cod_med,cod_anat,cod_adm,grupo,b.descripcion as des_grupos,linea,c.descripcion as des_lineas,marca,d.descripcion as des_marca,unimed,e.descripcion as des_medida,concentracion,tipo,modalidad,vigencia,costo,institucional,eps,iva,cif ".
		   " from sis_prod as a ,grupo as b, lineas as c, marcas as d, medidas as e, formas as f ". 
		   " where a.codigo = '$dato' and a.grupo = b.codigo and a.linea = c.codigo and a.marca = d.codigo and a.unimed = e.codigo group by a.codigo";
	break;	   
	}//swicth
	
echo $sql;
$rs = $this->Execute($sql);	
return $rs;
}//function


function guardarI($r){
// DELETE FROM nombre_tabla WHERE condici�n 
	$sql  = "UPDATE seriales SET logo = '$r'";	
	//echo "$sql";
	$this->query($sql);
}

function Command($sql,$host,$database,$cliente,$clave){
	$new = $this->Connect($host,$database,$cliente,$clave);
	try{
		$rs = $this->query($sql);
	}catch(Exception $e){
		die($e->getMessage());
	}
	return $new;
}

function existeHistoriaAbierta($historia,$tipo_historia) {
/*	
	$num = $this->getDato("COUNT(*)", "hcingres", "nro_historia = '$historia' AND estado = 'A' AND tipo_histo='$tipo_historia'");*/
	if($tipo_historia=="-1"){return false;}
	$num = $this->getDato("COUNT(*)", "hcingres", "nro_historia = '$historia' AND estado = 'A'");
	
	/*$num = $this->getDato("COUNT(*)", "hcingres", "nro_historia = '$historia' AND estado = 'A'");*/
	return $num > 0 ? true : false;
}

/*
function CrearCliente($parametro) {
	exec("mysqladmin -u root -padmin --host=192.168.0.142 create $parametro");
	exec("mysqldump -u root -padmin --host=192.168.0.142 --opt dbsismanet | mysql -u root -padmin --host=192.168.0.142 $parametro");
	//exec("C:\exec_mysql $parametro");
	return 'ejecutado';
}

function CrearUsuario($DataName,$NewUsuario,$NewCode,$nombre,$cedula,$email){
	$new = $this->Connect($_SESSION['host'],$DataName,$_SESSION['cliente'],$_SESSION['clave']);
	$sql = "'$NewUsuario','$cedula','$nombre','0','$NewCode','1','".fecha_hora_actual()."','remota','$email'";
	$command = "insert into usuario values ($sql)";
	$this->query($command) or die ("Excepcion: <br> $command");
	mysql_close($new);
}

function crearSerial($database, $nit, $tipo, $rsocial, $id, $nombre, $direccion, $ciudad, $email, $telefono, $id, $departamento, $contenido) {
	$new = $this->Connect($_SESSION['host'],$database,$_SESSION['cliente'],$_SESSION['clave']);
	$SQL = "INSERT INTO seriales 
		(nit, tipoid, r_social, codigo_eps, repre_lega, direccion, ciudad, web, e_mail, 
		aa, telefono, serial_aut, depto, fecha, rutwin, coment,	resolucion, interfazconta, 
		logo, enlace)
		VALUES
		('$nit', '$tipo', '$rsocial', '$id', '$nombre', '$direccion', '$ciudad', 'default',
		'$email', '', '$telefono', '$id', '$departamento', { fn NOW() }, '', '', '$id', '',
		'$contenido', '')";
	$this->query($SQL, $new);
	mysql_close($new);
}
*/

function insert_into($tabla,$sentencia,$requerido,$where,$sentenciaUp,$show=NULL){
    	global $db;
		
		if($requerido == "|")
		 {	$x = stripos($tabla,'(');
			$tablax = $tabla;
			if($x >= 1 ){
			$tablax = substr($tabla,0,$x); }
			$sw = $this->Existe($tablax,$where,$show);
			if($sw == 0)
			{   //echo "Insertando...<br>";
				$sql = "INSERT INTO $tabla values ($sentencia)";	
			    //echo $sql;
				$tx = $db->execute($sql) >= 1 ? 1 : 0;
			}else{
			//echo " -+ Actualizando +-";
			$this->Update($tabla,$sentenciaUp,$where,$show);
			$tx =true;}
		} else {
				$sql = "INSERT INTO $tabla values ($sentencia)";
				//echo "$sql<br>";
				$tx = $db->query($sql) or die ("<b>Transaccion Insert Into : <br> No se Puedo Insertar los Datos<BR><CENTER>".mysql_error()."</CENTER>  </b>");
		}	
	//	echo "$sql<br>";
	if ($show != NULL)
			echo "$sql<br>";
			
    return $tx;		
}

function obtCCFarmacia() {
	global $db;
	
	$rs = $db->select("codigo", "sis_costo", "es_farmacia = 1");
	$r = $rs->nextRow();
	$rs->free();
	
	return $r[0];
}

function obtCCVentasP() {
	global $db;
	
	$rs = $db->select("codigo", "sis_costo", "es_ventasp = 1");
	$r = $rs->nextRow();
	$rs->free();
	
	return $r[0];
}

function obtManualVentasP() {
	global $db;
	
	$rs = $db->select("codigo", "sis_manual", "tipo = 'INST'");
	$r = $rs->nextRow();
	$rs->free();
	
	return $r[0];
}

function obtSFarmacia($estudio) {
	global $db;
	
	$tipo_estudio = $this->getDato("tipo_estudio", "sis_maes", "con_estudio = $estudio");
	
	/*$rs = $db->select("t.fuente", "sis_tipo AS t, sis_maes AS m, sis_contrato AS c", 
		"t.filerips = 'AM' AND t.fuente = c.servicio AND c.codigo = m.contrato 
		AND m.con_estudio = $estudio AND c.$tipo_estudio = 1");*/
	
	$rs = $db->select("t.fuente", "sis_tipo AS t 
		INNER JOIN sis_contrato AS c ON t.fuente = c.servicio
		INNER JOIN sis_maes AS m ON m.contrato = c.codigo
		INNER JOIN ufcuenta AS ufc ON c.ufuncional = ufc.id", 
		"t.filerips = 'AM' AND ufc.codigo_uf = m.ufuncional AND t.fuente = ufc.codigo_ser 
			AND m.con_estudio = $estudio AND c.{$tipo_estudio} = 1 ");
	
	$r = $rs->nextRow();
	$rs->free();
	
	return $r[0];
}

function obtCCServicio($codigo) {
	global $db;
	
	$rs = $db->select("ccosto", "sis_tipo", "fuente = $codigo");
	$r = $rs->nextRow();
	$rs->free();
	
	return $r[0];
}

function isFormatoHC( $codigo ) {
    global $db;

    $rs = $db->select("esFormato", "hctiphis", "codigo LIKE '$codigo'");
    $r = $rs->nextRow();
    
    return ($r[0] == "1");
}

function getHMFormatos() {
    global $db;

    $rs = $db->select("codigo, detalle, Nadmon, ImprimeOdon, esFormato", "hctiphis", "esFormato LIKE '1'");
    return $rs;
}

function CrearListadoCompras($nombre){
	$sql = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = '$nombre' AND xtype = 'U')
		CREATE TABLE $nombre(      
		prov varchar(10) default NULL,         
		articulo varchar(10) default NULL,     
		referencia varchar(10) default NULL,   
		fecha varchar(15) default NULL,        
		cant_solicitada int(10) default NULL,             
		descripcion varchar(50) default NULL,  
		medida varchar(50) default NULL,       
		costo int(10) default NULL,            
		iva int(3) default NULL,               
		total int(10) default NULL)";
	$resultid = $this->query($sql);
	return $resultid;
} 



function AlterTable($tabla,$columna,$tipo = 'BIGINT') {
	global $db;
	$sql = "ALTER TABLE $tabla ADD $columna $tipo";
	// No muestra el error
	$row = @$db->execute($sql);
	//echo $sql;
	return $row > 0 ? 1 : 0;
}

function  CrearTablaPlan($nombre){
	$sql = "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = '$nombre' AND xtype = 'U')
		CREATE TABLE $nombre (                    
		id varchar(10) default NULL,           
		tratamiento varchar(10) default NULL,  
		nombre varchar(50) default NULL,       
		evento varchar(100) default NULL       
		)";
	$result = $this->query($sql) or die ("<b>Transaccion CrearTablaPlan : <br> No se Puedo Crear la Tabla </b>");
	return $result; 
}    


function CrearTablaCirugias($nombre){
	//$SQL = "DROP TABLE $nombre";
	//$this->query($SQL);
	$SQL = "CREATE TABLE $nombre (
		count int default '1',
		id varchar(10) default '0',
		realizacion int default '0',
		liquidado int default '0',		
		num int default '0',
		nomcirugia varchar(200) default NULL,
		codproc varchar(10) default NULL,
		nomproc varchar(150) default NULL,
		tipo char(2) default 'A',
		valor int default NULL,
		servicio int default NULL,
		nomservicio varchar(50) default NULL,
		dx_principal varchar(10) default NULL,
		dx_relacionado varchar(10) default NULL,
		dx_complicacion varchar(10) default NULL )
		";
	$result = $this->query($SQL) or die ("<b>Transaccion CrearTablaCirugias : <br> No se Puedo Crear la Tabla </b>");
	return 1;  
}    


function DropTabla($nombre){
	$sql = "if not objectproperty(object_id('$nombre'),'IsTable') is null
			drop table $nombre ";
	$result = $this->query($sql) or die ("<b>Transaccion DropTabla : <br> No se Puedo Eliminar la Tabla $nombre </b>");
	return $result; 
}	   
	   
function DatosCirugia($id,$case,$tabla){
	switch($case){
	
	case 'num' :
	$sql = "SELECT num FROM $tabla WHERE ID = '$id'";
	$rs = $this->Execute($sql);break;
	
	case 'nomcirugia' :
	$sql = "SELECT nomcirugia  FROM $tabla WHERE ID = '$id'";
	$rs = $this->Execute($sql);break;
	
	case 'total':
	$sql = "SELECT sum(valor)as total FROM $tabla WHERE ID = '$id'";
	$rs = $this->Execute($sql);break;

	case 'nCirugias':
	$sql = "SELECT max(num) as nCirugias FROM $tabla ";
	$rs = $this->Execute($sql);break;
	
	case 'all':
	$sql = "SELECT * FROM $tabla WHERE ID = '$id'";
	$rs = $this->Execute($sql);break;
	
	case 'pack':

	}
	return $rs;
}

function liquicionCirugias($codmanual, $tipoCirugia, $nCirugia, $tipo){
	global $db;
	
	$sql = "select medico,anestesiologo,ayudante,dsala,materiales from liquidacion ".
			"where resolucion = '$codmanual' and liquidacion = $tipoCirugia and cirugia = $nCirugia and tipo ='$tipo' ";
	//echo $sql;
	$result = $db->query($sql) or die ("MOdel<br>Transaccion LiquidacionCirugias: <br>No se puedo realizar la Transaccion $sql ");
    $row = $result->nextRow();
	return $row;
}

	   
function Paginacion($sql){
	$tokens = explode('From', $sql);
	$token_count = count($tokens);
	$sql = "select count(*)/50 from ".$tokens[1];
	$result=$this->query($sql);
	$row = $this->nextRow($result);
	return $row;
}


function DameReport($codreport){
	global $db;
	$sql    = " SELECT * FROM sis_report WHERE codigo='$codreport'";
	//echo $sql;
	$result = $db->query($sql);
	$row    = $this->nextRow($result);
	$this->freeResult($result);
return $row;
}



function TxManual($dato, $tipo){
switch($tipo){	

case 'tipoxnombre': 
				$sql = 	"select tipo from sis_manual where nombre = '$dato'" ;
				$resultid = $this->query($sql);
				$row = $this->nextRow($resultid);
				$dato = $row['tipo'];
				
				if( ($dato == '256') || ($dato == '312') || ($dato == '200')){
				 $tx = "iss";
				}elseif($dato == 'SOAT'){
				 $tx = "soat";
				}else{
				 $tx = "cups";
				} 
				break;


case 'codigo': 	$sql = "select m.codigo from sis_manual as m,contratos AS c where c.codigo = '$dato' and m.codigo = c.manual";
				$resultid = $this->query($sql);
				$row = $this->nextRow($resultid);
				$tx  = $row['codigo'];break;
				
case 'tipoxcodigo':
		 		$sql = 	"select tipo from sis_manual where codigo = '$dato' " ;
				//echo "$sql<br>";
				$resultid = $this->query($sql);
				$row = $this->nextRow($resultid);
				$tx = $row['tipo'];
				break;
}//swicth

return $tx;	
} // Function


function DameProc($tipo,$id_manual,$codproc){
$sql = "select codigo,nombreve,fuente,tipo_proc,formulado,rips,uvr,manual_$id_manual from sis_proc 
		where tipo = '$tipo' and codigo='$codproc' ";
//echo $sql;
return $this->Execute($sql);
}



function DameProd($id_manual,$codprod){
$sql =  "select p.codigo,cod_med,cod_anat,cod_adm,forma_pre,concent,nombre,uni_med,grupo,tipo_usu,paquete,manual_$id_manual
		 from sis_prod as p ,sis_medica as m 
		 where p.codigo = '$codprod' and m.codigo = '$codprod'  group by p.codigo";
//echo $sql;		 
return $this->Execute($sql);
}

function ActualizarCopagos($estudio) {
$sql = "select sum(valor)as valor from pagos where con_estudio = $estudio AND tipo = 3"; // valor total pagado por el paciente
$resultid = $this->query($sql);
$row = $this->nextRow($resultid);
$row[0] = empty($row[0]) ? 0 : $row[0];
$this->Update("sis_maes", "vlr_descto = {$row[0]}", "con_estudio = $estudio"); 
}

function calcularNetoFactura($estudio) {
	
	$rs = $this->query("[dbo].[colocarTotalesFacturanew] @estudio ='$estudio', @calcular ='S'");
	if(is_object($rs)){
		$r = $rs->nextRow();
		$neto = $r['neto'];
	}else{
		$neto = 0;
	}
	
	/*$netoTemp = $this->getDato("ISNULL(vlr_neto, 0)", "sis_maes", "con_estudio = $estudio");
	if($netoTemp > 0){
		return $netoTemp;
	}
	
	$total = $this->getDato("SUM(total)", "sis_deta", "estudio = $estudio");
	$total_cobra_copago = $this->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");
	
	$row = $this->select("subsidio, maximo, vlr_coopago", "sis_extraxemp, sis_maes", 
			"estrato = cod_clasi AND empresa = cod_entidad AND con_estudio = $estudio", NULL, 1);
			
	$tipoContrato = urlencode($this->getDato("descripcion", "FacturasTiposContratos", "estudio = $estudio"));
	$tipo_con=$this->getDato("tipo_contrato","contratos","codigo='".$this->getContrato($estudio)."'");	
			
	$subsidio = $row[0];
	$maximo = $row[1];
	$vlr_copago = $row[2];
	
	$descuento = $this->getDato("SUM(valor)", "pagos", "con_estudio = $estudio AND tipo = 3");
	$asume_desc = $this->getDato("emp_asume_desc", "sis_empre, sis_maes", 
			"con_estudio = $estudio AND cod_entidad = sis_empre.codigo");
	$sub=$subsidio;
	$max=$maximo;
	
		if ($sub == -1) {
		$copago = $vlr_copago;
		$subsidio = $sub;
	} elseif($sub == 0){ //Cobro Pleno de lo facturado
		$copago = $total_cobra_copago;
		$subsidio = $sub;
	}else 	if ($sub <= 100) {
		$copago = round($total_cobra_copago * (100 - $sub) / 100,-2);
		$subsidio = $sub;
	} else {
		//$copago = round(($total_cobra_copago == 0) ? 0 : $sub,-2);
		$copago =$sub;
		$subsidio = $sub;
	}
	
	if ($max != 0 && $copago > $max) {
		
		$copago = $max;
	}
	
	if($tipo_con==5){
	$copago=$total_cobra_copago;
}

			  if($asume_desc){
			  if($tipo_con==5){
			  $tb=number_format(((float)$total -(float)$descuento), 0, '.', '.');				
			  }		  
			  else{
				 $tb=number_format((float)$total -((float)$copago-(float)$descuento), 0, '.', '.');
				  }
		  }else{
			  $tb=number_format(((float)$total -(float)$copago), 0, '.', '.');				  
		  }

	// ASUME = 1, NO ASUME = 2
	$neto = $tb;
	//$asume_desc == 1 ? $total - ($copago - $descuento) : $total - $copago;*/
	return $neto;
}

function convertirFechaEdad($fecha_naci) {
	if (is_null($fecha_naci) || empty($fecha_naci)) {
		return;
	}
	
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$a = strtok ($fecha_naci,"-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	if ($anio < $a || ($anio == $a && $mes < $m) || ($anio == $a & $mes == $m & $dia < $d)) {
		return -1;
	}
	
	if ($anio == $a) {
		if ($mes == $m) {
			$edad = $dia - $d;
			return "$edad dia".($edad == 1 ? "" : "s");
		} else {
			$edad = $mes - $m;
			if ($edad == 1 && $dia < $d) {
				$edad = numeroDiasMes($a, $m) - $d + $dia;
				return "$edad dia".($edad == 1 ? "" : "s");
			} else if ($dia < $d) {
				$edad--;
			}
			return "$edad mes".($edad == 1 ? "" : "es");
		}
	} else {
		$edad = $anio - $a;
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))) {
			$edad = 12 - $m + $mes - ($mes < $m && $dia < $d ? 1 : 0);
			return "$edad mes".($edad == 1 ? "" : "es");
		} else if ($mes < $m || ($mes == $m && $dia < $d)) {
			$edad--;
		}
		return "$edad a&ntilde;o".($edad == 1 ? "" : "s");
	}
}

function calcularCopago($estudio) {
	$rs = $this->RSAsociativo("SELECT dbo.fnGetCopagoPagar(".(int)$estudio.") AS Copago");
	return ($rs[0]["Copago"]);
}

function colocarNetoyValorEnLetra($estudio) {
	$row = $this->nextRow($this->select("sum(total)", "sis_deta", "estudio = $estudio"));
	$total = $row[0];
	$row = $this->nextRow($this->select("subsidio, maximo, vlr_coopago", "sis_extraxemp, sis_maes", "estrato = cod_clasi AND empresa = cod_entidad AND con_estudio = $estudio"));
	$subsidio = $row[0];
	$maximo = $row[1];
	$vlr_copago = $row[2];
	$row = $this->nextRow($this->select("sum(valor)", "pagos", "con_estudio = $estudio AND tipo = 3"));
	$descuento = empty($row[0]) ? 0 : $row[0];
	$descuento = $descuento;
	
	$total_cobra_copago = $this->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");

	if ($subsidio == -1) {
		$copago = $vlr_copago;
	} else 	if ($subsidio <= 100) {
		$copago = round($total_cobra_copago * (100 - $subsidio) / 100,0);
	} else {
		$copago =($total_cobra_copago == 0) ? 0 : $subsidio;
	}
	
	if ($maximo != 0 && $copago > $maximo) {
		$copago = $maximo;
	}

	$neto = $this->calcularNetoFactura($estudio);
	$letras = convertirNumero2Letra($neto);
	$sql = "UPDATE sis_maes	SET valor_letra = '$letras', vlr_descto = $descuento, vlr_neto = $neto WHERE con_estudio = $estudio"; 
	$this->insertar($sql);
}

public function getSpId()
{
	return $this->RSAsociativo('SELECT @@SPID SpId')[0]['SpId'];
}

function colocarTotalesFactura($estudio) {
	$row = $this->nextRow($this->select("sum(total)", "sis_deta", "estudio = $estudio"));
	$total = empty($row[0]) ? 0 : $row[0];
	$row = $this->nextRow($this->select("subsidio, maximo, vlr_coopago", "sis_extraxemp, sis_maes", "estrato = cod_clasi AND empresa = cod_entidad AND con_estudio = $estudio"));
	$subsidio = $row[0];
	$maximo = $row[1];
	$vlr_copago = empty($row[2]) ? 0 : $row[2];
	$row = $this->nextRow($this->select("sum(valor)", "pagos", "con_estudio = $estudio AND tipo = 3"));
	$descuento = empty($row[0]) ? 0 : $row[0];
	$descuento = $descuento;
	
	$total_cobra_copago = $this->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");

	if ($subsidio == -1) {
		$copago = $vlr_copago;
	} else 	if ($subsidio <= 100) {
		$copago = round($total_cobra_copago * (100 - $subsidio) / 100,0);
	} else {
		$copago = ($total_cobra_copago == 0) ? 0 : $subsidio;
	}
	
	if ($maximo != 0 && $copago > $maximo) {
		$copago = $maximo;
	}

	$neto = $this->calcularNetoFactura($estudio);
	$letras = convertirNumero2Letra($neto);
	$sql = "UPDATE sis_maes	SET vlr_factura = $total, vlr_descto = $descuento, vlr_coopago = $copago, valor_letra = '$letras', vlr_neto = $neto WHERE con_estudio = $estudio"; 
	$this->insertar($sql);
}

/*function colocarTotalesFacturaPorNumero($numero) {
	$row = $this->nextRow($this->select("sum(d.total)", 
		"sis_deta AS d, sis_maes AS m", "m.con_estudio = d.estudio AND m.nro_factura = $numero"));
	$total = $row[0];
	$row = $this->nextRow($this->select("subsidio, maximo", "sis_extraxemp, sis_maes", "estrato = cod_clasi AND empresa = cod_entidad AND nro_factura = $numero"));
	$subsidio = $row[0];
	$maximo = $row[1];
	$row = $this->nextRow($this->select("sum(p.valor)", "pagos AS p, sis_maes AS m", "m.con_estudio = p.con_estudio AND m.nro_factura = $numero AND tipo = 3"));
	$descuento = $row[0];

	if ($subsidio == -1) {
		$copago = 0;
	} else 	if ($subsidio <= 100) {
		$copago = $total * (100 - $subsidio) / 100;
	} else {
		$copago = ($total == 0) ? 0 : $subsidio;
	}
	
	if ($maximo != 0 && $copago > $maximo) {
		$copago = $maximo;
	}

	$neto = $this->calcularNetoFactura($estudio);
	$letras = EnLetras($neto);
	$letras;
	$sql = "UPDATE sis_maes	SET vlr_factura = $total, vlr_coopago = $copago, valor_letra = '$letras', vlr_neto = $neto WHERE con_estudio = $estudio"; 
	$this->insertar($sql);
}*/

function ActionProc($tabla,$columna,$dato){
$this->Connect($_SESSION['host'],$_SESSION['database'],$_SESSION['cliente'],$_SESSION['clave']);
switch ($tabla){
	case "sis_tipo":	$sql ="select $columna from sis_tipo where fuente == $dato";
						$result = $this->query($sql);
						$row = $this->nextRow($result);
						$dato = $row[$columna];
						return $dato;
	}
}

function AlterDropTable($tabla,$columna){
	global $db;
	$sql = "ALTER TABLE $tabla DROP COLUMN $columna";
	$resultid = $db->execute($sql);
} 


function Existe($tabla, $where, $show = NULL){
	global $db;
	
	$sql = "select top(1) * from $tabla where $where";
	$rs = $db->query($sql);
		if ($show != NULL)
			echo "$sql<br>";

	return $rs->numRows() >= 1 ?  true : false;
}

function Registrate($nom, $cod) {
	include_once('constantes.php');
	
	$nom = trim(strtoupper($nom));
	$cod = trim(strtoupper($cod));
	
	/*redundancia en la validacion del usuario
	$rs = $this->select("COUNT(*)", "usuario", "UPPER(usuario) = '$nom'");
	$existe = $this->nextRow($rs);
	$this->freeResult($rs);
	
	if ($existe[0] > 0) {*/
		
		$rs = $this->select("COUNT(*),usuario_sys", "usuario", "(UPPER(codigo) = '$cod' AND UPPER(usuario) = '$nom' AND usuario_sys=0) OR (UPPER(usuario) = '$nom' AND usuario_sys=1) group by usuario_sys");
		$existe = $this->nextRow($rs);
		$this->freeResult($rs);
		
		if($existe["usuario_sys"]=="1"){
			switch($this->validEstructuraClaveSoporte()){
				case "1":
					return ERR_CONTRASENIA_INCORRECTA;
				break;
				case "2":
					return LOGIN_OK;
				break;
				case "3":
					return ERR_TIEMPO_VENCIDO;
				break;
			}
			
		}
		return $existe[0] > 0 ? LOGIN_OK : ERR_CONTRASENIA_INCORRECTA;
	/*} else {
		return ERR_USUARIO_NO_EXISTE;
	}*/
}	
 
 function validEstructuraClaveSoporte(){
	 $ClaveSoporte=$_SESSION["ClaveSoporte"];
	 $ano=substr($ClaveSoporte,0,4);
	 $mes=substr($ClaveSoporte,4,2);
	 $dia=substr($ClaveSoporte,6,2);
	 
	 $fecha=substr($ClaveSoporte,0,4).'/'.substr($ClaveSoporte,4,2).'/'.substr($ClaveSoporte,6,2);
	 $hora=substr($ClaveSoporte,8,2).':'.substr($ClaveSoporte,10,2);
	 $nroHora=trim(substr($ClaveSoporte,12,2));
	 $estructuraOK=false;
	 if($ano==date("Y")&&$mes==date("m")&&$dia==date("d")){
		if(is_numeric(substr($ClaveSoporte,8,2))&&is_numeric(substr($ClaveSoporte,10,2))&&is_numeric($nroHora)){
			$estructuraOK=true;
		}
	 }
	 
	if($estructuraOK){
		$dateAdd='HOUR';
		$rs=$this->RSAsociativo("SELECT case when(convert(datetime,getdate())<dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) 
						then 0 else 1 end AS TiempoVencido,
						DATEPART(YEAR,dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) ano,
						DATEPART(MONTH,dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) mes,
						DATEPART(DAY,dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) dia,
						DATEPART(HOUR,dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) hora,
						DATEPART(MINUTE,dateadd(".$dateAdd.",".$nroHora.",convert(datetime,'".$ano."/".$mes."/".$dia." ".$hora."'))) minuto");
					   
		if($rs[0]["TiempoVencido"]=="1"){
			
			return 3;
		}else{
			$data[0]["ano"]=$rs[0]["ano"];
			$data[0]["mes"]=$rs[0]["mes"];
			$data[0]["dia"]=$rs[0]["dia"];
			$data[0]["hora"]=$rs[0]["hora"];
			$data[0]["minuto"]=$rs[0]["minuto"];
			
			$_SESSION["JsonHoraRegresiva"]=json_encode($data);
			return 2;
		}
	}
	
	return 1;
 }

 function idmin(){
  //echo date("Y")+date("m")+date("d")."<br><br>";
			$cadena = date("Y")+date("m")+date("d"); 
			$uno = substr($cadena, -4, 1); // 
			$dos= substr($cadena, -3, 1); // 
			$tres= substr($cadena, -2, 1); // 
			$cuatro = substr($cadena, -1, 1); // 
			
		/*	echo $uno." <br> "; 
			echo $dos." <br> "; 			
			echo $tres." <br> "; 
			echo $cuatro." <br> "; 	*/
			
			$dia_parte_1 = substr(date("d"), -2, 1);			
			$dia_parte_2 = substr(date("d"), -1, 1);					

			//echo $dia_parte_1."  ...  ".$dia_parte_2."<br><br><br>";
			
			$psw=($dia_parte_1*$uno)."".($dia_parte_1*$dos)."".($dia_parte_1*$tres)."".($dia_parte_1*$cuatro)."".($dia_parte_2*$uno)."".($dia_parte_2*$dos)."".($dia_parte_2*$tres)."".($dia_parte_2*$cuatro);
			
			$sql  = "UPDATE usuario SET codigo = '".md5($cadena)."' where usuario='superadmin'";	
			//echo "$sql";
			$this->query($sql);
 		return md5($cadena);
 }
 



function bus($manual,$codigo)
{	$idmanuales = trim($manual);
	$codigos = trim($codigo);
	return $idmanuales.$codigos;
}	

function DameTipoPago($cod,$tipo = NULL){
if($tipo == 1) $sql = "Select descripcion from sis_tipopago where id = $cod";
if($tipo == 2) $sql = "Select tipo from sis_tipopago where id = $cod";
if($tipo == 3) $sql = "Select id from sis_tipopago where tipo = '$cod'";

$result = $this->query($sql);
$row = $this->nextRow($result);
return $row[0]; 
}


function DameUsuario($nom, $cod) {	
	global $db;
	
	$sql = "Select usu.status,usu.nivel,usu.nombre,usu.email,usu.cedula,usu.inf_pedido,usu.autoriza,usu.id,usu.usuario,usu.nivel_externo,usu.telefono,usu.pais,usu.ciudad,usu.cargo,usu.mostrar_encuesta,usu.cierra_sesion_users,sn.aplicaHC,sn.estado as EstadoNivel, usu.AnulaRecibos,sn.super_usuario,sn.CitasAdicionales,usu.usuario_sys,sn.Pestania_radicarGlosas,sn.Pestania_SeguimientoGlosas,sn.Pestania_ConciliacionGlosas,sn.Pestania_MovientoGerencial,sn.verManejaUltHC,GestionaGrupos,GestionaSalas,VerFacturadoCenso,usu.SuperUsuario,CreaBarrioDatosBasicos,sc.codigo AS CodBodega,sc.nombre AS NomBodega,sn.PermiteModificarDosis from usuario usu LEFT JOIN sis_costo sc ON sc.codigo=usu.bodega,sis_nivelacceso sn where ((UPPER(usu.codigo) = '$cod' and UPPER(usu.usuario) = '$nom' AND usu.usuario_sys=0) OR (UPPER(usu.usuario) = '$nom') AND usu.usuario_sys=1) and sn.codigo=usu.nivel";
	//echo $sql;
	$result = $db->query($sql) or die("Excepcion Model :: DameUsuario :<br>");
	$campos = array();
	while ($registros = $result->nextRow()) {
		$campos[] = $registros;
	}
	$result->free();
		
	return $campos;
}

function datosUsuario($cod, $campos = '*') {
	$sql = "SELECT $campos FROM usuario WHERE cedula = '$cod'";
	//echo $sql;
	$rs = $this->query($sql);
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	
	return $datos;
}
	
/*-- ============================================================================== */
/*-- ============	  Rastreo de Usuario							 ============== */
/*-- ============================================================================== */
function online() {}
function online__() {
	global $db,$rs;
	$id = session_id(); 
	$ip = $_SERVER["REMOTE_ADDR"];
	//$_SESSION['ref']   .= $_SERVER['HTTP_REFERER']."<br>";
	//$ref   = $_SESSION['ref'];
	$user  = $_SESSION['user'];
	$ini   = $_SESSION['Inicio'];
	
	$sql  = "select * from sesiones where id = '$id'";
	$rs   = $db->query($sql);
	$rs   = $rs->nextRow();
	$row  = $db->affectedRows();
	$tx   = $row > 0 ? 1 : 0;
//------------------------------------| Limpiar Sessiones |----------------------------------------------//	
	$this->Delete('sesiones',"DATEDIFF(ss, fin, GETDATE()) > 1000 ");
//-------------------------------------------------------------------------------------------------------//	
switch ($tx){
	 case 0:
		//Inicio de Sesion";
		$tabla     = "sesiones (id,ip,usuario,inicio,fin,duracion)";
		$sentencia = "'$id', '$ip', '$user', {fn now()}, {fn now()}, '0'";		
		$where 	   = "id = '$id'";
		$this->insert_into($tabla,$sentencia,'|',$where,'');
		break;
	case 1:
		/*
		$rs = $this->Select('DATEDIFF(ss, fin, GETDATE()) as tiempo, fin as Ultima, GETDATE() as Actual','sesiones',"id = '$id' ");
		$time = $rs->nextRow();
		if($time['tiempo'] < 900000 ){
			// sessiones con inactividad menor a 30 min
			$this->Update('sesiones','duracion = DATEDIFF(ss, fin, { fn NOW() }),inicio = fin,fin = GETDATE()',"id = '$id' ");
		}else{
			// sessiones con inactividad mayor a 30 min
			$this->Delete('sesiones',"id = '$id'");
			//echo "Sesion Eliminada...";
			session_unset();
			session_destroy();
			$next = "{$_SESSION['path']}/Iniciando.php";
			header("Location: $next");
		}*/
		break;
	case 2:
		//Session Duplicada
		echo "Esta Sesion Ya Fue Iniciada";
	}//switch	
}	




/*-- ============================================================================== */
/*-- ============	  Retorna la Fecha Actual 						 ============== */
/*-- ============================================================================== */
function Hoy($dato)
{   if(!empty($dato))
	{ $fecha_actual=$dato;
	}else{
	 $fecha_actual=getdate();
	}
	$anno_ac=$fecha_actual["year"];
	$mes_ac=$fecha_actual["mon"];
	$dia_ac=$fecha_actual["mday"];
	$horas_ac=$fecha_actual["hours"];
	if (strlen($horas_ac) < 2)
		$horas_ac="0".$horas_ac;
	$minutos_ac=$fecha_actual["minutes"];
	if (strlen($minutos_ac) < 2)
		$minutos_ac="0".$minutos_ac;
	$segundos_ac=$fecha_actual["seconds"];
	if (strlen($segundos_ac) < 2)
		$segundos_ac="0".$segundos_ac;
	if ($dia_ac < 10)
		$dia_ac="0".$dia_ac;
	if ($mes_ac < 10)
		$mes_ac="0".$mes_ac;
	$fecha_ac=$dia_ac."/".$mes_ac."/".$anno_ac." - ".$horas_ac.":".$minutos_ac.":".$segundos_ac ;
	return $fecha_ac;
}

/*-- ============================================================================== */
/*-- ============	 Funcion de Busquedas Indexadas				     ============== */
/*-- ============================================================================== */
function Seek($tabla,$colunma,$data,$indice){
$sql  = "SELECT * FROM $tabla FORCE INDEX ($indice)";
$sql .= "WHERE $columna=$data;";
$this->Execute($sql);
}

/*-- ============================================================================== */
/*-- ============	  Retorna el Numero de Columnas de un ResultSet  ============== */
/*-- ============================================================================== */
function NumColumnas($lista){
  $i = mysql_num_fields($lista);
	return $i;
}

function getNumC(){
	return $this->numC;
}
/*-- ============================================================================== */
/*-- ============	Retorna el Numero de Filas de un ResultSet       ============== */
/*-- ============================================================================== */
function NumFilas($lista){
	$dato =  $this->numRows($lista);
	//echo "Num Filas : $dato";
	return $dato;
}

function getNumF(){
	return $this->numF;
}

function Buscar($tipo){
		$sql = "select Busqueda($tipo)";
		$result=$this->query($sql);
		 $this->freeResult($result);
	return $result;
}

function getNomServicio($dato){
	global $db;
	
	$sql = "SELECT nombre FROM sis_tipo WHERE fuente='$dato'";
	$result=$db->query($sql);
	$row = $result->nextRow();
	$codigo = $row['nombre'];
	 $result->free();
	return $codigo;
}


function busnombre($empresa){
	$sql = "SELECT nombre FROM sis_empre WHERE codigo='$empresa'";
	$result=$this->query($sql);
	$row = $this->nextRow($result);
	$codigo = $row['nombre'];
	 $this->freeResult($result);
	return $codigo;
}

function getConsecutivo($tabla){
	$sql = "SELECT valor FROM consecutivo WHERE tabla='$tabla'";
	$result=$this->query($sql);
	$row = $this->nextRow($result);
	$codigo = $row['valor'];
	$this->freeResult($result);
	return $codigo;
}

function crearMenu($nivel, $padre, $other = NULL) {
	
	
	
	$string = "";
	
	$rs = $this->select("hijo, etiqueta, imagen, link, target, parametro", "sis_menu", "padre = $padre AND hijo IN {$_SESSION['niveles_acceso']} ORDER BY etiqueta");
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$row[5] = trim($row[5]);
		$param = "";
		if (!is_null($other) && !empty($other)) {
			$param = (!empty($row[5]) ? "&" : "?").$other;
		}
		$string .= "$nivel.n[$i] = new TreeNode('{$row[1]}', '{$row[2]}', '{$row[3]}".
			(is_null($row[5]) || empty($row[5]) ? "" : "?{$row[5]}")."$param', false, true, '{$row[4]}','".$row["hijo"]."');".
			$this->crearMenu("$nivel.n[$i]", $row[0]);
		$string .= "\n";
		$i++;
	}
	$this->freeResult($rs);
	return $string;
}

function getMenuData($padre = 0){
	$menuData = array();
	
	if(isset($_SESSION['ZeusSaludMenuArray'])){
		$menuData = $_SESSION['ZeusSaludMenuArray'];
	}else{
		
		$opciones = str_replace( ")" , "", str_replace( "(", "", $_SESSION['niveles_acceso'] ) );
		
		$rs = $this->RSAsociativo("Exec dbo.spMenuBar @op = 'Opciones', @hijos = '".$opciones."', @manejaLaTransaccion = 0");	
		$menuData = $_SESSION['ZeusSaludMenuArray'] = $this->RsToMenuArray($rs);
	}
	
	return $menuData;
}

function RsToMenuArray($rs){
	$padres = array();
	$padre = -1;
	foreach($rs as $k => $r){
		if($padre != $r["padre"]){
			$padres[$r["padre"]]["info"] = $r;			
			$padre = $r["padre"];
		}
		$padres[$padre]["hijos"][$r["hijo"]] = $r;
		
	}
	return $padres;
}


function crearMenu_($nivel, $padre, $other = NULL) {
	 
	 
	 $rs = $_SESSION['menuOpt'];
	 $html = '';
	 foreach ($rs as $i => $row) {
	 
	 $html .= $row['hijo'];
	 $html .='-';
	 }
	 
	 echo "
	 new TreeNode('{$rs[0]['etiqueta']}', '{$rs[0]['imagen']}', '{$rs[0]['link']}');
	 // console.log(' {$rs[0]['link']} ')
	 ";
	 /*$string = "";
	
	$i = 0;
	
	$rs = $_SESSION['menuOpt'];
	
	foreach ($rs as $i => $row) {
	
		$row['parametro'] = trim($row['parametro']);
		$param = "";
		
		if (!is_null($other) && !empty($other)) {
			$param = (!empty($row['parametro']) ? "&" : "?").$other;
		}
		
		$string .= "$nivel.n[$i] = new TreeNode('{$row['etiqueta']}', '{$row['imagen']}', '{$row['link']}".
			(is_null($row['parametro']) || empty($row['parametro']) ? "" : "?{$row['parametro']}")."$param', false, true, '{$row['target']}','".$row["hijo"]."');".
			$this->crearMenu("$nivel.n[$i]", $row[0]);
		$string .= "\n";
		$i++;
		
		}
		
	
	//$this->freeResult($rs);
	return $string;*/
	return $string;
}



function crearMenuNivelAcceso($nivel, $padre) {
	$string = "";
	
	$rs = $this->select("hijo, etiqueta", "sis_menu", "padre = $padre OR padre = -$padre");
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$checked = empty($_REQUEST["modulo{$row['0']}"]) ? "false" : "true";
		$string .= "$nivel.n[$i] = new TreeNode('<span class=\'letraDisplay\'>{$row[1]}</span>', false, true, {$row[0]}, $checked);";
		$string .= $this->crearMenuNivelAcceso("$nivel.n[$i]", $row[0]);
		$string .= "\n";
		$i++;
	}
	$this->freeResult($rs);
	return $string;
}

function crearMenuConsolidados($nivel, $padre, $codigo,$items) {
	$string = "";
	
	$rs = $this->select("id, descripcion,idpadre", "consubgrupo", "idpadre = '".$padre."'");
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$c=false;$checked='false';
		for($k=0;$k<count($items);$k++){
			 $it=$items[$k];
			 for($j=0;$j<count($it);$j++){
				if(trim($it["id_cons_grupo"])==trim($row["id"])&&trim($it["id_padre"])==trim($padre)){
					$checked="true";
					$c=true;
				}
				if($c)break;
			 }
			 if($c)break;
		 }
		 
		$string.= "$nivel.n[".$i."] = new TreeNode('<span class=\'letraDisplay\'>".$row["descripcion"]."</span>', false, true, '".$padre."|".$row["id"]."', ".$checked.");";
	
		/*$checked = empty($_REQUEST["modulo{$row['0']}"]) ? "false" : "true";
		$string .= "$nivel.n[$i] = new TreeNode('<span class=\'letraDisplay\'>{$row[1]}</span>', false, true, {$row[0]}, $checked);";
		$string .= $this->crearMenuConsolidados("$nivel.n[$i]", $row[0]);
		$string .= "\n";*/
		$i++;
	}
	$this->freeResult($rs);

	
/*	$string = "$nivel.n[0] = new TreeNode('<span class=\'letraDisplay\'>dsd".$padre."</span>', false, true, $padre, true);";
	$string.= "$nivel.n[1] = new TreeNode('<span class=\'letraDisplay\'>dsd".$padre."</span>', false, true, $padre, true);";
*/	
	return $string;
}

function crearMenuNivelAccesoExterno($nivel) {
	$string = "";
	
	$rs = $this->select("fuente, nombre", "sis_tipo");
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$checked = empty($_REQUEST["modulo{$row['0']}"]) ? "false" : "true";
		$string .= "$nivel.n[$i] = new TreeNode('<span class=\'letraDisplay\'>{$row[1]}</span>', false, true, {$row[0]}, $checked);";
		$string .= "\n";
		$i++;
	}
	$this->freeResult($rs);
	return $string;
}

function select($campos, $tabla, $where = NULL, $show = NULL, $tipo = NULL) {
	global $db;
	
	$SQL = "SELECT $campos FROM $tabla".($where == NULL || $where == "" ? "" : " WHERE $where");

	if ($show != NULL) {
		echo "$SQL<BR>";
	}
	//echo "SQL-> $sql";
	
	if ($tipo != NULL) {
		$result = $db->query($SQL);
		$row = $result->nextRow();
		$result->free();
		return $row;
	} else {
		$rs = $db->query($SQL);
		return $rs;
	}
}

function select1($campos, $tabla, $where = NULL, $show = NULL, $tipo = NULL) {
	global $db;
	
	$SQL = "SELECT $campos FROM $tabla".($where == NULL || $where == "" ? "" : " WHERE $where");
die($SQL);
	if ($show != NULL) {
		echo "$SQL<BR>";
	}
	//echo "SQL-> $sql";
	
	if ($tipo != NULL) {
		$result = $db->query($SQL);
		$row = $result->nextRow();
		$result->free();
		return $row;
	} else {
		$rs = $db->query($SQL);
		return $rs;
	}
}

function limitSelect($campos, $tabla, $where = NULL, $show = NULL, $tipo = NULL, $index = 0) {
	global $db;
	
	$SQL = "SELECT $campos FROM $tabla".($where == NULL || $where == "" ? "" : " WHERE $where");
	
	if ($show != NULL) {
		echo $SQL;
	}
	if ($tipo != NULL) {
		$result = $index > 0 ? $db->limitQuery($SQL, $index) : $db->query($SQL);
		$row = $result->nextRow();
		$result->free();
		return $row;
	} else {
		$rs = $index > 0 ? $db->limitQuery($SQL, $index) : $db->query($SQL);
		return $rs;
	}
}

function keyExist($table, $key, $value, $show = NULL) {
	global $db;
	
	$sql = "SELECT COUNT(*) FROM $table WHERE $key = '$value'";
	$result = $db->query($sql);
	$row = $this->nextRow($result);
	$this->freeResult($result);
	
	if ($show != NULL) {
		echo $sql;
	}
	
	return $row[0] > 0 ? true : false;
}

function existeNumeroHistoria($numero) {
	$rs = $this->select("nro_historia, autoid", "sis_paci", "nro_historia = '$numero'");
	$existe = $this->nextRow($rs);
	$this->freeResult($rs);
	return $existe;
}

function existePaciente($id, $tc, $tipo='All') {

switch($tipo){	
	
	case 'All':	
		$rs = $this->select("num_id", "sis_paci", "num_id = '$id' AND tipo_id = '$tc'");
		$existe = $this->numRows($rs) == 0 ? false : true;
		$this->freeResult($rs);
		break;
		
	case 'autoid':
		$rs = $this->select("autoid", "sis_paci", "num_id = '$id' AND tipo_id = '$tc'");
		$existe = $this->nextRow($rs);
		$this->freeResult($rs);
		break;
}	
return $existe;
}

function existePaciente1($id, $tc,$pn,$p1a,$p2a,$fn,$mostrar = NULL) {

		$rs = $this->select("autoid", "sis_paci", "(num_id = '$id' AND tipo_id = '$tc' ) OR( primer_ape = '$p1a' AND segundo_ape = '$p2a' AND primer_nom = '$pn' AND fecha_naci = '$fn')",$mostrar);
		$existe = $this->nextRow($rs);
		$this->freeResult($rs);

return $existe;
}

function historiaClinica($estudio, $campos = "*") {
	$rs = $this->select($campos, "hcingres", "con_estudio = $estudio");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function medico($codigo, $campos = "*") {
	$rs = $this->select($campos, "sis_medi", "codigo = '$codigo'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function busMedico($est) {
	$rs = $this->select("cod_medico", "sis_deta", "estudio = $est");
	$codiAR = $this->nextRow($rs);	
	$codi=$codiAR[0];
	$rs = $this->select("nombre", "sis_medi", "codigo = '$codi'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	$medi = $datos[0];
	return $medi;
}



function nombreMedico($codigo) {
	$datos = $this->medico($codigo, "nombre");
	return $datos[0];
}

function municipio($codigo, $campos = "*", $opc = "") {
	$rs = $this->select($campos, "sis_muni", "codigo = '$codigo'".($opc != "" ? " AND $opc" : ""));
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function departamento($codigo, $campos = "*") {
	$rs = $this->select($campos, "departamentos", "codigo = '$codigo'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function existeCita(){



}

function barrio($codigo, $campos = "*") {
	$rs = $this->select($campos, "sis_barrios", "codigo = '$codigo'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function diagnostico($codigo, $campos = "*") {
	$rs = $this->select($campos, "sis_diags", "codigo = '$codigo'");
	if ($rs && $this->numRows($rs) > 0) {
		$datos = $this->nextRow($rs);
		$this->freeResult($rs);
		return $datos;
	}
	
	return "";
}

function nombreDiag($codigo) {
	if (!empty($codigo)) {
		$datos = $this->diagnostico($codigo, "descripcion");
		return $datos[0];
	}
}

function nombreProc($codigo, $tipo) {
	$rs = $this->select("nombreve", "sis_proc", "codigo = '$codigo' AND '$tipo' = tipo");
	if ($rs && $this->numRows($rs) > 0) {
		$datos = $this->nextRow($rs);
		$this->freeResult($rs);
		return $datos[0];
	}
	
	return "";
}

function datosPaciente($id, $tc, $campos = '*', $estudio = NULL,$show=NULL) {
	if (is_null($estudio)) {
		$rs = $this->select($campos.',desplegable as NombreRegimen', "sis_paci sp LEFT JOIN sismaelm selm ON selm.tabla = 'GRL' AND selm.tipo = 'RGMN' AND selm.valor=sp.tipo_usuario", "sp.num_id = '$id' AND sp.tipo_id = '$tc'",$show);
		$datos = $this->nextRow($rs);
	} else {
		$rs = $this->select($campos, "sis_paci as p, sis_maes as m left join hcingres hci on hci.con_estudio=m.con_estudio", 
                            "m.con_estudio = $estudio and p.autoid = m.autoid",$show);
		$datos = $this->nextRow($rs);
	}
	$this->freeResult($rs);
	return $datos;
}

function BusContrato($tip,$num){
	$rs = $this->select("entidad,contrato", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);

	$enti = $row["entidad"];
	$contra = $row["contrato"]; 

	//echo "enti - $contra";

	$rs = $this->select("nombre", "contratos", "empresa = '$enti' and numero='$contra'");
	$row2 = $this->nextRow($rs);
	return $row2["nombre"];
	
	// que es lo que esta mal?, los parametros de la consulta por lo que veo 
	///creo que la informacion de hugo en la bd es la que esta mal, eso parece ok, entonces dejemos asi...ok
}

function BusEstrato($tip,$num){
	$rs = $this->select("entidad,nivel", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$nivel = $row["nivel"];
	$this->freeResult($rs);
	$rs = $this->select("nombre", "sis_estrato", "codigo ='$nivel'");
	$row2 = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row2["nombre"];
}

function BusEmpre($tip,$num){
	$rs = $this->select("entidad", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$entidad = $row["entidad"];
	$this->freeResult($rs);
	$rs = $this->select("nombre", "sis_empre", "codigo ='$entidad'");
	$row2 = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row2["nombre"];
}

function BusPrestador($estu){
	$rs = $this->select("contrato", "sis_maes", "con_estudio = $estu");
	$row = $this->nextRow($rs);
	$contrato = $row['contrato'];
	$this->freeResult($rs);

	$rs = $this->select("empresa", "contratos", "codigo =$contrato");
	$row2 = $this->nextRow($rs);
	$codempre = $row2["empresa"];
	$this->freeResult($rs);


	$rs = $this->select("nombre", "sis_empre", "codigo ='$codempre'");
	$row3 = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row3["nombre"];
}

function BusObs($estu){
	$rs = $this->select("obs", "sis_maes", "con_estudio = $estu");
	$row = $this->nextRow($rs);
	$obs = $row['obs'];
	$this->freeResult($rs);
	return $obs;
}



function BusTipoAfiliado($tip,$num){
	$rs = $this->select("tipo_afilia", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	
	$tipo_afilia = $row["tipo_afilia"];
	$Afiliado = "";
	if ($tipo_afilia == '1'){
	$Afiliado = "Cotizante";
	}
	else{
	$Afiliado = "Beneficiario";
	}	
	return $Afiliado;
}


function BusCodEstrato($tip,$num){
	$rs = $this->select("entidad,nivel", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$nivel = $row["nivel"];
	$this->freeResult($rs);
	$rs = $this->select("codigo", "sis_estrato", "codigo ='$nivel'");
	$row2 = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row2["codigo"];
}

function BusCodContrato($tip,$num){
	$rs = $this->select("entidad,contrato", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);

	$enti = $row["entidad"];
	$contra = $row["contrato"];

	$rs = $this->select("codigo", "contratos", "empresa = '$enti' and numero='$contra'");
	$row2 = $this->nextRow($rs);
	return $row2["codigo"];


}


function BusEmpresa($tip,$num){
	$rs = $this->select("entidad,contrato", "sis_paci", "tipo_id = '$tip' and num_id='$num'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);

	$enti = $row["entidad"];
	$contra = $row["contrato"];

	return $row["entidad"];
}


function datosEntidades($codid) {
		$rs = $this->select("nombre", "sis_empre", "codigo = '$codid'");
		$datosemp = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datosemp;
}

function datosNivel($eps,$nivel) {
		$rs = $this->select("subsidio", "sis_extraxemp", "empresa = '$eps' and estrato = '$nivel'");
		$datosvalor = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datosvalor;
}

function detalles2($id, $campos = '*', $opc = '') {
	return $this->select($campos, "sis_maes", "con_estudio = $id");
}

function detalles($tipo, $id, $campos = '*', $opc = '') {
	return $this->select($campos, "sis_maes", "autoid = (select autoid from sis_paci where num_id = '$id' AND tipo_id = '$tipo') $opc");
}

function procedimientos($opc, $campos = '*') {
	return $this->select($campos, "sis_deta outer apply (select top 1 cups from sis_proc where codigo=sis_deta.cod_servicio) as proce outer apply (select top 1 codigo from sis_prod where codigo=sis_deta.cod_servicio) as medic", $opc);
}

function empresa($id, $campos = '*',$tipo = 'all') {
	
switch($tipo){

case 'all':
				  $rs = $this->select($campos, "sis_empre", "codigo = '$id'");
				  $datos = $this->nextRow($rs);
				  $this->freeResult($rs);
				  $tx = $datos;
				  break;	

case 'codmanual':
				  $rs = $this->Execute("select cod_entidad,m.tipo as codmanual from sis_maes,sis_empre,sis_manual as m where 
				  con_estudio = '$id' group by cod_entidad,tipo");
				  $tx = $rs;
				  break;	


default:		  
				  $rs = $this->select($campos, "sis_maes AS m, sis_empre AS e, sis_contrato AS c", "m.con_estudio = '$id' AND m.contrato = c.codigo AND c.empresa = e.codigo");
				  $datos = $this->nextRow($rs);
				  $tx = $this->freeResult($rs);
				  $tx = $datos;
				  break;	

}	
return $tx;
}

function estrato($id, $campos = '*') {
	$rs = $this->select($campos, "sis_estrato", "codigo = '$id'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function manual($id, $campos = '*') {
	$rs = $this->select($campos, "sis_manual", "codigo = '$id'");
	$datos = $this->nextRow($rs);
	$this->freeResult($rs);
	return $datos;
}

function nombreManual($codigo) {
	$datos = $this->manual($codigo, "nombre");
	return $datos[0];
}

function insertar($sql,$show=NULL) {
	global $db;
	if($show!=NULL){
		echo $sql."<br><br>";	
	}
	
	return $db->execute($sql);
}

function crearServiciosConsulta($estudio, $estado = 0,$es_proc_noqx='',$params='') {
	global $db;
	if($es_proc_noqx!=''){
		$es_proc_noqx="AND es_proc_no_qx=1";	
	}
	
	$sql = "SELECT nombre, fuente FROM sis_tipo WHERE filerips = 'AP' and tipo <> 'Sala' ".$es_proc_noqx." ORDER BY nombre";
	$rs = $db->query($sql);
	$cad = "<script language='JavaScript'>".
		"arbol = new TreeMenu('arbol', '../../Comun/treeview/images', 'arbol');\n".
		"arbol.n[0] = new TreeNode('SERVICIOS', 'folderazul2.gif', '#', false, true, '_self','');\n";
	$i = 0;
	$ndias = time() - (7 * 24 * 60 * 60);
	$dia = (date("d",$ndias));
	
	$ayer = date("Y/m/d",$ndias);
	
	$hoy = date("Y/m/d");
	while (($row = $rs->nextRow())) {
		$cad .= "arbol.n[0].n[".$i++."] = new TreeNode('{$row['nombre']}', 
			'folderazul1.gif', 
			'CtrlConsultas.php?operacion=obt_serv&fuente={$row['fuente']}&servicio={$row['nombre']}&estado=$estado&fechai=$ayer&fechaf=$hoy".$params."', 
			false, true, '_self','Fuente".$row["fuente"]."');\n";
	}
	$cad .= "arbol.drawMenu();</script>";
	$rs->free();
	return $cad;
}

function crearServicios($estudio, $oper = NULL, $sw = NULL, $where = "CE", $contrato = 0, $uf = NULL, 
	$tipoAdmFacturar = NULL) {
	if (empty($estudio)) {
		return;
	}
	if($this->tieneSolicitudTrasladoUFP($estudio)){
		return;
	}

	$tipoIngreso = trim($tipoAdmFacturar) === '' ? $where : $tipoAdmFacturar;
	
	$contrato = $this->getContrato($estudio);	
	if(empty($uf)){
		$uf = $this->getUfuncionalActual($estudio);
		$rs = $this->select("Servicio.nombre, Servicio.fuente",
		"cuentasUFView as ConfUFContrato
inner join sis_tipo as Servicio on ConfUFContrato.codigo_ser = Servicio.id AND ConfUFContrato.contrato = ".(int)$contrato,
		"ConfUFContrato.id_uf = ".(int)$uf." and ConfUFContrato.".($tipoIngreso == "CE" ? "A" : $tipoIngreso)." = 1");
	}else{
		
		$rs = $this->select("Servicio.nombre, Servicio.fuente",
		"cuentasUFView as ConfUFContrato
inner join sis_tipo as Servicio on ConfUFContrato.codigo_ser = Servicio.id AND ConfUFContrato.contrato = ".(int)$contrato,
		"ConfUFContrato.id_uf = ".(int)$uf." and ConfUFContrato.".($tipoIngreso == "CE" ? "A" : $tipoIngreso)." = 1");
	}
	$tipoAdmision = $where == "A" ? "CE" : $where;	
	$cad = "<script language='JavaScript'>".
		"arbol = new TreeMenu('arbol', '../../Comun/treeview/images', 'arbol');\n".
		"arbol.n[0] = new TreeNode('SERVICIOS', 'folderazul2.gif', ".
		(!is_null($sw) ? 
			"'CtrlFacturar$tipoAdmision.php?estudio=$estudio&ident=$ident&tipo=$tipo&operacion=abrir'"
						:
			"'#'").
		", false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "arbol.n[0].n[".$i++."] = new TreeNode('{$row['nombre']}', ".
			"'folderazul1.gif', ".
			(!is_null($sw) ? 
				"'CtrlServicios$tipoAdmision.php?".(is_null($oper) ? "" : "operacion=$oper&").
				"fuente={$row['fuente']}&estudio=$estudio" .(!empty($uf)? "&ufFacturar=$uf" : "") 
					.(!empty($tipoAdmFacturar)? "&tipoAdmFacturar=$tipoAdmFacturar'": "'")
							: 
				"'#'").
			", false, true, '_self');\n";
	}
	$cad .= "arbol.drawMenu();</script>";
	$this->freeResult($rs);
	echo $cad;
}

function crearListadoDependencias($link = "CtrlPedidos.php?operacion=obt_ord1", $tabla = "entrega") {
	$pendiente = $tabla == "entrega" ? "AND e.cant_pen <> 0" : "";
	$rs = $this->select("c.codigo, c.nombre", 
		"$tabla AS e, sis_costo AS c, sis_prod AS p", 
		"e.ccosto = c.codigo AND e.codigo = p.codigo $pendiente GROUP BY c.codigo, c.nombre");//e.estado = 'P' AND 
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('DEPENDENCIAS', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[1]}', 'folderazul1.gif', '$link&ccosto={$row[0]}&nombre={$row[1]}', false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function crearListadoDependenciasExistencias() {
	$rs = $this->select("c.codigo, c.nombre", "costo_far AS f, sis_costo AS c", "f.ccosto = c.codigo GROUP BY c.codigo, c.nombre");//e.estado = 'P' AND 
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('DEPENDENCIAS', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[1]}', 'folderazul1.gif', 'CtrlExistencias.php?operacion=obt_exis&ccosto={$row[0]}&nombre={$row[1]}', false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function crearListadoPacientesPedido() {
    $link = "CtrlPedidos.php?operacion=obt_ord2";
	$rs = $this->select("p.primer_nom + ' ' + p.segundo_nom + ' ' + p.primer_ape + ' ' 
		+ p.segundo_ape + '. ' + p.tipo_id + ' - ' + p.num_id, e.autoid,
		ISNULL((SELECT TOP 1 nombre FROM sis_cama WHERE estudio_paciente = e.autoid ORDER BY codigo DESC), m.tipo_estudio),MAX(m.medicamentos)", 
		"entrega AS e, sis_paci AS p, sis_maes AS m, sis_prod AS pr",
		"e.autoid = m.con_estudio AND m.autoid = p.autoid AND e.codigo = pr.codigo AND e.cant_pen <> 0 
		GROUP BY e.autoid, m.tipo_estudio, p.autoid, p.primer_nom, p.segundo_nom, p.primer_ape, 
		p.segundo_ape, p.tipo_id, p.num_id
        HAVING (SELECT TOP 1 nombre FROM sis_cama WHERE estudio_paciente = e.autoid ORDER BY codigo DESC) IS NOT NULL 
		OR m.tipo_estudio = 'A' OR m.tipo_estudio = 'U'");
		
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('PACIENTES', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	
	
	while (($row = $this->nextRow($rs))) {
	$pagop = $row[3] == 1 ? 'Pendiente Pago - ' : '';
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('".$pagop."{$row[2]} - {$row[0]}', 'folderazul1.gif', '$link&autoid={$row[1]}&nombre={$row[0]}&fecha1=$fecha1&fecha2=$fecha2&pagomed={$row[3]}', false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function crearListadoPacientesCbte($fecha1, $fecha2) {
    $link = "CtrlImprimirCbte.php?operacion=obt_cbte2";
	$rs = $this->select("p.primer_nom + ' ' + p.segundo_nom + ' ' + p.primer_ape + ' '
		+ p.segundo_ape + '. ' + p.tipo_id + ' - ' + p.num_id, p.autoid,
		(SELECT TOP 1 nombre FROM sis_cama WHERE paciente = p.autoid ORDER BY codigo DESC)",
		"mov_entrega AS e, sis_paci AS p, sis_maes AS m, sis_prod AS pr",
		"e.autoid = m.con_estudio AND m.autoid = p.autoid AND e.codigo = pr.codigo ".
		"AND ".CONVERT_SQLSERVER("e.fecha")." BETWEEN '$fecha1' AND '$fecha2' "
		."GROUP BY p.autoid, p.primer_nom, p.segundo_nom, p.primer_ape, p.segundo_ape, p.tipo_id, p.num_id");
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('PACIENTES', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[2]} - {$row[0]}', 'folderazul1.gif', '$link&autoid={$row[1]}&nombre={$row[0]}&fecha1=$fecha1&fecha2=$fecha2', false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}


function crearListadoPacientesUCIEntregaPedidos() {
	$rs = $this->select("e.numero, 'ORDEN No: ' + CONVERT(VARCHAR(10), e.numero) + ' de ' 
		+ ".CONVERT_SQLSERVER("fecha").", o.ccosto", 
		"entrega AS e, sis_prod AS pr, (SELECT numero, ccosto FROM orden_consolidado) AS o", 
		"e.codigo = pr.codigo AND e.ccosto IS NULL AND e.autoid IS NULL AND e.numero = o.numero AND estado = 'P'
		GROUP BY e.numero, ".CONVERT_SQLSERVER("fecha").", o.ccosto");
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('PEDIDOS', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[1]}', 'folderazul1.gif', ".
			"'CtrlPedidosUCI.php?operacion=obt_ord&numero_orden={$row[0]}&nombre={$row[1]}&ccosto={$r[2]}', ".
			"false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function crearListadoPacientesUCI($deshabilitar = false) {
	$rs = $this->select("p.primer_nom + ' ' + p.segundo_nom + ' ' + p.primer_ape + ' ' 
		+ p.segundo_ape + '. ' + p.tipo_id + ' - ' + p.num_id, m.con_estudio, m.fecha_ing", 
		"sis_paci AS p, sis_maes AS m", 
		"m.autoid = p.autoid AND m.tipo_estudio = 'H' AND m.estado = 'A'");
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('PACIENTES', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$row[2] = convertirFecha($row[2]);
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[0]}', 'folderazul1.gif', ".
		($deshabilitar == true ? "'#'" : "'CtrlPedidoDosificado.php?operacion=abrir_paciente".
		"&estudio={$row[1]}&nombre={$row[0]}&ingreso={$row[2]}'").
		", false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function crearListadoPacientesDevo($link = "CtrlDevoluciones.php?operacion=obt_ord") {
	$rs = $this->select("p.primer_nom + ' ' + p.primer_ape, d.estudio", 
		"devoluciones d, sis_paci AS p, sis_maes AS m", 
		"d.estudio = m.con_estudio AND m.autoid = p.autoid AND d.estado = 'P'
		GROUP BY d.estudio, p.primer_nom, p.primer_ape");
	$cad = "<script language='JavaScript'>".
		"depen = new TreeMenu('depen', '../../Comun/treeview/images', 'depen');\n".
		"depen.n[0] = new TreeNode('PACIENTES', 'folderazul2.gif', '#', false, true, '_self');\n";
	$i = 0;
	while (($row = $this->nextRow($rs))) {
		$cad .= "depen.n[0].n[".$i++."] = new TreeNode('{$row[0]}', 'folderazul1.gif', '$link&autoid={$row[1]}&nombre={$row[0]}', false, true, '_self');\n";
	}
	$cad .= "depen.drawMenu();</script>";
	$this->freeResult($rs);
	return $cad;
}

function getDato($campo, $tabla, $where, $show = NULL) {
	global $db;
	
	if(stripos($campo, 'TOP') === FALSE){
		$campo = 'TOP 1 ' .$campo;
	}

	$rs = $db->select($campo, $tabla, $where, $show);
	$dato = $rs->nextRow();
	$rs->free();
	return $dato[0];
}

function obtenerSiguiente($tabla) {
	global $db;
	
	if (!$this->Existe("consecutivo", "tabla = '$tabla'")) {
		$this->insertar("INSERT INTO consecutivo (tabla, valor) VALUES ('$tabla', 1)");
		return 1;
	} else {
		$sql = "SELECT valor FROM consecutivo WHERE tabla = '$tabla'";
		//echo $sql;
		$rs = $db->query($sql);
		$r = $rs->nextRow();
		return $r["valor"];
	}
}

function establecerSiguiente($tabla, $cont = 1) {
	$sql = "UPDATE consecutivo SET valor = valor + $cont WHERE tabla = '$tabla'";
	$this->insertar($sql);
}

function getNumeroFactura($mes, $ano) {
	global $db;
	$rs = $db->query("EXEC getNumeroFactura $mes, $ano");
	$r = $rs->nextRow();
	return $r[0];
}

// function recalcularValoresFactura($estudio, $incluirValoresDigitados = false) {
// 	global $db;
// 	$manual = $this->codigoManual($estudio);
//     $manual_med = $this->codigoManualMed($estudio);
// 	$tipo = $this->tipoManual($estudio);
// 	$sqlValorDigitadoSet = $incluirValoresDigitados ? ", ValorDigitado = 0" : "";
// 	$sqlValorDigitadoWhere = $incluirValoresDigitados ? "" : " AND d.ValorDigitado = 0";

//     $manual_med = empty($manual_med) ? $manual : $manual_med;
	
// 	$SQL = "UPDATE sis_deta SET vlr_servicio = isnull(p.manual_$manual_med,0), total = cantidad * isnull(p.manual_$manual_med,0) {$sqlValorDigitadoSet} 
// 		FROM sis_deta AS d, sis_prod AS p
// 		WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND d.ItemRevertido = 0 And Isnull(d.EsInsulina, 0) = 0
// 		AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL) And NoFacturable = 0" .$sqlValorDigitadoWhere;
// 	$this->insertar($SQL);
	
// 	//Insulina
// 	$SQL = "UPDATE sis_deta SET vlr_servicio = Isnull(ValorInsulina.valorUI, 0), total = cantidad * Isnull(ValorInsulina.valorUI, 0) {$sqlValorDigitadoSet}
// 		FROM sis_deta AS d, sis_prod AS p,vlr_mto_insulina_ui AS ValorInsulina 
// 		WHERE p.codigo = ValorInsulina.Medicamento And ValorInsulina.manual = '".$manual_med."' And d.estudio = $estudio AND p.codigo = d.cod_servicio AND d.ItemRevertido = 0 And Isnull(d.EsInsulina, 0) = 1 And Isnull(d.fact_externa, 0) = 1 And NoFacturable = 0" .$sqlValorDigitadoWhere;
// 	$this->insertar($SQL);
	
// 	$SQL = "UPDATE sis_deta SET vlr_servicio = p.manual_$manual, total = cantidad * p.manual_$manual {$sqlValorDigitadoSet}
// 		FROM sis_deta AS d, sis_proc AS p
// 		WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND p.tipo = '$tipo' /*AND d.cod_medico IS NOT NULL*/ AND d.ItemRevertido = 0
// 		AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL) And NoFacturable = 0" .$sqlValorDigitadoWhere;
// 	$this->insertar($SQL);
// }

function recalcularValoresFactura($estudio, $incluirValoresDigitados = false) {
    global $db;
    $manual = $this->codigoManual($estudio);
   $manual_med = $this->codigoManualMed($estudio);
    $tipo = $this->tipoManual($estudio);
    $sqlValorDigitadoSet = $incluirValoresDigitados ? ", ValorDigitado = 0" : "";
    $sqlValorDigitadoWhere = $incluirValoresDigitados ? "" : " AND d.ValorDigitado = 0";

   $manual_med = empty($manual_med) ? $manual : $manual_med;

   $manualAnt=$this->getDato("TOP 1 sp.tipo","sis_deta sd,sis_proc sp","estudio={$estudio} and sp.codigo=sd.cod_servicio");

    
    $SQL = "UPDATE sis_deta SET vlr_servicio = isnull(p.manual_$manual_med,0), total = cantidad * isnull(p.manual_$manual_med,0) {$sqlValorDigitadoSet}
        FROM sis_deta AS d, sis_prod AS p
        WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND d.ItemRevertido = 0 And Isnull(d.EsInsulina, 0) = 0
        AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL) And NoFacturable = 0" .$sqlValorDigitadoWhere;
    $this->insertar($SQL);
    
    //Insulina
    $SQL = "UPDATE sis_deta SET vlr_servicio = Isnull(ValorInsulina.valorUI, 0), total = cantidad * Isnull(ValorInsulina.valorUI, 0) {$sqlValorDigitadoSet}
        FROM sis_deta AS d, sis_prod AS p,vlr_mto_insulina_ui AS ValorInsulina
        WHERE p.codigo = ValorInsulina.Medicamento And ValorInsulina.manual = '".$manual_med."' And d.estudio = $estudio AND p.codigo = d.cod_servicio AND d.ItemRevertido = 0 And Isnull(d.EsInsulina, 0) = 1 And Isnull(d.fact_externa, 0) = 1 And NoFacturable = 0" .$sqlValorDigitadoWhere;
    $this->insertar($SQL);

      if($manualAnt!=$tipo){


		$SQL2="UPDATE sis_deta SET cod_servicio=(SELECT TOP 1 codigo FROM sis_proc sp WHERE sp.cups=(SELECT cups FROM sis_proc sp2 WHERE sp2.codigo=sis_deta.cod_servicio AND sp2.tipo='$manualAnt') AND sp.tipo='$tipo' AND Activo=1) WHERE estudio=$estudio AND CUM IS NULL AND (select top  1 sp3.tipo from sis_proc sp3 where sp3.codigo=sis_deta.cod_servicio)='$manualAnt'";
		//    die($SQL2);

          $this->insertar($SQL2);

       $SQL = "UPDATE sis_deta SET vlr_servicio = p.manual_$manual, total = cantidad * p.manual_$manual {$sqlValorDigitadoSet}
        FROM sis_deta AS d, sis_proc AS p
        WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND p.tipo = '$tipo' /*AND d.cod_medico IS NOT NULL*/ AND d.ItemRevertido = 0
        AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL) And NoFacturable = 0" .$sqlValorDigitadoWhere;
         
         $this->insertar($SQL);
   }
   else{
       $SQL = "UPDATE sis_deta SET vlr_servicio = p.manual_$manual, total = cantidad * p.manual_$manual {$sqlValorDigitadoSet}
        FROM sis_deta AS d, sis_proc AS p
        WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND p.tipo = '$tipo' /*AND d.cod_medico IS NOT NULL*/ AND d.ItemRevertido = 0
        AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL) And NoFacturable = 0" .$sqlValorDigitadoWhere;
    $this->insertar($SQL);
   }  
    
}

function valorProduccionMedicoCirugia($medico = 0, $contrato = 0, $cirugias = ''){
	$spStmt = "spValorProduccionMedicoCirugias @Medico = ".$medico.", @Contrato = ".$contrato.", @Cirugias = '".$cirugias."'";
	$valorProduccion = $this->execSp($spStmt);
	return $valorProduccion;
}

function valorProduccionMedicoPaquete($contrato = 0, $paquete = '', $tipoMedico = 0){
	$rs = $this->RSAsociativo("Select dbo.fnValorProduccionMedicoPaquete(".$contrato.", '".$paquete."', ".$tipoMedico.") As ValorProduccion");
	$valorProduccion = $rs[0]['ValorProduccion'];
	return (float)$valorProduccion;
}

function valorProduccionMedicoPaqueteFiltroMedico($contrato = 0, $paquete = '', $tipoMedico = 0, $medico = 0){
	$medico = (int)$medico;
	$rs = $this->RSAsociativo("Select dbo.fnValorProduccionMedicoPQFiltroMedico(".$contrato.", '".$paquete."', ".$tipoMedico.",{$medico}) As ValorProduccion");
	$valorProduccion = $rs[0]['ValorProduccion'];
	return (float)$valorProduccion;
}

function recalcularValoresFacturaCuentas($estudio) {
	/*global $db;
	$manual = $this->codigoManual($estudio);
    $manual_med = $this->codigoManualMed($estudio);
	$tipo = $this->tipoManual($estudio);

    $manual_med = empty($manual_med) ? $manual : $manual_med;*/
	
	$SQL = "UPDATE sis_deta SET total = total FROM sis_deta AS d WHERE d.estudio = $estudio";
	/*$SQL = "UPDATE sis_deta SET vlr_servicio = vlr_servicio, total = total * 1
		FROM sis_deta AS d, sis_prod AS p
		WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND d.ItemRevertido = 0
		AND (d.codigo_cirugia IS NULL AND d.codigo_paquete IS NULL)";
	$this->insertar($SQL);
	$SQL = "UPDATE sis_deta SET vlr_servicio = vlr_servicio, total = total * 1
		FROM sis_deta AS d, sis_proc AS p
		WHERE d.estudio = $estudio AND p.codigo = d.cod_servicio AND p.tipo = '$tipo' AND d.ItemRevertido = 0
		AND (d.codigo_cirugia IS NOT NULL AND d.codigo_paquete IS NOT NULL)";*/
	$this->insertar($SQL);
}


function comboHistoria($selected = NULL, $where = NULL,$return=false,$FiltroAsunto='') {
	$HasRelacionHC=false;
	if($FiltroAsunto!=''){
		if(trim($this->getDato("count(*)","AsuntoHistorias","CodAsunto='".$FiltroAsunto."'"))>0){
			$HasRelacionHC=true;
		}
	}
	
	if($where != NULL){
	$sql = "SELECT * 
			FROM hctiphis ".(($FiltroAsunto!='' && $HasRelacionHC)?',dbo.AsuntoHistorias ah':'')."
			WHERE (esFormato!=1 OR esFormato IS NULL) and $where and es_triage<>1
			".(($FiltroAsunto!='' && $HasRelacionHC)?' AND ah.CodAsunto=\''.$FiltroAsunto.'\' AND ah.CodHistoria=hctiphis.codigo ':'')." ORDER BY codigo";
	}else{
			$sql = "SELECT * 
					FROM hctiphis ".(($FiltroAsunto!='' && $HasRelacionHC)?',dbo.AsuntoHistorias ah':'')." 
					WHERE esFormato!=1 OR esFormato  IS NULL and es_triage<>1 ".(($FiltroAsunto!='' && $HasRelacionHC)?' AND ah.CodAsunto=\''.$FiltroAsunto.'\' AND ah.CodHistoria=hctiphis.codigo ':'')." ORDER BY codigo";
		}
	//	echo $sql;
	$cadena='';
	$result = $this->query($sql);
	while(($row = $this->nextRow($result))) {
		if(!$return){
			printf("<option  ".(!is_null($selected) ? ($selected === $row["codigo"] ? "selected" : "") : "")." value='".$row["codigo"]."'>".$row["detalle"]."</option>");
		}else{
			$cadena.="<option  ".(!is_null($selected) ? ($selected === $row["codigo"] ? "selected" : "") : "")." value='".$row["codigo"]."'>".$row["detalle"]."</option>";
		}
	}
	if(!$return){
		$this->freeResult($result);
	}else{
		return $cadena;
	}
    //echo $sql;
}

function comboEstadoPacienteCama($cama, $manual, $selected = NULL) {
	global $db;
	
	$rs = $db->select("*", "estado_cama", "cama = $cama AND manual = '$manual'");
	while(($row = $rs->nextRow())) {
		printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value='".$row["id"]."'>".$row["nombre"]."</option>");
	}
	$rs->free();
}

function crearComboConf($nombre, $codigo, $posicion, $tipo, $selected = NULL, $titulo = "",$class="", $readonly = NULL) {
	global $db;
	//echo "conf_combo codigo = '$codigo' AND posicion = '$posicion' AND tipo = '$tipo'";
	$rs = $db->select("*", "conf_combo", "codigo = '$codigo' AND posicion = '$posicion' AND tipo = '$tipo'");
	if($readonly == 1){
		$select_readonly = "disabled";
	}
	print ("<select name='$nombre' id='$nombre' class='letraDisplay ".$class."' title='$titulo' $select_readonly>");
	//print ("<option value=''>[SELECCIONE]</option>");
	while(($r = $rs->nextRow())) {
		$inabilitado = '';
		if( $r["estado"] == 0 && $selected == $r["valor"] ) {
			$inabilitado = "disabled style ='background:#ccccccfa'";		
		} elseif($r["estado"] == 0 && $selected != $r["valor"]) 
		{
           $inabilitado = "style = display:none";
		}
		
		print ("<option ".(!is_null($selected) ? ($selected == $r["valor"] ? "selected" : "") : "").
			" value='{$r['valor']}' ".$inabilitado.">{$r['nombre']}</option>\n");
	}
	print ("</select>");
	$rs->free();
}

function crearCheckBoxHC_Formato($name_id, $valor, $checked = false, $titulo = "",$class="") {
	$checkedTxt = $checked ? 'checked' : '';
	$input = "<input type='checkbox' name='{$name_id}' id='{$name_id}' value='{$valor}' {$checkedTxt} />";
	$table = "<table>"
				."<tr>"
					."<td class='letraDisplayNew'>{$valor}</td>"
					."<td class='letraDisplayNew'>{$input}</td>"
				."</tr>"
			."</table>";
	print ($table);
}

function crearComboConfEvo($nombre, $codigo, $posicion, $tipo, $selected = NULL, $titulo = "") {
	global $db;
	//echo "conf_combo codigo = '$codigo' AND posicion = '$posicion' AND tipo = '$tipo'";
	$rs = $db->select("*", "conf_combo_evo", "codigo = 1 AND posicion = '$posicion'");
	$html="<select name='$nombre' class='letraDisplay' title='$titulo'>";
	$html.="<option value=''>[SELECCIONE]</option>";
	while(($r = $rs->nextRow())) {
		$html.="<option ".(!is_null($selected) ? ($selected == $r["valor"] ? "selected" : "") : "").
			" value='{$r['valor']}'>{$r['nombre']}</option>\n";
	}
	$rs=$db->select("campos_busqueda,table_busqueda,where_busqueda","titulosEvo","posicion = '$posicion'");
	if(($r = $rs->nextRow())) {
		$res=$db->select(trim($r["campos_busqueda"]),trim($r["table_busqueda"]),trim($r["where_busqueda"]));
		while($row=$res->nextRow()){
		$html.="<option ".(!is_null($selected) ? ($selected == $row["0"] ? "selected" : "") : "").
			" value='{$row['0']}'>{$row['1']}</option>\n";
		}
	}
	
	
	$html.="</select>";
	
	echo $html;
	
	$rs->free();
}

function crearCasillaVerificacion($nombre, $checked = NULL, $titulo = "",$class="", $enabled = 1) {
	if($enabled != 1){
		$readonly = "readonly";
	}
	print "<input type='radio' title='$titulo' name='$nombre' id='{$nombre}S' class='".$class."' value='SI' ".($checked == "SI" ? "checked" : "")." $readonly/>SI";
	print "<input type='radio' title='$titulo' name='$nombre' id='{$nombre}N' class='".$class."' value='NO' ".($checked == "NO" ? "checked" : "")." $readonly/>NO";
}

function crearCampoFecha($nombre, $valor, $icono, $formulario, $titulo = "") {
	print "<input name=\"$nombre\" title='$titulo' type=\"text\" class=\"norm\" id=\"$nombre\" 
		onfocus=\"select()\" onblur=\"valFecha(this)\" onkeypress=\"return validFormat(this,event,'Date','')\" value=\"$valor\" maxlength=\"10\" />
        <a href=\"javascript:Calendario('$formulario','$nombre','General')\" 
        onmouseover=\"window.status='Buscar';return true\" title=\"Fecha\"><img src=\"$icono\" width=\"16\" height=\"14\" border=\"0\" /></a>";
}

function crearCampoHora($nombre, $valor, $titulo = "",$class="") {
	print "<input name=\"$nombre\" title='$titulo' type=\"text\" class=\"norm ".$class."\" id=\"$nombre\" 
		onblur=\"return validFormat(this,event,'Hour','nextFocus(\'departamento\')')\" 
		onfocus=\"select()\" onkeypress=\"return validFormat(this,event,'Hour','')\" 
		value=\"$valor\" maxlength=\"5\" />";
}

function crearCampoTextoCorto($nombre, $valor, $titulo = "", $width=30,$class="",$expresion='', $enabled = 1) {
	if($enabled != 1){
		$readonly = 'readonly';
	}
	$valor = trim($valor) == "" ? trim($valor) : $valor;
	
	if( !is_array($nombre)  ){
		
	print "<input expresion='".$expresion."' name=\"$nombre\" title='$titulo' type=\"text\" class=\"norm ".$class."\" id=\"$nombre\"
		onfocus=\"select()\" onkeypress=\"return validFormat(this,event,'Mayus','')\" value='".$valor."' size=\"$width\" $readonly/>";
		}else{
	print "<input expresion='".$expresion."' name=\"$nombre[0]\" title='$titulo' type=\"text\" class=\"norm ".$class."\" id=\"$nombre[1]\"
		onfocus=\"select()\" onkeypress=\"return validFormat(this,event,'Mayus','')\" value='".$valor."' size=\"$width\" $readonly/>";
			
			}
	}

function DameServicio($fuente) {
	$sql = "select nombre from sis_tipo where fuente = '$fuente' order by nombre";
	$result = $this->query($sql);
    $row = $this->nextRow($result);
	return $row[0];
}

function dameCCosto($codigo) {
	$rs = $this->select("nombre", "sis_costo", "codigo = '$codigo'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row[0];
}

function DameSalida($cantidad,$codigo,$ccosto){
  $salida = $this->getDato("($cantidad + residuo) / dosificacion", "stockDosificado,sis_prod", "stockDosificado.codigo = '$codigo'  and ccosto = '$ccosto' and sis_prod.codigo = stockDosificado.codigo");
  $this->query("UPDATE stockDosificado 
	  SET residuo = ($cantidad + residuo) % dosificacion
	  FROM stockDosificado, sis_prod
	  WHERE stockDosificado.codigo = '$codigo'  and ccosto = '$ccosto' and sis_prod.codigo = stockDosificado.codigo");
	$this->Update("stockDosificado", "salida = $cantidad + salida ","codigo = '$codigo'  and ccosto = '$ccosto' ");
	$this->Update("stockDosificado", "existencia = entrada - (salida + debaja) ","codigo = '$codigo'  and ccosto = '$ccosto' ");
	return $salida;
}

function dameExistenciaStock($codigo, $ccosto) {
	$r = $this->getDato("existencia", "stockDosificado", "codigo = '$codigo'  and ccosto = '$ccosto'");
	return empty($r) ? 0 : $r;
}

function dameExistencia($codigo, $codigo_art, $mes, $anio) {
	$mes = mes_actual($mes);
	$r = $this->getDato("SUM(existencia)", "existencia", "cod_stock= '$codigo' AND cod_prod= '$codigo_art'");
	return empty($r) ? 0 : $r;
}

function getCantidadArticuloLote( $articulo, $lote, $stock, $show = NULL ) {
    $r = $this->getDato("existencia", "existencia", "cod_prod = '$articulo' AND 
                        cod_lote = '$lote' AND cod_stock= '$stock'", $show);
    return empty($r) ? 0 : $r;
}

function esError( $error, &$error_ant, $show=null ) {
    if( $error != 0 )
        $error_ant = true;

    if( $show )
        echo "<br>$show. error: $error_ant | error ant: $error<br>";
}

function finTransaccion( $error ) {
    if( $error ) {
        $sql = "ROLLBACK TRAN";
        $this->insertar($sql);
    } else {
        $sql = "COMMIT TRAN";
        $this->insertar($sql);
    }
	//echo $sql;
}

function iniciarTransaccion() {
    $sql = "BEGIN TRAN";
	//echo $sql;
    $this->insertar($sql);
}

function comboTipoPago($selected = NULL, $where = NULL,$return=false, $estudio = NULL) {
	global $db;
	
	$estrato = $this->getDato("cod_clasi","sis_maes","con_estudio = '".$estudio."'");
	
	
	$sql = "select Tp.* from sis_tipopago As Tp INNER JOIN dbo.EstratoTipoPago Rtp ON Tp.id = Rtp.TipoPago And Rtp.Estrato = '" .$estrato ."' " .(empty($where) ? "" : " WHERE $where")." order by tipo";
	
	$result = $db->query($sql);
	
	$hidden="";
	while($row = $result->nextRow()) {
		printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["descripcion"]."</option>");
		$hidden.="<input type='hidden' name='tip".$row["id"]."' id='tip".$row["id"]."' value='".$row["tipo"]."'>";
	}
	//printf('<input type="text"  value="'.$sql.'" />' );
	$result->free();
	if($return){
		return	$hidden;
	}

}

function comboServicio($selected = NULL, $where = NULL, $estudio = NULL, $setServicioDefault = false) {
	global $db;
	
	if (is_null($estudio)) {
		$sql = "SELECT * FROM sis_tipo ".($where == NULL ? "" : "WHERE $where ")."ORDER BY fuente";
	} else {
		$tipo = $this->getDato("tipo_estudio", "sis_maes", "con_estudio = $estudio");
		$sql = "SELECT distinct t.*  
			FROM sis_tipo AS t, sis_maes AS m, sis_contrato AS c, ufcuenta ufc 
			WHERE m.con_estudio = $estudio AND m.contrato = c.codigo AND c.servicio = t.fuente
			AND c.$tipo = 1 AND ufc.id = c.ufuncional AND ufc.codigo_uf = m.ufuncional $where
			ORDER BY t.nombre, t.fuente";
		
	}	

	$result = $this->RSAsociativo($sql);
	if ($setServicioDefault) {
		$servicios = implode(',', array_map(function ($r) {
			return $r['id'];
		}, $result));
		if ($servicios !== '') {
			$selected = $this->getDato('fuente_tips, SUM(Cargos.total) Total', 'sis_deta Cargos', 
				"Cargos.estudio='{$estudio}' AND Cargos.NoFacturable=0 AND Cargos.fuente_tips IN ({$servicios}) "
				."GROUP BY Cargos.fuente_tips ORDER BY Total DESC");
		}
	}
	foreach ($result as $k => $row) {
		printf("<option ".(!is_null($selected) ? ($selected == $row["fuente"] ? "selected" : "") : "")." value=".$row["fuente"].">".$row["nombre"]."</option>");
	}
}

function comboCCosto($selected = NULL, $where = NULL) {
	$result = $this->select("*", "sis_costo", $where);
	while($row = $this->nextRow($result)) {
		printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["nombre"]."</option>");
	}
	$this->freeResult($result);
}

/* Crea una lista UL sin ordenar*/
function crearLista($tabla, $tipo) {
	global $db;
	
	$SQL = "SELECT * FROM sismaelm WHERE tabla = '$tabla' AND tipo = '$tipo' ORDER BY nro";
	$result = $db->query($SQL);
	while($row = $result->nextRow()) {
		echo "{$row['nombre']} {$row['desplegable']}<br />";
	}
	$result->free();
}

function crearListaNew($tabla, $tipo) {
	global $db;
	
	$SQL = "SELECT * FROM sismaelm WHERE tabla = '$tabla' AND tipo = '$tipo' ORDER BY nro";
	$result = $db->query($SQL);
	while($row = $result->nextRow()) {
		echo "{$row['nro']} {$row['desplegable']}<br />";
	}
	$result->free();
}

function comboAyudaDiag($selected = NULL, $where = NULL, $cup = "") {
	global $db;
	$plantillas = $this->query("Exec dbo.spProcsPlantillasParaclinicos @Op = 'S_Plantillas', @CUP = '".$cup."'");
	if($plantillas->numRows() == 0){
		$sql = "SELECT * FROM sis_ayudadx ".($where == NULL ? "" : "WHERE $where ")."ORDER BY fuente";
		$result = $db->query($sql);
	}else{
		$result = $plantillas;
	}
	while($row = $result->nextRow()) {
		printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["nombre"]."</option>");
	}
	$result->free();
}

function estadoEstudio($estudio) {
	return $this->getDato("estado", "sis_maes", "con_estudio = $estudio");
}

function existeAyudaDiag($where = NULL) {
	global $db;
	
	$sql = "SELECT * FROM sis_ayudadx WHERE $where";
	$result = $db->query($sql);
	$cant = $result->numRows();
	$result->free();
	return $cant;
}

function getListaMedicos() {
	$rs = $this->select("codigo, nombre", "sis_medi", "es_medico = 1");
	$medicos = array();
	while(($row = $this->nextRow($rs))) {
		$medicos[$row[0]] = $row[1];
	}
	return $medicos;
}

function crearMovimientos($estudio, $oper = NULL, $sw = NULL, $where = "CE") {
	if (empty($estudio)) {
		return;
	}
	$sql = "SELECT t.nombre, cod_medico, cod_servicio, descripcion, 
		cantidad, total, vlr_servicio, fecha_servicio, nom_usuario, num_servicio
		FROM sis_deta, sis_tipo as t
		WHERE t.fuente = sis_deta.fuente_tips AND sis_deta.estudio = $estudio 
		ORDER BY t.nombre, cod_servicio, fecha_servicio";
	$rs = $this->query($sql);
	
	if ($this->numRows($rs) == 0) return "";
	
	$medicos = $this->getListaMedicos();
	
	$cad = "<script language='JavaScript'>".
		"arbol2 = new TreeMenu('arbol2', '../../Comun/treeview/images', 'arbol2', '_self');\n";

	$i = 0;
	$row = $this->nextRow($rs);
	$temp = $row[0];

	do {
		$acum = 0;
		$cad .= "arbol2.n[".$i."] = new TreeNode('{$row[0]}', '<td align=right width=30>&nbsp;</td><td align=right width=90>&nbsp;</td><td align=right width=90>&nbsp;</td><td align=right width=80>&nbsp;</td><td align=right width=100>&nbsp;</td>', 'iconito.gif', '#', false, true, '');\n";
		$j = 0;
		while ($row[0] == $temp) {
			$row['fecha_servicio'] = convertirFecha($row['fecha_servicio']);
			$cad .= "arbol2.n[".$i."].n[".$j."]".
				" = new TreeNode('<font color=#434498>{$row['cod_servicio']}&nbsp;&nbsp;</font>".((strlen($row['descripcion']) > 30 ? substr($row['descripcion'], 0, 30)."..." : $row['descripcion']))."', '".
				"<td align=right width=40><font color=#434498><b>{$row['cantidad']}</b></font></td>".
				"<td align=right width=85><font color=#434498><b>\${$row['vlr_servicio']}</b></font></td>".
				"<td align=right width=85><font color=#434498><b>\${$row['total']}</font></b></td>".
				"<td align=center width=100><font color=#434498>{$row['fecha_servicio']}</font></td>".
				"<td align=left width=110><font color=#434498><b>".substr($row['nom_usuario'], 0, 8)."</b></font></td>".
				"', 'iconito.gif', '#', false, true, '{$row['descripcion']}');\n";
			$cad .= "arbol2.n[".$i."].n[".$j++."].n[0] = new TreeNode('<font color=#434498>Numero Servicio:&nbsp;{$row['num_servicio']}</font>', ".
				"'<td width=100%><font color=#434498><b>{$row['1']}:&nbsp;&nbsp;{$medicos[$row[1]]}</b></font></td>', 'iconito.gif', ".
				(!is_null($sw) 
					? "'CtrlServicios$where.php?".(is_null($oper) ? "" : "operacion=$oper&").
					"estudio=$estudio&serv={$row['num_servicio']}'"
					: "'#'").
				", false, true, '');\n";
			$acum += $row['total'];
			$viejo = $row[0];
			if (!($row = $this->nextRow($rs))) {
				$cad .= "arbol2.n[".$i."].title += '<td><font color=#434498><b>\$$acum</b></font></td>';\n";
				break 2;
			}
		}
		$cad .= "arbol2.n[".$i."].title += '<td><font color=#434498><b>\$$acum</b></font></td>';\n";
		$temp = $row[0];
		$i++;
	} while ($row);
	$cad .= "arbol2.drawMenu();</script>";
	return $cad;
}

function getConElem($tabla,$tipo){
	  $sql = "SELECT valor FROM elementos WHERE tabla='$tabla' and tipo = $tipo";
	$result=$this->query($sql);
	$row = $this->nextRow($result);
	$codigo = $row['valor'];
	 $this->freeResult($result);

return $codigo;
}

function setConsecutivo(){
	$sql = "UPDATE consecutivo SET valor = valor + 1 WHERE tabla='articulo'";
	$result=$this->query($sql);
	 $this->freeResult($result);

}

/*-- ============================================================================== */
/*-- ============	Genera una lista de una SQL			             ============== */
/*-- ============================================================================== */
function listGenericHash($query) {
	global $db;
 	
	try {
		$result = $db->limitQuery($query, 100);
		$this->numC = $result->numFields();
		$this->numF = $result->numRows();
		
		while(($row = $result->nextRow())) {
			$lista[] = $row;
		}
		
       	 $result->free(); 

	    if (!empty($lista))
    		return $lista; 
		
  }catch(SQLException $excepcion){};
  
}//function

function tipoManual($estudio) {
	$rs = $this->select(
		"tipo", 
		"sis_maes AS m, sis_manual AS ma, contratos AS c",
		"m.contrato = c.codigo AND c.manual = ma.codigo AND m.con_estudio = $estudio");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row[0];
}

function tipoManualCodigo($codigo) {
	$rs = $this->select(
		"tipo", 
		"sis_manual",
		"codigo = '$codigo'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row[0];
}

function tipoManualEmpresa($contrato) {
	return $this->getDato(
		"tipo", 
		"contratos AS c, sis_manual AS ma",
		"c.manual = ma.codigo AND c.codigo = '$contrato'");
}

function getContrato($estudio) {
	return $this->getDato("contrato", "sis_maes", "con_estudio = '$estudio'");
}

function getNomContrato($estudio) {
	$codcontra= $this->getContrato($estudio);
	return $this->getDato("nombre", "contratos", "codigo = '$codcontra'");
}

function DameElementos($columna,$tabla,$tipo,$where){
$sql = "select $columna from sismaelm where TABLA = '$tabla' AND TIPO = '$tipo' AND $where";
$result = $this->Execute($sql);
return $result;
}

function codigoManual($estudio) {
	$rs = $this->select(
		"manual", 
		"sis_maes AS m, contratos AS c",
		"m.contrato = c.codigo AND m.con_estudio = '$estudio'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row[0];
}

function codigoManualMed($estudio) {
	$rs = $this->select(
		"manual_med",
		"sis_maes AS m, contratos AS c",
		"m.contrato = c.codigo AND m.con_estudio = '$estudio'");
	$row = $this->nextRow($rs);
	$this->freeResult($rs);
	return $row[0];
}

function codigoManualContrato($codigo) {
	return $this->getDato("manual", "contratos", "codigo = '$codigo'");
}

function Mylista($tablas, $tipo, $where = NULL, $selected = NULL, $otro = NULL){
		global $db;
		
		if($where != NULL){
			if ($otro != NULL) {
				$sql = "select * from $tablas where $where $otro";
			} else {
				$sql = "select * from $tablas where $where ";
			}
		}else{
		$sql = "select * from $tablas ";
		}
		$result=$db->query($sql) or die("Excepcion en la Funcion: Model::MyLista Model");
		$arreglo = array();
		
		while($row = $this->nextRow($result))
		{   if($tipo == 0)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).">".trim(substr($row["nombreve"], 0, 20))."</option>");
			if($tipo == 1)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).">".trim($row["nombre"])."</option>");
			if($tipo == 2)
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".$row["nombre"]."</option>");
			if($tipo == 3)
			$arreglo = array("linea" => "<option value=".$row["codigo"].">".$row["nombre"]."</option>");
			if($tipo == 4)
			printf("<option value=".trim($row["usuario"]).">".trim($row["nombre"])."</option>");
			if($tipo == 5)
			printf("<option ".(!is_null($selected) ? ($selected == $row["fuente"] ? "selected" : "") : "")." value=".$row["fuente"].">".$row["nombre"]."</option>");
			if($tipo == 6)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["detalle"]."</option>");
			if($tipo == 7)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".$row["codigo"].">".$row["descripcion"]."</option>");
			if($tipo == 8)
			printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["descripcion"]."</option>");
			if($tipo == 9)
			printf("<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["mes"]."</option>");
			if($tipo == 10)
			printf("<option ".(!is_null($selected) ? ($selected == $row["ccosto"] ? "selected" : "") : "")." value=".$row["ccosto"].">".$row["nombre"]."</option>");
			if($tipo == 11)
			printf("<option ".(!is_null($selected) ? ($selected == $row["codigo"] ? "selected" : "") : "")." value=".trim($row["codigo"]).trim($row["nombre"]).">".trim($row["nombre"])."</option>");
			if($tipo == 12)
			printf("<option ".(!is_null($selected) ? ($selected == $row["fuente"]."-".$row["filerips"] ? "selected" : "") : "")." value=".$row["fuente"]."-".$row["filerips"].">".$row["nombre"]."</option>");

			//printf("<option value=".$row["fuente"].">".$row["nombre"]."</option>");
			

		}
	    $this->freeResult($result);
	
return $datos;
		
	}

function ListaTmp($tabla,$datos,$where,$tablaTemp){
	   $sql = "Create Local Temporary Table $tablaTemp AS  select $datos from $tabla where $where";
     //echo "$sql";
     $this->freeResult($this->query($sql));

} 

/*
	$lista : lista donde se va a buscar
	$codigo : elemento a buscar
	$nombre : nombre del campo con que se va a comparar, "codigo" por defecto
*/
function existeElem($lista, $codigo, $nombre = "codigo") {
	$size = sizeof($lista);
	for ($i = 0; $i < $size; $i++) {
		if ("A".$lista[$i][$nombre] == "A".$codigo) {
			return $i;
		}
	}
	
	return -1;
}

/*
	$lista : lista donde se va a buscar
	$element1 y $element2: elemento x los que se va a buscar
	$nombre : nombre del campo con que se va a comparar, "codigo" por defecto
*/
function existeElem2($lista, $element1, $element2) {
    
	for ($i = 0; $i < sizeof($lista); $i++) {
        if( "A".$lista[$i]["codigo"] == "A".$element1 ) {
            if( "A".$lista[$i]["lote"] == "A".$element2 ) {
                return $i;
                //echo "e1:$element1 - e2:$element2";
            }
        }
	}

	return -1;
}

function ExisteElemento($lista, $codigo, $nombre = "codigo", $columnaTipo = "", $valorTipo = "") {
	for ($i = 0; $i < sizeof($lista); $i++) {
		if ("A".$lista[$i][$nombre] == "A".$codigo && $lista[$i][$columnaTipo] == $valorTipo ) {
			return $i;
		}
	}
	
	return -1;
}


function nombreElemento($tabla, $tipo, $select) {
	global $db;
	$rs = $this->select("desplegable", "sismaelm", "tabla = '$tabla' AND tipo = '$tipo' AND valor = $select");
	$row = $this->nextRow($rs);
	return $row ? $row[0] : false;
}


function tipo_transaccion($ids="", $valor=""){
	global $db;
	if($ids!=""){
		$ids=" where Id in(".$ids.")";
	}
	$sql = "SELECT * FROM tipo_transaccion ".$ids." order by sigla";
	$result = $db->query($sql);

	while ($row = $result->nextRow()) {
		echo "<option value=".$row["sigla"]." ".(!is_null($valor) ? ($valor == $row["sigla"] ? "selected" : "") : "").">".$row["nombre"]."</option>";
	}
}


function elementos($tabla, $tipo, $clase = NULL, $selected = NULL, $where = ""){
	global $db;
	
	$sql = "SELECT * FROM sismaelm WHERE tabla = '$tabla' AND tipo = '$tipo' $where ORDER BY nro";
	$result = $db->query($sql);
	$html = "";
	while ($row = $result->nextRow()) {
		
		if($selected == NULL){
			if($row["selected"]=='1'){
				$selected=$row["valor"];
			}
		}
		
		
		if ($clase == 1) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["valor"] ? "selected" : "") : "")." value=".$row["valor"].">".utf8_encode($row["desplegable"])."</option>");
		} else if ($clase == 2) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value='".$row["nombre"]."'>".utf8_encode($row["nombre"])."</option>");
		} else if ($clase == 3) {
			$datos = array("titulo" => $row["nombre"] , "mensaje" => $row["desplegable"]);
			return $datos;
		} else if ($clase == 4) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".utf8_encode($row["nombre"])."</option>");
		} else if ($clase == 5) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".utf8_encode($row["desplegable"])."</option>");
		} else if ($clase == 6) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["valor"] ? "selected" : "") : "")." value=".$row["valor"].">".utf8_encode($row["nombre"])."</option>");
		
		}else if($clase == 7){
			$html .= "<option ".(!is_null($selected) ? ($selected == $row["valor"] ? "selected" : "") : "")." value='".$row["valor"]."'>".utf8_encode($row["desplegable"])."</option>";
		}else if($clase == 8){
			$html .= "<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value='".$row["nombre"]."'>".utf8_encode($row["desplegable"])."</option>";
		}
	}
	
	$result->free();
	
	if($clase == 7 || $clase == 8){
		return $html;
	}
}


function combo($consultar_en,$tabla, $tipo, $clase = NULL, $selected = NULL, $where = ""){
	global $db;
	
	$sql = "SELECT * FROM ".$consultar_en." WHERE tabla = '$tabla' AND tipo = '$tipo' $where ORDER BY nro";
	
	
	
	$result = $db->query($sql);

	while ($row = $result->nextRow()) {
		if ($clase == 1) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nro"] ? "selected" : "") : "")." valorItem=".$row["valor"]." value=".$row["nro"].">".utf8_encode($row["desplegable"])."</option>");
		} else if ($clase == 2) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value='".$row["nombre"]."'>".utf8_encode($row["nombre"])."</option>");
		} else if ($clase == 4) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".utf8_encode($row["nombre"])."</option>");
		} else if ($clase == 5) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["nombre"] ? "selected" : "") : "")." value=".$row["nombre"].">".utf8_encode($row["desplegable"])."</option>");
		} else if ($clase == 6) {
			printf("<option ".(!is_null($selected) ? ($selected == $row["valor"] ? "selected" : "") : "")." value=".$row["valor"].">".utf8_encode($row["nombre"])."</option>");
		} else if ($clase == 3) {
			$datos = array("titulo" => $row["nombre"] , "mensaje" => $row["desplegable"]);
			return $datos;
		}
	}
	
	$result->free();
}



function listarSedes($selected = NULL){
	global $db;
	$sql = "SELECT * FROM seriales";
	$result = $db->query($sql);
	$cadena='';
	while ($row = $result->nextRow()) {
		$cadena.="<option ".(!is_null($selected) ? ($selected == $row["id"] ? "selected" : "") : "")." value=".$row["id"].">".$row["r_social"]."</option>";
	}
	return $cadena;
}



function NombreElementos($valor){
	return $this->getDato("desplegable", "sismaelm", 
		"tabla = 'CIRUGIA' AND tipo = 'TPS' AND valor = $valor");;
}

function Execute($c, $show = NULL) {
	    global $db;
		
		$result=$db->query($c);
		if ($show != NULL)
			echo $c;
		$campos = array();
		while ($registros = $this->nextRow($result))
			{	$campos[] = $registros;	}
      	return $campos;
}

function RSAsociativo($c, $show = NULL) {
	    global $db;
		$result=$db->query($c);
		if ($show != NULL)
			echo $c;
		$campos = array();
		if(is_object($result)){
			$k = 0;
			while ($registros = $result->nextRowAssoc())
				{	$campos[$k++] = $registros;	}
		}
      	return $campos;
}

function getResultSetAsObjects($c, $show = NULL) {
	    global $db;
		$result=$db->query($c);
		if ($show != NULL)
			echo $c;
		$objectsList = array();
		if(is_object($result)){			
			$objectsList = $result->getResultToObjtec();
		}
      	return $objectsList;
}

function MDBinsertar($collecion,$array) {
	    global $db;
		$db->Mongoinsert($collecion,$array);
		
      	
}


function MDBupdate($collecion,$array,$condicion) {
	    global $db;
		$db->Mongoupdate($collecion,$array,$condicion);
		
      	
}

function Url($c,$show = NULL){
	    global $db;
		if($show != NULL) echo $c;
		$result=$db->query($c);
		return http_build_query($result->nextRowAssoc());
}

function Assoc($c,$show = NULL){
	    global $db;
		if($show != NULL) echo $c;
		$result=$db->query($c);
		return $result->nextRowAssoc();
}

function Urls($arreglo){
	return http_build_query($arreglo);
}

function obtenerPorcentaje($servicio, $cirugia, $tipo_rea, $tipo_manual, $medico_rea) {
	$porcentaje = 0;
	
	$medico_rea = empty($medico_rea) ? "" : " AND tipo = '$medico_rea'";
	
	switch ($servicio) {
		case 1:
			$porcentaje = $this->getDato("medico",
				"liquidacion", "liquidacion = $tipo_rea AND resolucion = '$tipo_manual'
				AND cirugia = $cirugia$medico_rea");
			break;
		case 2:
			$porcentaje = $this->getDato("anestesiologo",
				"liquidacion", "liquidacion = $tipo_rea AND resolucion = '$tipo_manual'
				AND cirugia = $cirugia$medico_rea");
			break;
		case 3:
			$porcentaje = $this->getDato("ayudante",
				"liquidacion", "liquidacion = $tipo_rea AND resolucion = '$tipo_manual'
				AND cirugia = $cirugia$medico_rea");
			break;
		case 4:
			$porcentaje = $this->getDato("dsala",
				"liquidacion", "liquidacion = $tipo_rea AND resolucion = '$tipo_manual'
				AND cirugia = $cirugia$medico_rea");
			break;
		case 5:
			$porcentaje = $this->getDato("materiales",
				"liquidacion", "liquidacion = $tipo_rea AND resolucion = '$tipo_manual'
				AND cirugia = $cirugia$medico_rea");
			break;
	}
	
	return $porcentaje;
}

function obtenerValorProcedimientoCirugia($manual, $proc, $tipo = 'All', $uvr = NULL, $servicio = NULL, $codigoCirugia = NULL) {
	//global $db;
   $rs = $this->RSAsociativo("SELECT dbo.fnValorProcedimientoCirugia('".$codigoCirugia."','".$manual."','".$proc."',".(int)$uvr.",".(int)$servicio.") AS Valor");
   $r = $rs[0];
	/*switch ($tipo) {
	case 'All': case 'SOAT':   
	   $sql  = "select Isnull(manual_$manual,0) as valor from sis_proc where codigo='$proc' and tipo = '$tipo' ";
	   $rs = $this->nextRow($db->query($sql));
	   break;
	   
	case '256': case '200':
	   if($servicio == '5' || ($servicio == '4' && $uvr < 451)) {
	   $sql  = "select Isnull(manual_$manual,0) as valor from sis_proc where codigo='$proc' and tipo = '$tipo' ";
	   //echo "Normal : [".$servicio."]".$sql;
	   } else {
	   //echo "Multiplico : [".$servicio."]";
			if ($servicio == 3 && $uvr >= 450) {
				$sql  = "select CONVERT(numeric(18, 2), round(isnull(manual_$manual,0) / 360, 0) * 960 * $uvr) as valor from sis_proc where codigo='$proc' and tipo = '$tipo' ";
			} else {
				$sql  = "select (isnull(manual_$manual,0) * $uvr ) as valor from sis_proc where codigo='$proc' and tipo = '$tipo' ";
			}
	   }	  
	   $rs = $this->nextRow($db->query($sql));
	   break;
	
	case '312':
	   $sql  = "select (manual_$manual) as valor from sis_proc where codigo='$proc' and tipo = '$tipo' ";
	   $rs = $this->nextRow($db->query($sql));
	   break;
}*/     
return $r["Valor"];
}

function mensaje($texto,$tipo,$estilo='',$return=NULL,$advise=false){
		
			$borderColor='#74B33E';
			$letraColor='#21432B';
			$bgColor='#B5D699';
			if($tipo=='error'){
				$borderColor='#930';
				$letraColor='#FFF';
				$bgColor='#EE6262';
			}else if($tipo=="azul"){
				$borderColor='#34b0c4';
				$letraColor='#3366A8';
				$bgColor='#34b0c4';	
			}
			if($advise){
				$bgColor='#FFFF99';	
				$letraColor='#805713';
			}
			/*echo '<div class="error_message">'.$texto.'</div>';*/
				$html= '<table cellpadding="0" cellspacing="0" width="100%">
						<tr height="30px;">
							<td align="center" bgcolor="'.$bgColor.'" style="border:solid; border-color:'.$borderColor.';border-width:2px; color:'.$letraColor.' "><label id="contenidoMensaje">'.utf8_encode($texto).'</label></td>	
						</tr>
					  </table>';
				if($return==NULL){
					echo $html;	  
				}else{
					return $html;	
				}
		
	}


function Update($tabla,$campos,$where = NULL,$show = NULL) {
        global $db;
		
		$x = stripos($tabla,'(');
		if($x > 0)
    	 $tabla = substr($tabla,0,$x);
		if($where != NULL){
		$sql ="UPDATE $tabla SET $campos WHERE $where";		
		}else{
		$sql ="UPDATE $tabla SET $campos";		
		}
		if ($show != NULL)
			echo "$sql<br>";
		
		//echo $sql;
		$row = $db->query($sql);
		  if($row >= 1)
			{	$tx = true;
			}else{
				$tx = false;
			}
	return $tx;
	}

	function crearListaSelect($sql, $campo_valor, $campo_display, $selected = NULL, $group_by = NULL){
		global $db;
		$datos = "";
		if ($sql != NULL){
			//echo $sql;
			$result=$db->query($sql) or die("Excepcion en la Funcion: Model::crearListaSelect Model");
            
			$grupo = "";
			while($row = $this->nextRow($result)){ 
				$valor = $row[$campo_valor];
				$display = $row[$campo_display];
				if ($group_by != NULL && $grupo != $row[$group_by]){
                    $grupo = $row[$group_by];
                    print("<optgroup label=\"$grupo\">");
                }
				if ($selected != NULL && $valor == $selected){
					print("<option value=\"$valor\" selected>$display</option>" );
				}else{
					print("<option value=\"$valor\">$display</option>" );
				}
			}
		    $this->freeResult($result);
		}
		
		return $datos;
	}
	
	function getLastErrorCode(){
		global $db;
		$result=$db->query("SELECT @@ERROR") or die("Excepcion en la Funcion: Model::getLastErrorCode Model");
		$row = $this->nextRow($result);
		$this->freeResult($result);
		return $row[0];
	}

    /* Funciones GL */
    function crearClienteGL($id, $nit, $nombre, $dir, $tel) {
		if ($_SESSION['interfazGL'] != true) {
			return -1;
		}

		if ($this->getDato("COUNT(*)", "{$_SESSION['databaseGL']}.dbo.clientes", "clinit = '$nit' AND clivalor != $id") == 0) {
			$this->query("EXEC {$_SESSION['databaseGL']}.dbo.sp_create_clientes '$nit', '$nombre', '$dir', '$tel', $id");
			return true;
		} else {
			return false;
		}
	}

    function crearServicioGL($id, $nombre, $cuenta) {
		if ($_SESSION['interfazGL'] != true) {
			return -1;
		}

		$this->query("EXEC {$_SESSION['databaseGL']}.dbo.sp_create_servicios '$id', '$nombre', '$cuenta'");
	}

    function crearMaestrosArticulosGL($id, $codigo, $descripcion, $tabla) {
		if ($_SESSION['interfazGL'] != true) {
			return -1;
		}

		switch ($tabla) {
			case "lineas": $tabla = "lineas"; break;
			case "grupo": $tabla = "descrip"; break;
			case "marcas": $tabla = "marcas"; break;
			case "medidas": $tabla = "medidas"; break;
			case "formas": $tabla = ""; return;
		}

		$this->query("EXEC {$_SESSION['databaseGL']}.dbo.sp_create_$tabla '$codigo', '$descripcion'");
	}

    function insertarLote($articulo, $cantidad, $numero, $fecha, $bodega) {
        $count = $this->getDato("COUNT(*)", "lote", 
                "articulo = '$articulo' AND numero = '$numero' AND bodega = '$bodega'");
        
        if ($count > 0) {
            $SQL = "UPDATE lote SET
                entrada = entrada + $cantidad".
                (empty($numero) ? "" : ", fecha_vence = '$fecha'").
                " WHERE articulo = '$articulo' AND numero = '$numero' AND bodega = '$bodega'";
        } else {
            $fecha = empty($numero) ? "NULL" : "'$fecha'";
            $numero = empty($numero) ? "''" : "'$numero'";
            $SQL = "INSERT INTO lote(articulo, entrada, numero, fecha_vence, bodega, ano)
                VALUES ('$articulo', '$cantidad', $numero, $fecha, '$bodega','".date('Y')."')";
        }
        $r = $this->query($SQL);

        //echo $SQL;
        //return $SQL;
        return $this->affectedRows() > 0 ? true : false;
    }

    function descontarLote($articulo, $cantidad, $lote, $bodega) {        
        $lote = (strcmp($lote, "Sin Lote") == 0) ? "" : $lote;

        $count = $this->getDato("COUNT(*)", "existencia",
                "cod_prod = '$articulo' AND cod_lote = '$lote' AND cod_stock='".$bodega."'");

        if( $count > 0 ) {
            $sql = "UPDATE existencia
                SET existencia = (existencia - $cantidad)
                WHERE (cod_prod = '$articulo') AND (cod_lote = '$lote') AND (cod_stock = '$bodega')";
            $this->insertar($sql);
       
            return $this->affectedRows();
        } 
       // echo "</br>".$sql.".....";
        return 0;
    }

    function exportarMovimientosInventarioGL($oper, $comp, $fecha, $detalle) {
        if ($_SESSION['interfazGL'] != true) {
			return -1;
		}

        $r = $this->select("*", "cfgcostos", "", NULL, 1);
        if ($oper == "SL") {
            $debito = $r['costo'];
            $credito = $r['inventario'];
        } else if ($oper == "DV-P") {
            $debito = $r['inventario'];
            $credito = $r['costo'];
        } else if ($oper == "DV-G") {
            $debito = $r['inventario'];
            $credito = $r['devolucion'];
        } else if ($oper == "DV-C") {
            $debito = $this->getDato("p.cuenta", "proveedor AS p, nota_credito AS n",
                "n.comprobante = $comp AND n.proveedor = p.id");
            $credito = $r['inventario'];
        }

        $valor = $this->getDato("SUM(cantidad * costoprom)", "sis_inven", "comprobante = $comp");

        if ($valor == 0) {
            return;
        }

        $rs = $this->query("EXEC {$_SESSION['databaseGL']}.dbo.importarCostosInventario
            '$debito', '$credito', '$fecha', '$detalle', $valor, '{$r['tipo_documento']}'");

        $r2 = $this->nextRow($rs);
        $SQL = "INSERT INTO log_costos_exportados VALUES('$comp', '{$r2[0]}',
            '{$r['tipo_documento']}')";
        $this->query($SQL);
    }

    function getCodigoCamaPaciente($estudio, $show=NULL) {
        $cama = $this->getDato("codigo", "sis_cama", "estudio_paciente = $estudio ORDER BY codigo DESC", $show);
        if( $cama )
            return $cama;
        else
            return -1;
    }

    function getCodigoCamaEntrega($id, $show=NULL) {
        $cama = $this->getDato("cama", "entrega", "id = $id", $show);
        if( $cama )
            return $cama;
        else
            return -1;
    }

    function getCodigoCamaDevoluciones($id, $show=NULL) {
        $cama = $this->getDato("cama", "devoluciones", "id = $id", $show);
        if( $cama )
            return $cama;
        else
            return -1;
    }

    function getCodigoCamaOrdenesMedicas($id, $show=NULL) {
        $cama = $this->getDato("cama", "nueva_orden_medica", "numero = $id", $show);
        if( $cama )
            return $cama;
        else
            return -1;
    }

	function selectYesNOOption($name, $option=-1) {
		switch ($option) {
			case NULL:
				$select1 =  "selected='selected'";
				break;
			case 1:
				$selectYes =  "selected='selected'";
				break;
			case 0:
				$selectNo =  "selected='selected'";
				break;

			default:
				$select1 =  "selected='selected'";
				break;
		}

		echo <<<HTML
		<select name="$name" class="letraDisplay" id="$name">
                <option $select1>[Seleccione]</option>
                <option $selectYes value="1">Si</option>
                <option $selectNo value="0">No</option>
              </select>
HTML;
	}

    function crearSelect($op, $selected, $name) {
        switch ($op) {
            case 1:
               $options = array("1"=>"Empieza Con", "2"=>"Contiene", "3"=>"Termina");
                break;
                    
            default:
                $options = $op;
                break;
        }
            
        echo "<select name='$name' class='letraDisplay' id='$name'>";
        foreach ($options as $value => $label){
            if( $selected == $value )
                echo "<option selected value='$value'>$label</option>";
            else
                echo "<option value='$value'>$label</option>";
        }
        echo "</select>";
    }
	
	function esSuperUsuario($cedula){
		$dato = $this->getDato("n.super_usuario","usuario u, sis_nivelacceso n","u.nivel = n.codigo AND cedula = '".$cedula."'");
		
		return ($dato == 1)? true : false;
	}
	
	function tieneSolicitudTrasladoUFP($estudio, $esIngreso = false){
		if(!$esIngreso){
			$i = $this->getIngreso($estudio);
		}else{
			$i = $estudio;
		}
		$sols = $this->getDato("COUNT(id)",
			"solicitud_tr_uf","ingreso = '{$i}' AND estado = 0");
		if($sols > 0){
			return true;
		}else{ 
			return false;	
		}
	}
	
	function UFDestinoPaciente($estudio, $esIngreso = false){
		if(!$esIngreso){
			$ingreso = $this->getIngreso($estudio);
		}else{
			$ingreso = $estudio;
		}
		
		$uf = $this->getDato("u.descripcion",
			"solicitud_tr_uf INNER JOIN ufuncionales u ON ufdestino = u.id",
			"ingreso = '$ingreso' AND estado = 0");
			
		return $uf;
	}
	
	
	function UFDestinoMedico($estudio){
		$ingreso = $this->getIngreso($estudio);
		$uf = $this->getDato("u.nombre",
			"solicitud_tr_uf INNER JOIN sis_medi u ON medico = u.cedula",
			"ingreso = '$ingreso' AND estado = 0");
			
		return $uf;
	}
	
	function comboUND_Mcto($medicamento, $und, $dataOnly = false){
		$cod_und = $this->getDato("codigo","medidas","id = ".(int)$und);
		$params = "@op = 'S_GetEQMcto_Ordenamiento', @Medicamento = '".$medicamento."'";
		$rs = $this->RSAsociativo("Exec spEquivalencias ".$params);
		
		if ($dataOnly) {
			$data = array();
			$data[0] = array('IdEQ' => 0, 'EQ' => $cod_und);

			foreach ($rs as $k => $r) {
				$data[] = array('IdEQ' => $r['id'], 'EQ' => ($r["CodigoBase"]==$cod_und?$r["CodigoEQ"]:$r["CodigoBase"]));
			}

			return $data;
		} else {
			echo "<option selected='selected' value='0'>".$cod_und."</option>";
			foreach($rs as $k => $row){			
				echo ("<option value='".$row["id"]."'>".($row["CodigoBase"]==$cod_und?$row["CodigoEQ"]:$row["CodigoBase"])."</option>");
			}
		}
	}
	
	function comboUND($und, $dataOnly = false) {
		$cod_und = $this->getDato("codigo","medidas","id = $und");
		$sql = "SELECT DISTINCT eq.id, (SELECT TOP 1 m.codigo FROM medidas AS m WHERE m.id = eq.und_base), (SELECT TOP 1 m.codigo FROM medidas AS m WHERE m.id = eq.und_equi) FROM sis_equivalencias AS eq WHERE eq.und_base = (SELECT TOP 1 m.id FROM medidas AS m WHERE m.id = '".$und."') OR eq.und_equi = (SELECT TOP 1 m.id FROM medidas AS m WHERE m.id = '".$und."') ORDER BY eq.id";
		$result = $this->query($sql);

		if ($dataOnly) {
			$rs = array();
			$rs[0] = array('IdEQ' => 0, 'EQ' => $cod_und);

			while($row = $this->nextRow($result)) {
				$rs[] = array('IdEQ' => $row[0], 'EQ' => ($row[1]==$cod_und?$row[2]:$row[1]));
			}

			return $rs;
		} else {
			printf("<option selected='selected' value='0'>".$cod_und."</option>");
			while(($row = $this->nextRow($result))) {
				
				printf("<option value='".$row[0]."'>".($row[1]==$cod_und?$row[2]:$row[1])."</option>");
			}
			$this->freeResult($result);
		}
		//echo $sql;
	}
	
	function initParametrosGenerales($sede = 0){
		
		$rs=$this->RSAsociativo("Exec dbo.spParametrosGenerales @op='InitAplicacion', @Sede = ".$sede);
		$arrayParam = NULL;
		foreach($rs as $r=>$row){
			$arrayParam[strtolower($row["parametroAplicacion"])]=$row["value"];
		}
		$_SESSION["arrayParametrosGenerales"]=$arrayParam;
	}
	
	function getParametroGeneral($parametro, $modulo = 'GENERAL'){
		$sql = "SELECT dbo.getParametroGeneral('$parametro', '$modulo') AS value";
		$param = $this->nextRow($this->query($sql));
		return $param['value'];
		// $arrayParam=$_SESSION["arrayParametrosGenerales"];
		// $key = strtolower($parametro);
		// return $arrayParam[$key];
		
	}
	function getIngreso($estudio) {
		$rs_ingreso = $this->query("Select ISNULL(dbo.fnGetIngresoPorEstudio($estudio), 0) AS ingreso")->nextRow();
        $_ingreso = $rs_ingreso["ingreso"];
	    if( $_ingreso>0 )
            return $_ingreso;
        else
            return -1;
    }
	
	function getAutoidPaciente($estudio){
		if(!empty($estudio)){
			$rs = $this->query("SELECT dbo.fnGetAutoidPaciente('{$estudio}') AS autoid")->nextRow();
		}
		return empty($rs["autoid"])? -1: $rs["autoid"];
	}

	function getEstadoIngreso($estudio) {
        $_ingreso = $this->getIngreso($estudio);
		$_estado  = $this->getDato("rtrim(ltrim(estado))", "ingresos", "id = $_ingreso");
        return $_estado;
    }
	
	// function LiberarUF($estudio,$motivo='') {
    //     $_sql = "UPDATE solicitud_tr_uf SET estado = 1,fecha_confi = NULL,
	// 	fecha_cancel = GETDATE(), usuario_cancel = '{$_SESSION['codigo_user']}',motivo='$motivo' 
	// 	WHERE ingreso = 
	// 	(SELECT TOP 1 id.id_ingreso FROM ingresos_deta id WHERE id.estudio = '".$estudio."')";
	// 	  $this->insertar($_sql);
	// }

	function LiberarUF($estudio,$motivo='') {
        $_sql = "UPDATE solicitud_tr_uf SET estado = 1,
		fecha_cancel = GETDATE(), usuario_cancel = '{$_SESSION['codigo_user']}',motivo='$motivo' 
		WHERE ingreso = 
		(SELECT TOP 1 id.id_ingreso FROM ingresos_deta id WHERE id.estudio = '".$estudio."') and usuario_confi is null";
		  $this->insertar($_sql);
    }
	
	function cancelarSolicitudTrUf($id, $motivo, $usuario){
		  $_sql = "UPDATE solicitud_tr_uf SET estado = 1,fecha_confi = NULL"
		  	.", fecha_cancel = GETDATE(), usuario_cancel = '".$usuario."',motivo='".$motivo."' "
			."WHERE id = ".(int)$id;
		  $this->insertar($_sql);
	}
	
	function verificarAutorizacionTraslado($id){
		$solicitud = $this->getDato("id","Autoriza","idSolicitudTraslado = ".(int)$id);
	
		if($solicitud === null){
			return false;
		}else{
			return true;
		}
	}

	function execSp($stmt, $show = NULL, $devRs = NULL){
		
		if($show == 1){
			
			echo "Exec {$stmt}";
			
		}/*else{*/
		$rs = $this->query("Exec ".$stmt);	
		if(is_object($rs)){
			
			if($devRs != NULL){
				return $rs;	
			}
			
			$r = $rs->nextRow();
			if(is_null($r[0])){
				return "";
			}else{
				return $r[0];
			}
		}
		//}
	}
	
	function getPlanActualPaciente($ingreso, $show = NULL){
		if(empty($ingreso)){
			return "-1";
		}
		if(!is_null($show)){
			echo "Exec spIngresoDetalle @op = 'EPP', @tipoId = '', @numId = '', @idIngreso = $ingreso";
		}
		// EPP = Estudio Plan Principal 
		$rs = $this->query("Exec spIngresoDetalle @op = 'EPP', @tipoId = '', @numId = '', @idIngreso = $ingreso");
		while($r = $rs->nextRow()){	
			return $r["estudio"];
		}		
		return NULL;
	}
	
	function getUfuncionalActual($estudio){
		return $this->getDato("ufuncional","sis_maes","con_estudio = '$estudio'");
	}
	
	function getTotalPlanesAbiertosPorIngreso($ingreso){
		return 1;
		/*if(empty($ingreso)){
			return -1;
		}
		$sql = "Exec spIngresoDetalle	@op = N'DIA', @idIngreso = $ingreso";
		$total = $this->query($sql)->numRows();
		
		return $total;*/
		
	}
	
		function getIdent_Current($tabla){
		if(empty($tabla)){
			return -1;
		}
		$sql = "SELECT IDENT_CURRENT('$tabla')";
		$total = $this->query($sql)->nextRow();
		
		return $total[0];
		
	}

	function PermiteRevertir($cedula){
		$dato = $this->getDato("n.revertir_f","usuario u, sis_nivelacceso n","u.nivel = n.codigo AND cedula = '$cedula'");
		
		return ($dato == 1)? true : false;
	}
	
	function CancelarUF($estudio,$motivo=''){
		$autoid=$this->getAutoidPaciente($estudio);
		$cama=$this->getCamaPaciente($autoid);
		$est_h=$this->gethistoriaClinica($estudio);
		
			if($est_h==0){ //historia cerrada
				if($cama==-1){//sin cama o paciente de alta
					$this->LiberarUF($estudio,$motivo);
				}
			}
	}
	
    function getCamaPaciente($autoid, $show=NULL) {
        $cama = $this->getDato("codigo", "sis_cama", "paciente = $autoid ORDER BY codigo DESC", $show);
        if( $cama )
            return $cama;
        else
            return -1;
    }

	function gethistoriaClinica($estudio,$show=NULL) {
		$ingreso=$this->getIngreso($estudio);
		$datos = $this->getDato('estado', "hcingres",
								 "con_estudio in( select estudio from ingresos_deta where id_ingreso = '$ingreso')", $show);
								 
		return $datos=='A' ? 1 : 0;
	}
	
	function HCDiligenciada($estudio){
		$tieneDatos = 0;
		
		if(!empty($estudio)){		
			$r = $this->query("SELECT dbo.fnGetIngresoPorEstudio($estudio)")->nextRow();
			$estudio_ing = $this->getDato("con_estudio", "hcingres", "ingreso = '{$r[0]}'");
			$tieneDatos = $this->getDato("COUNT(estudio)", "detalleshc", "estudio = $estudio_ing");
		}
		$tieneDatos = empty($tieneDatos)? 0 : $tieneDatos;
		return $tieneDatos === 0? false : true;
	}
	
	function getTextoBusquedaGeneral($texto){
		if(strlen($texto) == 0){
			return "%%";
		}
		//$texto = utf8_encode($texto);
		/*if(strpos("%", $texto) == 0){			
			$texto = str_replace("%", "%25", $texto);
			echo $texto;
		}*/
		$textoBusqueda = preg_replace("/[\s]+/", "%", $texto);
		//echo preg_replace("/s+/", "%", $texto);
		
		$textoAux = $textoBusqueda;
		$inicio = $textoAux[0];
		$fin = $textoAux[strlen($textoBusqueda)-1];
		
		if($fin !== "%"){
			$textoBusqueda .= "%";
		}
		//$textoBusqueda = str_replace("Ê", "%25ca", $textoBusqueda); // Escape para caracteres como %CA/%ca
		return $textoBusqueda;
	}

	public function ActivarRutadeAtencion($estudio, $codigo_hc, $posicion_hc, $valor_hc,$justificacion, $motivo_general, $manejaTransaccion = 1, $llamaSp = 'N'){
		$res = $this->RSAsociativo("EXEC spGestionarRutaAtencion @operacion='ActivarRuta', 
		@estudio=".$estudio.", @codigo_hc='$codigo_hc', @posicion_hc='$posicion_hc', 
		@valor_hc='$valor_hc', @usuario=".$_SESSION['id_user_sistema'].", @justificacion='$justificacion',
		@motivo_general='$motivo_general',	@manejaLaTransaccion=".$manejaTransaccion.", @llamadaSp = '".$llamaSp."'");

		// if(array_key_exists($res[0]["ERRORMSG"])){

		// }
	}

	function getCaptcha($tipo){
		$dts_conexion = parse_ini_file($_SESSION['root_vacuna'].'/conf/conf_zs.ini', true);
		return $dts_conexion['VACUNACION'][$tipo];
	}

	public function evaluarCaptcha($secretKey){
		$captcha_secret = $this->getCaptcha('captcha_secret');
		$respuesta = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$captcha_secret}&response={$secretKey}");
		$retorno = json_decode($respuesta);
		return $retorno;
	}

	function getFirmaEmail(){
		global $model;
		$respuesta = '';
		$imagen =$_SESSION["site_name_vacuna"]."/Archivos/Imagenes/footer_email.png";
		$remitente = $model->getParametroGeneral('mail_remitente','CORREO');
		if (file_exists($imagen) && $remitente != '' && $remitente != null) {
				$respuesta .= '<div>
				<table cellspacing="0">
					<tbody>
						<tr> 
							<td width=500>&nbsp;
								<img src="'.$imagen.'" alt="" width="398" height="114" data-tooltip="Descargar">
							</td>
						</tr>
					</tbody>
				</table>
			</div>';
		}else{
			$respuesta .= '';
		}
		return $respuesta;
	}
	
}// FIN DE LA CLASE MODEL


class SQLException extends Exception {
	public $problem;
	function __construct($problem) {
	$this->problem = $problem;
	print "Hay un Exception con el siguiente problema<br>$this->problem";
}// class 


}

/* Indispensable: no borre esta funci�n */
function clearSessionObject() {
	unset($_SESSION["ident"]);
	unset($_SESSION["tipo"]);
	unset($_SESSION['nombre']);
	unset($_SESSION['naci']);
	unset($_SESSION["sexo"]);
	unset($_SESSION['estudio']);
	unset($_SESSION['fecha_ing']);
	unset($_SESSION['autoriza']);
	unset($_SESSION['factura']);
	unset($_SESSION['empresa']);
	unset($_SESSION['admision']);
	unset($_SESSION['estrato']);
	unset($_SESSION['manual']);
	unset($_SESSION["estado"]);
	unset($_SESSION["copago_procs"]);
	unset($_SESSION["subsidio"]);
	unset($_SESSION["total_procs"]);
	unset($_SESSION["prods"]);
	unset($_SESSION["neto"]);
	unset($_SESSION["detalles"]);
	unset($_SESSION['historia']);
	unset($_SESSION["codigo_empresa"]);
	unset($_SESSION["autoid"]);
	unset($_SESSION["codigo_manual"]);
	unset($_SESSION['servicio']);
	unset($_SESSION["fuente"]);
	unset($_SESSION["filerips"]);
	unset($_SESSION["fecha"]);
	unset($_SESSION["fecha_servicio"]);
	unset($_SESSION["medico"]);
	unset($_SESSION["codigo_medico"]);
	unset($_SESSION["procs"]);
	unset($_SESSION["numero_servicio"]);
	unset($_SESSION['nombres']);
	unset($_SESSION['apellidos']);
	unset($_SESSION['hora_ing']);
	unset($_SESSION['tipo_factura']);
	unset($_SESSION['via_ingreso']);
	unset($_SESSION['causa_externa']);
	unset($_SESSION["codigo_estrato"]);
	unset($_SESSION["codigo_diag_ing"]);
	unset($_SESSION['contrato']);
	unset($_SESSION['tipo_hitoria']);
	unset($_SESSION["diag_ing"]);
	unset($_SESSION["observacion"]);
	unset($_SESSION["abonos"]);
	unset($_SESSION["procentaje"]);
	unset($_SESSION["pagos"]);
	unset($_SESSION["desc"]);
	unset($_SESSION["devs"]);
	unset($_SESSION["regimen"]);
	unset($_SESSION["cargo"]);
	unset($_SESSION["carnet"]);
	unset($_SESSION["paci"]);
	unset($_SESSION["pnombre"]);
	unset($_SESSION["snombre"]);
	unset($_SESSION["papellido"]);
	unset($_SESSION["sapellido"]);
	unset($_SESSION["total_recibos"]);
	//unset($_SESSION["total_ventas"]);
	unset($_SESSION["listaizq"]);
	unset($_SESSION["listader"]);
	unset($_SESSION["fecha_inicio"]);
	unset($_SESSION["fecha_fin"]);
	unset($_SESSION["ident_listaizq"]);
	unset($_SESSION["ident_listader"]);
	unset($_SESSION["total"]);
	unset($_SESSION["descuentos"]);
	unset($_SESSION["neto"]);
	unset($_SESSION["fecha_egreso"]);
	unset($_SESSION["listado"]);
	unset($_SESSION["lista"]);
	unset($_SESSION["lista2"]);
	unset($_SESSION['cantHor']);
	
	unset($_SESSION["evoluciones"]);
	unset($_SESSION["remision"]);
	unset($_SESSION["tabla"]);
	unset($_SESSION["cirugias"]);
	unset($_SESSION["codigo_paquete"]);
	unset($_SESSION['paquete']);
	unset($_SESSION["valor_paquete"]);
	unset($_SESSION["med"]);
	unset($_SESSION["total_med"]);
	unset($_SESSION["tipo_servicio"]);
	unset($_SESSION['evoluciondiaria']);
	unset($_SESSION['examen']);
	unset($_SESSION['datos']);
	unset($_SESSION['tipo_sangre']);
	unset($_SESSION["ccosto"]);
	unset($_SESSION['pabellon']);
	unset($_SESSION['cama']);
	unset($_SESSION['codigo_cama']);
	unset($_SESSION['servicio_cama']);
	unset($_SESSION['fecha_cama']);
	unset($_SESSION['medicamentos']);
	unset($_SESSION["dir"]);
	unset($_SESSION['obs']);
	unset($_SESSION['lista_elem']);
	unset($_SESSION['costo_paquete']);
}

function getNombreEstado($estado) {
	switch ($estado) {
		case "A":
		return "Abierta";
		case "C":
		return "Cerrada"; 
		case "N":
		return "Anulada";
		case "D":
		return "Devuelta";
	}
}

/* 
Calcula la Edad de Una Persona: 
A�O/MES/DIA
*/
function calcularEdad($fecha_naci) {
	global $model;
	$rs=$model->RSAsociativo("select dbo.fnEdadAproximada('".$fecha_naci."') as  Edad");
	return $rs[0]["Edad"];


	if (is_null($fecha_naci) || empty($fecha_naci)) {
		return;
	}
	
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$a = strtok ($fecha_naci,"-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	if ($anio < $a || ($anio == $a && $mes < $m) || ($anio == $a & $mes == $m & $dia < $d)) {
		return 0; //return -1; -- Cambiado por orden de alex verificar cuando pase algo
	}
	
	if ($anio == $a) {
		if ($mes == $m) {
			$edad = $dia - $d;
			return "$edad dia".($edad == 1 ? "" : "s");
		} else {
			$edad = $mes - $m;
			if ($edad == 1 && $dia < $d) {
				$edad = numeroDiasMes($a, $m) - $d + $dia;
				return "$edad dia".($edad == 1 ? "" : "s");
			} else if ($dia < $d) {
				$edad--;
			}
			return "$edad mes".($edad == 1 ? "" : "es");
		}
	} else {
		$edad = $anio - $a;
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))) {
			$edad = 12 - $m + $mes - ($mes < $m && $dia < $d ? 1 : 0);
			return "$edad mes".($edad == 1 ? "" : "es");
		} else if ($mes < $m || ($mes == $m && $dia < $d)) {
			$edad--;
		}
		return $edad;
	}
}

function calcularEdaddx($fecha_naci) {  
	if (is_null($fecha_naci) || empty($fecha_naci)) { return; }
	$anio = date("Y");      
	$mes = date("m");      
	$dia = date("d");               
	$a = strtok ($fecha_naci,"-/"); 
	$m = strtok("-/");   
	$d = strtok("-/");              
	if ($anio < $a || ($anio == $a && $mes < $m) || ($anio == $a & $mes == $m & $dia < $d)) {         return -1;     
	}               
	if ($anio == $a) {              
		if ($mes == $m) {                       
			$edad = $dia - $d;
			$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     
			return "2$edad";                
		} else {                        
			$edad = $mes - $m;                      
			if ($edad == 1 && $dia < $d) {                         
				$edad = numeroDiasMes($a, $m) - $d + $dia;
				$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     				
				return "2$edad";                        
			} else if ($dia < $d){                               
				$edad--;                        
				$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     
			}                       
		return "3$edad";                
		}       
	} else {                
		$edad = $anio - $a;             
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))){                       
			$edad = 12 - $m + $mes - ($mes < $m && $dia < $d ? 1 : 0);            
			$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     			         
			return "3$edad";                
		} else if ($mes < $m || ($mes == $m && $dia <$d)) {                  
			$edad--;                
			$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     
		}  
	$edad = strlen($edad)==1 ? '0'.$edad : $edad;                     
	return "4$edad";        
	}
}

function calcularEdad2($fecha_naci) {
	//return calcularEdad($fecha_naci);
	
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$a = strtok ($fecha_naci,"-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	if ($anio == $a) {
		if ($mes == $m) {
			$edad = $dia - $d;
		} else {
			$edad = $mes - $m;
			if ($edad == 1 && $dia < $d) {
				$edad = numeroDiasMes($a, $m) - $d + $dia;
			} else if ($dia < $d) {
				$edad--;
			}
		}
	} else {
		$edad = $anio - $a;
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))) {
			$edad = 12 - $m + $mes - ($mes < $m && $dia < $d ? 1 : 0);
		} else if ($mes < $m || ($mes == $m && $dia < $d)) {
			$edad--;
		}
	}
	
	return ($edad < 0 ? 0 : $edad);
}

function calcularUnidad($fecha_naci) {
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$a = strtok ($fecha_naci,"-/");
	$m = strtok("-/");
	$d = strtok("-/");	
	
	if ($anio == $a) {
		if ($mes == $m) {
			return 3;
		} else {
			$edad = $mes - $m;
			if ($edad == 1 && $dia < $d) {
				return 3;
			}
			return 2;
		}
	} else {
		$edad = $anio - $a;
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))) {
			return 2;
		}
		return 1;
	}
}

function calcularEdadTcpdf($fecha_naci) {
	if (is_null($fecha_naci) || empty($fecha_naci)) {
		return;
	}
	
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$a = strtok ($fecha_naci,"-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	if ($anio < $a || ($anio == $a && $mes < $m) || ($anio == $a & $mes == $m & $dia < $d)) {
		return -1;
	}
	
	if ($anio == $a) {
		if ($mes == $m) {
			$edad = $dia - $d;
			return "$edad Dia".($edad == 1 ? "" : "s");
		} else {
			$edad = $mes - $m;
			if ($edad == 1 && $dia < $d) {
				$edad = numeroDiasMes($a, $m) - $d + $dia;
				return "$edad Dia".($edad == 1 ? "" : "s");
			} else if ($dia < $d) {
				$edad--;
			}
			return "$edad Mes".($edad == 1 ? "" : "es");
		}
	} else {
		$edad = $anio - $a;
		if ($edad == 1 && ($mes < $m || ($mes == $m && $dia < $d))) {
			$edad = 12 - $m + $mes - ($mes < $m && $dia < $d ? 1 : 0);
			return "$edad Mes".($edad == 1 ? "" : "es");
		} else if ($mes < $m || ($mes == $m && $dia < $d)) {
			$edad--;
		}
		return "$edad Año".($edad == 1 ? "" : "s");
	}
}

function numeroDiasMes($anio, $mes) {
	switch ($mes) {
		case 2: return esBisiesto($anio) ? 29 : 28;
		case 1: case 3: case 5: case 7: case 8: case 10: case 12: return 31;
		case 4: case 6: case 9: case 11: return 30;
		default: return -1;
	}
}

function esBisiesto($anio) {
	return ($anio % 4 == 0 && $anio % 100 != 0) || $anio % 400 == 0;
}

function calcularEdadAnhios($fecha_naci) {
	if (is_null($fecha_naci) || empty($fecha_naci)) {
		return;
	}
	
	$anio = date("Y");
	$mes = date("m");
	$dia = date("d");
	
	$d = strtok ($fecha_naci, "-/");
	$m = strtok("-/");
	$a = strtok("-/");
	
	$edad = $anio - $a;
	if ($mes < $m || ($mes == $m && $dia < $d)) {
		$edad--;
	}
	
	return $edad;
}

/* Version 1.0 */
function mes_actual($mes) {
	switch ($mes) {
		case 1: return "ene";
		case 2: return "feb";
		case 3: return "mar";
		case 4: return "abr";
		case 5: return "may";
		case 6: return "jun";
		case 7: return "jul";
		case 8: return "ago";
		case 9: return "sep";
		case 10: return "oct";
		case 11: return "nov";
		case 12: return "dic";
		default: return NULL;
	}
}

function mes_actual_nombre($m) {
	switch ($m) {
		case "ene": return "Enero";
		case "feb": return "Febrero";
		case "mar": return "Marzo";
		case "abr": return "Abril";
		case "may": return "Mayo";
		case "jun": return "Junio";
		case "jul": return "Julio";
		case "ago": return "Agosto";
		case "sep": return "Septiembre";
		case "oct": return "Octubre";
		case "nov": return "Noviembre";
		case "dic": return "Diciembre";
		default: return NULL;
	}
}

function mes_actual_inv($mes) {
	switch ($mes) {
		case "ene": return 1;
		case "feb": return 2;
		case "mar": return 3;
		case "abr": return 4;
		case "may": return 5;
		case "jun": return 6;
		case "jul": return 7;
		case "ago": return 8;
		case "sep": return 9;
		case "oct": return 10;
		case "nov": return 11;
		case "dic": return 12;
		default: return NULL;
	}
}

function nombre_mes($fecha) {
	strtok ($fecha, "-/");
	$m = strtok("-/");
	switch ($m) {
		case 1: return "Enero";
		case 2: return "Febrero";
		case 3: return "Marzo";
		case 4: return "Abril";
		case 5: return "Mayo";
		case 6: return "Junio";
		case 7: return "Julio";
		case 8: return "Agosto";
		case 9: return "Septiembre";
		case 10: return "Octubre";
		case 11: return "Noviembre";
		case 12: return "Diciembre";
		default: return NULL;
	}
}

/* Version 2.0: Requiere la Fecha en Formato A/M/D - A-M-D */
function mes_actual_2($fecha) {
	$a = strtok ($fecha, "-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	return mes_actual($m);
}

/* Version 2.0: Requiere la Fecha en Formato D/M/A - D-M-A */
function anio_actual_2($fecha) {
	return strtok ($fecha, "-/");
}

/**
  * Funcion de conversion de fecha para SQL Server
  */

function convertirFechaHora($fecha) {
	//$fecha = substr($fecha, 0, 10);
	
	/*$fecha = strtok ($fecha, " ");
	$hora = strtok(" ");
	
	$d = strtok ($fecha, "-/");
	$m = strtok("-/");
	$a = strtok("-/");	
	
	$d = strlen($d) == 1 ? "0$d" : $d;
	$m = strlen($m) == 1 ? "0$m" : $m;
	
	return "$d/$m/$a - $hora";*/
	return $fecha;
}

function CONVERT_SQLSERVER($campo) {
	// before 103, now 111
	return "CONVERT(VARCHAR(10), $campo, 111)";
}

function convertirFecha($fecha) {
	$fecha = substr($fecha, 0, 10);
	
	$d = strtok ($fecha, "-/");
	$m = strtok("-/");
	$a = strtok("-/");	
	
	$a = substr($a, 0, 4);
	
	$d = strlen($d) == 1 ? "0$d" : $d;
	$m = strlen($m) == 1 ? "0$m" : $m;
	
	return "$d/$m/$a";
}

function convertirFechaDMY($fecha) {
	$fecha = substr($fecha, 0, 10);
	
	$a = strtok ($fecha, "-/");
	$m = strtok("-/");
	$d = strtok("-/");	
	
	$d = strlen($d) == 1 ? "0$d" : $d;
	$m = strlen($m) == 1 ? "0$m" : $m;
	
	return "$d/$m/$a";
}

function convertirFechaYMD($fecha) {
	$fecha = substr($fecha, 0, 10);
	
	$d = strtok ($fecha, "-/");
	$m = strtok("-/");
	$a = strtok("-/");
	
	$d = strlen($d) == 1 ? "0$d" : $d;
	$m = strlen($m) == 1 ? "0$m" : $m;
	
	return "$a/$m/$d";
}

function convertirFechaToLong($fecha, $dia = 0, $mes = 0, $anio = 0) {
	$fecha = substr($fecha, 0, 10);
	
	$a = strtok ($fecha,"-/");
	$m = strtok("-/");
	$d = strtok("-/");
	
	return mktime(0, 0, 0, $m + $mes, $d + $dia, $a + $anio);
}

// rellena el numero con 0 a la izquierda dependiendo del valor de $digs
function rellenar($numero, $digs=6,$caracter='0') {
	return str_pad($numero, $digs, $caracter, STR_PAD_LEFT);
}

/*-- ============================================================================== */
/*-- ============	Devuelve las letras de un numero                 ============== */
/*-- ============================================================================== */
$Z_z_unid = array(10);
$Z_z_dece = array(9);
$Z_z_dec1 = array(9);
$Z_z_cien = array(9);
$Z_z_unid[1] = 'UN ';
$Z_z_unid[2] = 'DOS ';
$Z_z_unid[3] = 'TRES ';
$Z_z_unid[4] = 'CUATRO ';
$Z_z_unid[5] = 'CINCO ';
$Z_z_unid[6] = 'SEIS ';
$Z_z_unid[7] = 'SIETE ';
$Z_z_unid[8] = 'OCHO ';
$Z_z_unid[9] = 'NUEVE ';
$Z_z_unid[10] = 'DIEZ ';
$Z_z_dece[1] = 'ONCE ';
$Z_z_dece[2] = 'DOCE ';
$Z_z_dece[3] = 'TRECE ';
$Z_z_dece[4] = 'CATORCE ';
$Z_z_dece[5] = 'QUINCE ';
$Z_z_dece[6] = 'DIECISEIS ';
$Z_z_dece[7] = 'DIECISIETE ';
$Z_z_dece[8] = 'DIECIOCHO ';
$Z_z_dece[9] = 'DIECINUEVE ';
$Z_z_dec1[2] = 'VEINTE ';
$Z_z_dec1[3] = 'TREINTA ';
$Z_z_dec1[4] = 'CUARENTA ';
$Z_z_dec1[5] = 'CINCUENTA ';
$Z_z_dec1[6] = 'SESENTA ';
$Z_z_dec1[7] = 'SETENTA ';
$Z_z_dec1[8] = 'OCHENTA ';
$Z_z_dec1[9] = 'NOVENTA ';
$Z_z_cien[1] = 'CIEN ';
$Z_z_cien[2] = 'DOSCIENTOS ';
$Z_z_cien[3] = 'TRECIENTOS ';
$Z_z_cien[4] = 'CUATROCIENTOS ';
$Z_z_cien[5] = 'QUINIENTOS ';
$Z_z_cien[6] = 'SEISCIENTOS ';
$Z_z_cien[7] = 'SETECIENTOS ';
$Z_z_cien[8] = 'OCHOCIENTOS ';
$Z_z_cien[9] = 'NOVECIENTOS ';

function preparar($valor) {
	$valor .= ".00";
	$n = strlen($valor);
	$rs = "";
	for ($i = 1; $i <= 15 - $n; $i++) {
		$rs .= "0";
	}
	$rs .= $valor;
	return $rs;
}

function EnLetras($valor) {
  global $Z_z_unid, $Z_z_dece, $Z_z_dec1, $Z_z_cien;
  $valor = preparar($valor);
  $Xmillon = $valor % 1000000;
  if ($valor > 0) {
	$Z_z_nume = $valor;
	$Z_z_nume = trim($Z_z_nume);
    $Z_z_mimi = substr($Z_z_nume, 0, 3);
    $Z_z_mill = substr($Z_z_nume, 3, 3);
    $Z_z_mile = substr($Z_z_nume, 6, 3);
    $Z_z_cent = substr($Z_z_nume, 9, 3);
    $Z_z_centv = substr($Z_z_nume, 13, 2);
    $Z_z_diga = "";
    $Z_z_diga = $Z_z_diga.($Z_z_mimi > 0 ? Fm_trac($Z_z_mimi).'MIL '.($Z_z_mill == 0 ? 'MILLONES ' : '') : '');
    if ($Z_z_mill > 0) {
      $Z_z_diga = $Z_z_diga.Fm_trac($Z_z_mill).($Z_z_mill > 1 ? 'MILLONES ' : 'MILLON ');
    }
    if ($Z_z_mile > 0) {
      $Z_z_diga = $Z_z_diga.Fm_trac($Z_z_mile).'MIL ';
    }
    if ($Z_z_cent > 0) {
      $Z_z_diga = $Z_z_diga.Fm_trac($Z_z_cent);
    }
    if ($Xmillon == 0) {
      $Z_z_diga = $Z_z_diga.($valor > 2 ? 'DE PESOS' : ($valor > 0.99 ? 'PESO' : ''));
    } else {
      $Z_z_diga = $Z_z_diga.($valor > 2 ? 'PESOS' : ($valor > 0.99 ? 'PESO' : ''));
    }
    $Z_z_diga = $Z_z_diga.($Z_z_centv > "00" ? ' CON '.$Z_z_centv.'/100' : '').' M/CTE';
    return $Z_z_diga;
	echo $Z_z_diga;
  }
  return;
}

function Fm_trac($valor) {
  global $Z_z_unid, $Z_z_dece, $Z_z_dec1, $Z_z_cien;
  settype($valor, "integer");
  $Z_j_letra = '';
  if ($valor > 99) {
    $Z_j_x = $valor / 100;
	settype($Z_j_x, "integer");
    $Z_j_letra = $Z_j_letra.$Z_z_cien[$Z_j_x];
    $valor = $valor - $Z_j_x * 100;
    if ($Z_j_x == 1 && $valor > 0) {
      $Z_j_letra = trim($Z_j_letra).'TO ';
    }
  }
  
if ($valor >= 1) {
      if ($valor <= 10) {
        $Z_j_letra = $Z_j_letra.$Z_z_unid[$valor];
      } else if ($valor <= 19) {
        $Z_j_letra = $Z_j_letra.$Z_z_dece[$valor - 10];
      } else if ($valor <= 29) {
        $Z_j_letra = $Z_j_letra."VEINT";
        $Z_j_x = $valor % 20;
        if ($Z_j_x > 0) {
          $Z_j_letra = $Z_j_letra.'I'.$Z_z_unid[$Z_j_x];
        } else {
          $Z_j_letra = $Z_j_letra.'E ';
        }
      } else if ($valor > 29) {
        $Z_j_x = $valor / 10;
		settype($Z_j_x, "integer");
        $Z_j_letra = $Z_j_letra.$Z_z_dec1[$Z_j_x];
        $Z_j_x = $valor - $Z_j_x * 10;
        if ($Z_j_x > 0) {
          $Z_j_letra = $Z_j_letra.'Y '.$Z_z_unid[$Z_j_x];
        }
	  }
}
  return $Z_j_letra;
}

function Arreglo(){
$arrBgColor = array();	
   if ($cssGrid == 0) {
	 $arrBgColor[0] = "#FFFFFF";
	 $arrBgColor[1] = "#FAFAFA";
	 $arrBgColor[2] = "#FEF7E7";
   } else if ($cssGrid == 1) {
	 $arrBgColor[0] = "#FFFFFF";
	 $arrBgColor[1] = "#000000";
	 $arrBgColor[2] = "#FEF7E7";
   } else if ($cssGrid == 2) {
	 $arrBgColor[0] = "#FFFFFF";
	 $arrBgColor[1] = "#DADCEF";
	 $arrBgColor[2] = "#003366"; // color de la cabecera de la tabla
   }

return $arrBgColor; 	
}

/*****************************************************************************
 *
 * Funci�n para convertir valores numericos en letras
 *
 *****************************************************************************/
function unidad($numuero) {
	switch ($numuero) {
		case 9:
			$numu = "NUEVE";
			break;
		case 8:
			$numu = "OCHO";
			break;
		case 7:
			$numu = "SIETE";
			break;
		case 6:
			$numu = "SEIS";
			break;
		case 5:
			$numu = "CINCO";
			break;
		case 4:
			$numu = "CUATRO";
			break;
		case 3:
			$numu = "TRES";
			break;
		case 2:
			$numu = "DOS";
			break;
		case 1:
			$numu = "UN";
			break;
		case 0:
			$numu = "";
			break;
	}
	
	return $numu;	
}

function decena($numdero) {
	if ($numdero >= 90 && $numdero <= 99) {
		$numd = "NOVENTA ";
		if ($numdero > 90) {
			$numd = $numd."Y ".(unidad($numdero - 90));
		}
	} else if ($numdero >= 80 && $numdero <= 89) {
		$numd = "OCHENTA ";
		if ($numdero > 80) {
			$numd = $numd."Y ".(unidad($numdero - 80));
		}
	} else if ($numdero >= 70 && $numdero <= 79) {
		$numd = "SETENTA ";
		if ($numdero > 70) {
			$numd = $numd."Y ".(unidad($numdero - 70));
		}
	} else if ($numdero >= 60 && $numdero <= 69) {
		$numd = "SESENTA ";
		if ($numdero > 60) {
			$numd = $numd."Y ".(unidad($numdero - 60));
		}
	} else if ($numdero >= 50 && $numdero <= 59) {
		$numd = "CINCUENTA ";
		if ($numdero > 50) {
			$numd = $numd."Y ".(unidad($numdero - 50));
		}
	} else if ($numdero >= 40 && $numdero <= 49) {
		$numd = "CUARENTA ";
		if ($numdero > 40) {
			$numd = $numd."Y ".(unidad($numdero - 40));
		}
	} else if ($numdero >= 30 && $numdero <= 39) {
		$numd = "TREINTA ";
		if ($numdero > 30) {
			$numd = $numd."Y ".(unidad($numdero - 30));
		}
	} else if ($numdero >= 20 && $numdero <= 29) {
		if ($numdero == 20) {
			$numd = "VEINTE ";
		} else {
			$numd = "VEINTI".(unidad($numdero - 20));
		}
	} else if ($numdero >= 10 && $numdero <= 19) {
		switch ($numdero) {
			case 10:
				$numd = "DIEZ ";
				break;
			case 11:
				$numd = "ONCE ";
				break;
			case 12:
				$numd = "DOCE ";
				break;
			case 13:
				$numd = "TRECE ";
				break;
			case 14:
				$numd = "CATORCE ";
				break;
			case 15:
				$numd = "QUINCE ";
				break;
			case 16:
				$numd = "DIECISEIS ";
				break;
			case 17:
				$numd = "DIECISIETE ";
				break;
			case 18:
				$numd = "DIECIOCHO ";
				break;
			case 19:
				$numd = "DIECINUEVE ";
				break;
		}	
	} else {
		$numd = unidad($numdero);
	}
	
	return $numd;
}

function centena($numc) {
	if ($numc >= 100) {
		if ($numc >= 900 && $numc <= 999) {
			$numce = "NOVECIENTOS ";
			if ($numc > 900) {
				$numce = $numce.(decena($numc - 900));
			}
		} else if ($numc >= 800 && $numc <= 899) {
			$numce = "OCHOCIENTOS ";
			if ($numc > 800) {
				$numce = $numce.(decena($numc - 800));
			}
		} else if ($numc >= 700 && $numc <= 799) {
			$numce = "SETECIENTOS ";
			if ($numc > 700) {
				$numce = $numce.(decena($numc - 700));
			}
		} else if ($numc >= 600 && $numc <= 699) {
			$numce = "SEISCIENTOS ";
			if ($numc > 600) {
				$numce = $numce.(decena($numc - 600));
			}
		} else if ($numc >= 500 && $numc <= 599) {
			$numce = "QUINIENTOS ";
			if ($numc > 500) {
				$numce = $numce.(decena($numc - 500));
			}
		} else if ($numc >= 400 && $numc <= 499) {
			$numce = "CUATROCIENTOS ";
			if ($numc > 400) {
				$numce = $numce.(decena($numc - 400));
			}
		} else if ($numc >= 300 && $numc <= 399) {
			$numce = "TRESCIENTOS ";
			if ($numc > 300) {
				$numce = $numce.(decena($numc - 300));
			}
		} else if ($numc >= 200 && $numc <= 299) {
			$numce = "DOSCIENTOS ";
			if ($numc > 200) {
				$numce = $numce.(decena($numc - 200));
			}
		} else if ($numc >= 100 && $numc <= 199) {
			if ($numc == 100) {
				$numce = "CIEN ";
			} else {
				$numce = "CIENTO ".(decena($numc - 100));
			}
		}
	} else {
		$numce = decena($numc);
	}
	
	return $numce;	
}

function miles($nummero) {
	if ($nummero >= 1000 && $nummero < 2000) {
		$numm = "MIL ".(centena($nummero % 1000));
	}
	if ($nummero >= 2000 && $nummero < 10000) {
		$numm = unidad(Floor($nummero / 1000))." MIL ".(centena($nummero % 1000));
	}
	if ($nummero < 1000) {
		$numm = centena($nummero);
	}
	
	return $numm;
}

function decmiles($numdmero){
	if ($numdmero == 10000) {
		$numde = "DIEZ MIL";
	}
	if ($numdmero > 10000 && $numdmero < 20000) {
		$numde = decena(Floor($numdmero / 1000))."MIL ".(centena($numdmero % 1000));
	}
	if ($numdmero >= 20000 && $numdmero < 100000) {
		$numde = decena(Floor($numdmero / 1000))." MIL ".(miles($numdmero % 1000));		
	}
	if ($numdmero < 10000) {
		$numde = miles($numdmero);
	}
	
	return $numde;
}		

function cienmiles($numcmero) {
	if ($numcmero == 100000) {
		$num_letracm = "CIEN MIL";
	}
	if ($numcmero >= 100000 && $numcmero < 1000000){
		$num_letracm = centena(Floor($numcmero / 1000))." MIL ".(centena($numcmero % 1000));		
	}
	if ($numcmero < 100000) {
		$num_letracm = decmiles($numcmero);
	}
	return $num_letracm;
}	

function millon($nummiero){
	if ($nummiero >= 1000000 && $nummiero < 2000000) {
		$num_letramm = "UN MILLON ".(cienmiles($nummiero % 1000000));
	}
	if ($nummiero >= 2000000 && $nummiero < 10000000) {
		$num_letramm = unidad(Floor($nummiero / 1000000))." MILLONES ".(cienmiles($nummiero % 1000000));
	}
	if ($nummiero < 1000000) {
		$num_letramm = cienmiles($nummiero);
	}
	
	return $num_letramm;
}	

function decmillon($numerodm) {
	if ($numerodm == 10000000) {
		$num_letradmm = "DIEZ MILLONES";
	}
	if ($numerodm > 10000000 && $numerodm < 20000000) {
		$num_letradmm = decena(Floor($numerodm / 1000000))."MILLONES ".(cienmiles($numerodm % 1000000));		
	}
	if ($numerodm >= 20000000 && $numerodm < 100000000){
		$num_letradmm = decena(Floor($numerodm / 1000000))." MILLONES ".(millon($numerodm % 1000000));		
	}
	if ($numerodm < 10000000) {
		$num_letradmm = millon($numerodm);
	}
	
	return $num_letradmm;
}

function cienmillon($numcmeros) {
	if ($numcmeros == 100000000) {
		$num_letracms = "CIEN MILLONES";
	}
	if ($numcmeros >= 100000000 && $numcmeros < 1000000000){
		$num_letracms = centena(Floor($numcmeros / 1000000))." MILLONES ".(millon($numcmeros % 1000000));		
	}
	if ($numcmeros < 100000000) {
		$num_letracms = decmillon($numcmeros);
	}
	
	return $num_letracms;
}	

function milmillon($nummierod) {
	if ($nummierod >= 1000000000 && $nummierod < 2000000000) {
		$num_letrammd = "MIL ".(cienmillon($nummierod % 1000000000));
	}
	if ($nummierod >= 2000000000 && $nummierod < 10000000000){
		$num_letrammd = unidad(Floor($nummierod / 1000000000))." MIL ".(cienmillon($nummierod % 1000000000));
	}
	if ($nummierod < 1000000000) {
		$num_letrammd = cienmillon($nummierod);
	}
	
	return $num_letrammd;
}	
			
		
function convertirNumero2Letra($numero) {
	$m = new Model();
	/*$numero = round($numero, 0);
	$parte1 = strtok($numero, ".");
	$parte2 = strtok(".");
	$numf = milmillon($parte1);
	$numj = centena($parte2);*/
	$rs = $m->query("Select dbo.convertirNumero2Letra($numero)as valorLetra")->nextRow();
	//return $numf." PESOS ".(empty($parte2) ? "" : "CON $numj CENTAVOS ")."MCTE";
	return $rs["valorLetra"];
}


function convertirNumero2LetraDias($numero) {
	$num = centena($numero);
	return $num." D&Iacute;A".($numero > 1 ? "S" : "");
}



/*****************************************************************************
 *
 * Fin de la funci�n para convertir valores numericos en letras
 *
 *****************************************************************************/
 /*
	clase para encriptar y desencriptar
 */
 class encrypt_vb{

	var $UserKeyAscii;
	var $TextAscii;
	var $UserKey;


  function encrypt_vb()
  {$this->UserKeyAscii = array();
    $this->TextAscii    = array();
    $this->UserKey      = 'Zeussalud';
   }
 
   //------------------------------------------------------------------------------------
   // Encripta una cadena de texto.
   //------------------------------------------------------------------------------------
  // Parámetros
   //------------------------------------------------------------------------------------
   // $String1: Cadena a encriptar.
   //------------------------------------------------------------------------------------
   function Encrypt($string) {
   $result = '';
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($this->UserKey, ($i % strlen($this->UserKey))-1, 1);
      $char = chr(ord($char)+ord($keychar));
      $result.=$char;
   }
   return base64_encode($result);
}
 
   //------------------------------------------------------------------------------------
   // Desencripta una cadena de texto.
   //------------------------------------------------------------------------------------
   // Parámetros
   //------------------------------------------------------------------------------------
   // $String1: Cadena a desencriptar.
   //------------------------------------------------------------------------------------
 function Decrypt($string) {
   $result = '';
   $string = base64_decode($string);
   for($i=0; $i<strlen($string); $i++) {
      $char = substr($string, $i, 1);
      $keychar = substr($this->UserKey, ($i % strlen($this->UserKey))-1, 1);
      $char = chr(ord($char)-ord($keychar));
      $result.=$char;
   }
   return $result;
}

}
?>