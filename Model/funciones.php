<?php
include_once("Model.php");

function control_combo($model, $col_valor, $col_display, $tabla, $where, $selected){
	
	$where = $where != NULL ? "WHERE $where" : '' ;
	$sql = "SELECT $col_valor, $col_display FROM $tabla $where";
	$datos = $model->Execute($sql);
	
	for($i=0; $i<sizeof($datos); $i++){
		$select = $datos[$i][$col_valor] == $selected ? 'selected="selected"' : '' ;
		$salida .= " <option value=".$datos[$i][$col_valor]." $select>".$datos[$i][$col_display]."</option>";	
	}
	return $salida;
}

function dameCodigoDia($fecha){
	return date("w", $fecha);
}

function diaSemana($dia){
	switch($dia){
		case 0: $dia = "Domingo"; break;
		case 1: $dia = "Lunes"; break;
		case 2: $dia = "Martes"; break;
		case 3: $dia = "Miercoles"; break;
		case 4: $dia = "Jueves"; break;
		case 5: $dia = "Viernes"; break;
		case 6: $dia = "Sabado"; break;
	}
	return $dia;
}
function obtenerDetallesMed($estudio) {
	global $model;
	
	$rs = $model->select("m.fecha_ing, m.hora_ing, m.nro_autoriza, m.nro_factura, m.estado, e.codigo, 
		e.nombre, es.codigo, es.nombre, c.codigo, c.nombre, m.obs, ma.codigo, 
		ma.nombre, ma.tipo, m.vlr_coopago, m.tipo_estudio, c.tipo_contrato, m.diagno_ing, m.fecha_egr, 
		m.hora_egr, c.manual_med, (SELECT nombre FROM sis_manual WHERE codigo=c.manual_med)",
		"sis_empre AS e, contratos AS c, sis_maes AS m, sis_estrato AS es,
		sis_manual AS ma", 
		"m.con_estudio = $estudio AND m.contrato = c.codigo AND c.empresa = e.codigo 
		AND m.cod_clasi = es.codigo AND c.manual = ma.codigo", NULL, 1);
	
	$fecha_ing = convertirFecha($rs[0]);
	$hora_ing = $rs[1];
	$autorizacion = $rs[2];
	$factura = $rs[3];
	$estado = getNombreEstado($rs[4]);
	$codigo_empresa = $rs[5];
	$empresa = $rs[6];
	$codigo_estrato = $rs[7];
	$estrato = $rs[8];
	$codigo_contrato = $rs[9];
	$contrato = $rs[10];
	//$observacion = $rs[11];
	$codigo_manual = $rs[12];
	$manual = $rs[13];
	$tipo_manual = $rs[14];
	$vlr_copago = $rs[15];
	$tipo_estudio = $model->getNombreTipoEstudio($rs[16]);
	$tipo_contrato = $rs[17];
	$codigo_diagnostico_ingreso = $rs[18];
	$fecha_egr = $rs[19];
	$hora_salida = $rs[20];
	$codigo_manual_med = $rs[21];
	$manual_med = $rs[22];
	
	$tipoContrato = $model->getDato("descripcion", "FacturasTiposContratos", "estudio = $estudio");
	
	//$observacion = str_replace("\r\n", " - ", $observacion);
	
	$t = $model->select("ht.detalle", "hcingres AS h, hctiphis AS ht", "h.con_estudio = $estudio AND h.tipo_histo = ht.codigo", NULL, 1);
	$tipo_historia = $t[0];
	
	$total = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio");
	$total_cobra_copago = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");
	$resul = $model->select("subsidio, maximo", "sis_extraxemp", "estrato = '$codigo_estrato' AND empresa = '$codigo_empresa'", NULL, 1);
	$sub = $resul[0];
	$max = $resul[1];
	
	$total = empty($total) ? 0 : $total;
	if ($sub == -1) {
		$copago = $vlr_copago;
	}elseif($sub == 0){
		$copago = $total_cobra_copago;
	}else 	if ($sub <= 100) {
		$copago = round($total_cobra_copago * (100 - $sub) / 100, 0);
		$subsidio = $sub;
	} else {
		$copago = round(($total_cobra_copago == 0) ? 0 : $sub,0);
		$subsidio = $sub;
	}
	
	if ($max != 0 && $copago > $max) {
		$copago = round($max,0);
	}
	
	$porcentaje = empty($subsidio) ? "ESPECIAL" : ($subsidio <= 100 ? (100 - $subsidio) : $subsidio);
	
	$neto = round($model->calcularNetoFactura($estudio), 0);
	
	$datos = 
	"&estado=$estado&factura=$factura&estudio=$estudio&fecha_ing=$fecha_ing&hora_ing=$hora_ing".
		"&autorizacion=$autorizacion&contrato=$contrato&codigo_contrato=$codigo_contrato&empresa=$empresa".
		"&codigo_empresa=$codigo_empresa&estrato=$estrato&codigo_estrato=$codigo_estrato&medico=$medico".
		"&codigo_medico=$codigo_medico&tipo_historia=$tipo_historia&observacion=$observacion&manual=$manual".
		"&codigo_manual=$codigo_manual&tipo_manual=$tipo_manual&porcentaje=$porcentaje&total=$total".
		"&copago=$copago&subsidio=$subsidio&neto=$neto&tipo_estudio=$tipo_estudio&tipo_contrato=$tipo_contrato".
		"&codigo_diagnostico_ingreso=$codigo_diagnostico_ingreso&fecha_egr=$fecha_egr&hora_salida=$hora_salida".
		"&tipoContrato=$tipoContrato&codigo_manual_med=$codigo_manual_med&manual_med=$manual_med";
	
	return $datos;
}

function obtenerDetalles($estudio) {
	global $model;
	
	$rs = $model->select("m.fecha_ing, m.hora_ing, m.nro_autoriza, m.nro_factura, m.estado, e.codigo, 
		e.nombre, es.codigo, es.nombre, c.codigo, c.nombre, me.codigo, me.nombre, m.obs, ma.codigo, 
		ma.nombre, ma.tipo, m.vlr_coopago, m.tipo_estudio, c.tipo_contrato, m.diagno_ing, m.fecha_egr, 
		m.hora_egr, c.manual_med, (SELECT nombre FROM sis_manual WHERE codigo=c.manual_med)",
		"sis_empre AS e, contratos AS c, sis_maes AS m, sis_estrato AS es, sis_medi AS me,
		sis_manual AS ma", 
		"m.con_estudio = $estudio AND m.contrato = c.codigo AND c.empresa = e.codigo 
		AND m.cod_medico = me.codigo AND m.cod_clasi = es.codigo AND c.manual = ma.codigo", NULL, 1);
	
	$fecha_ing = urlencode(convertirFecha($rs[0]));
	$hora_ing = urlencode($rs[1]);
	$autorizacion = urlencode($rs[2]);
	$factura = urlencode($rs[3]);
	$estado = urlencode(getNombreEstado($rs[4]));
	$codigo_empresa = urlencode($rs[5]);
	$empresa = urlencode($rs[6]);
	$codigo_estrato = urlencode($rs[7]);
	$estrato = urlencode($rs[8]);
	$codigo_contrato = urlencode($rs[9]);
	$contrato = urlencode($rs[10]);
	$codigo_medico = urlencode($rs[11]);
	$medico = urlencode($rs[12]);
	//$observacion = $rs[13];
	$codigo_manual = urlencode($rs[14]);
	$manual = urlencode($rs[15]);
	$tipo_manual = urlencode($rs[16]);
	$vlr_copago = urlencode($rs[17]);
	$tipo_estudio = urlencode($model->getNombreTipoEstudio($rs[18]));
	$tipo_contrato = urlencode($rs[19]);
	$codigo_diagnostico_ingreso = urlencode($rs[20]);
	$fecha_egr = urlencode($rs[21]);
	$hora_salida = urlencode($rs[22]);
	$codigo_manual_med = urlencode($rs[23]);
	$manual_med = urlencode($rs[24]);

	
	$tipoContrato = urlencode($model->getDato("descripcion", "FacturasTiposContratos", "estudio = $estudio"));
	$codigoContrato = (int)$model->getContrato($estudio);
	$tipo_con=$model->getDato("tipo_contrato","contratos","codigo='".$codigoContrato."'");	
	
	$t = $model->select("ht.detalle", "hcingres AS h, hctiphis AS ht", "h.con_estudio = $estudio AND h.tipo_histo = ht.codigo", NULL, 1);
	$tipo_historia = urlencode($t[0]);	

	$total = $model->getDato("SUM(total)","sis_deta","estudio='".$estudio."' and ItemRevertido=0");

	$total_cobra_copago = $model->getDato("SUM(total)","sis_deta","estudio='".$estudio."' and ItemRevertido=0 And cobra_copago = 1");
	$manejaTablaLQ_Pagos = $model->getDato('ManejaTablaLQ_Pagos', 'contratos', "codigo = {$codigoContrato}");
	if ($manejaTablaLQ_Pagos == 1) {		
		$rs = $model->RSAsociativo("Exec spTablaLQ_PagosPcte @Op = 'S_DatosLiquidacionNivelxEstudio', @Estudio = '{$estudio}'");
		if (sizeof($rs) > 0) {
			$r = $rs[0];
			$subsidio = (float)$r['ValorPagar'];
			$max = (float)$r['ValorMaxEvento'];
		} else {
			$subsidio = 0;
			$max = 0;
		}
		
	} else {
		$resul = $model->select("e.subsidio, e.maximo, es.Tipo", "sis_estrato AS es INNER JOIN
	                      sis_maes AS m ON es.codigo = m.cod_clasi INNER JOIN
	                      sis_extraxemp AS e ON es.codigo = e.estrato AND m.cod_entidad = e.empresa", "m.con_estudio = '".$estudio."'",NULL, 1);
		$subsidio = $resul[0];
		$max = $resul[1];
	}

	/*$subsi=$model->getDato("e.subsidio", "sis_estrato AS es INNER JOIN
                      sis_maes AS m ON es.codigo = m.cod_clasi INNER JOIN
                      sis_extraxemp AS e ON es.codigo = e.estrato AND m.cod_entidad = e.empresa", "m.con_estudio = '".$estudio."'");*/
	
	$rs = $model->query("[dbo].[colocarTotalesFacturanew] @estudio ='$estudio', @calcular ='S'");
	if(is_object($rs)){
		$r = $rs->nextRow();
		$neto = $r['neto'];
		$total = $r['total_facturado'];
	}else{
		$neto = 0;
		$total = 0;
	}

	$copago = $r['copago'];
	
	if(trim($subsi)=='-1'){
		$copago=$vlr_copago;
	}	
	
	$total = empty($total) ? 0 : $total;

	if($subsidio==-1){
		$porcentaje=0;
	}else{
		$porcentaje = empty($subsidio) ? "ESPECIAL" : ($subsidio <= 100  ? (100 - $subsidio) : $subsidio);
	}
	
	//echo ".:  ".$porcentaje." .: ".$subsidio;
	
	$datos = "&estado=$estado&factura=$factura&estudio=$estudio&fecha_ing=$fecha_ing&hora_ing=$hora_ing".
		"&autorizacion=$autorizacion&contrato=$contrato&codigo_contrato=$codigo_contrato&empresa=$empresa&manual=$manual&codigo_manual_med=$codigo_manual_med&manual_med=$manual_med".
		"&codigo_empresa=$codigo_empresa&estrato=$estrato&codigo_estrato=$codigo_estrato&medico=$medico".
		"&codigo_medico=$codigo_medico&tipo_historia=$tipo_historia&observacion=$observacion".
		"&codigo_manual=$codigo_manual&tipo_manual=$tipo_manual&porcentaje=$porcentaje&total=$total".
		"&copago=$copago&subsidio=$subsidio&neto=$neto&tipo_estudio=$tipo_estudio&tipo_contrato=$tipo_contrato".
		"&codigo_diagnostico_ingreso=$codigo_diagnostico_ingreso&fecha_egr=$fecha_egr&hora_salida=$hora_salida".
		"&tipoContrato=$tipoContrato";
	
	return $datos;
}

function obtenerCopago($estudio) {
	global $model;
	
	$rs = $model->select("m.fecha_ing, m.hora_ing, m.nro_autoriza, m.nro_factura, m.estado, e.codigo, 
		e.nombre, es.codigo, es.nombre, c.codigo, c.nombre, me.codigo, me.nombre, m.obs, ma.codigo, 
		ma.nombre, ma.tipo, m.vlr_coopago, m.tipo_estudio, c.tipo_contrato, m.diagno_ing, m.fecha_egr, 
		m.hora_egr, c.manual_med, (SELECT nombre FROM sis_manual WHERE codigo=c.manual_med)",
		"sis_empre AS e, contratos AS c, sis_maes AS m, sis_estrato AS es, sis_medi AS me,
		sis_manual AS ma", 
		"m.con_estudio = $estudio AND m.contrato = c.codigo AND c.empresa = e.codigo 
		AND m.cod_medico = me.codigo AND m.cod_clasi = es.codigo AND c.manual = ma.codigo", NULL, 1);
	
	$fecha_ing = convertirFecha($rs[0]);
	$hora_ing = $rs[1];
	$autorizacion = $rs[2];
	$factura = $rs[3];
	$estado = getNombreEstado($rs[4]);
	$codigo_empresa = $rs[5];
	$empresa = $rs[6];
	$codigo_estrato = $rs[7];
	$estrato = $rs[8];
	$codigo_contrato = $rs[9];
	$contrato = $rs[10];
	$codigo_medico = $rs[11];
	$medico = $rs[12];
	//$observacion = $rs[13];
	$codigo_manual = $rs[14];
	$manual = $rs[15];
	$tipo_manual = $rs[16];
	$vlr_copago = $rs[17];
	$tipo_estudio = $model->getNombreTipoEstudio($rs[18]);
	$tipo_contrato = $rs[19];
	$codigo_diagnostico_ingreso = $rs[20];
	$fecha_egr = $rs[21];
	$hora_salida = $rs[22];
	$codigo_manual_med = $rs[23];
	$manual_med = $rs[24];

	
	$tipoContrato = $model->getDato("descripcion", "FacturasTiposContratos", "estudio = $estudio");
	
	//$observacion = str_replace("\r\n", " - ", $observacion);
	
	$t = $model->select("ht.detalle", "hcingres AS h, hctiphis AS ht", "h.con_estudio = $estudio AND h.tipo_histo = ht.codigo", NULL, 1);
	$tipo_historia = $t[0];
	
	$total = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio");
	$total_cobra_copago = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");
	$resul = $model->select("subsidio, maximo", "sis_extraxemp", "estrato = '$codigo_estrato' AND empresa = '$codigo_empresa'", NULL, 1);
	$sub = $resul[0];
	$max = $resul[1];
	
	$total = empty($total) ? 0 : $total;
	if ($sub == -1) {
		$copago = $vlr_copago;
	} elseif($sub == 0){ //Cobro Pleno de lo facturado
		$copago = $total_cobra_copago;
	}else 	if ($sub <= 100) {
		$copago = round($total_cobra_copago * (100 - $sub) / 100, 0);
		$subsidio = $sub;
	} else {
		$copago = round(($total_cobra_copago == 0) ? 0 : $sub,0);
		$subsidio = $sub;
	}
	
	if ($max != 0 && $copago > $max) {
		$copago = $max;
	}
	
	$porcentaje = empty($subsidio) ? "ESPECIAL" : ($subsidio <= 100 ? (100 - $subsidio) : $subsidio);
	
	$neto = $model->calcularNetoFactura($estudio);
	
	$datos = "&estado=$estado&factura=$factura&estudio=$estudio&fecha_ing=$fecha_ing&hora_ing=$hora_ing".
		"&autorizacion=$autorizacion&contrato=$contrato&codigo_contrato=$codigo_contrato&empresa=$empresa".
		"&codigo_empresa=$codigo_empresa&estrato=$estrato&codigo_estrato=$codigo_estrato&medico=$medico".
		"&codigo_medico=$codigo_medico&tipo_historia=$tipo_historia&observacion=$observacion&manual=$manual".
		"&codigo_manual=$codigo_manual&tipo_manual=$tipo_manual&porcentaje=$porcentaje&total=$total".
		"&copago=$copago&subsidio=$subsidio&neto=$neto&tipo_estudio=$tipo_estudio&tipo_contrato=$tipo_contrato".
		"&codigo_diagnostico_ingreso=$codigo_diagnostico_ingreso&fecha_egr=$fecha_egr&hora_salida=$hora_salida".
		"&tipoContrato=$tipoContrato&codigo_manual_med=$codigo_manual_med&manual_med=$manual_med";
	
	return $copago;
}

function obtenerDetallesH($estudio) {
	global $model;
	
	$autoid = $model->getDato("autoid", "sis_maes", "con_estudio = $estudio");
	
	// Actualiza constantemente el valor y la cantidad de d�as para la �ltima cama
	$camas = $model->limitSelect("d.id, c.codigo", "sis_deta AS d, sis_cama AS c", 
		"d.cama = c.codigo AND d.estudio = $estudio AND d.cama <> -1 AND c.paciente = $autoid
		ORDER BY d.num_servicio DESC", NULL, 1, 1);
	if ($camas) {
		//Calcula el numero de dias
		$canti = $model->getDato("DATEDIFF(d, fecha_servicio, GETDATE())", "sis_deta", "id = $camas[0]");
		$canti = $canti == 0 ? 1 : $canti;
		/*$sql = "UPDATE sis_deta SET
			fecha_fin = GETDATE(),
			cantidad = $canti,
			total = $canti * vlr_servicio
			FROM sis_deta AS d, sis_maes AS m
			WHERE d.id = $camas[0] AND d.estudio = m.con_estudio AND m.estado = 'A'";*/
		$sql = "UPDATE sis_deta SET
			cantidad = $canti,
			total = $canti * vlr_servicio
			FROM sis_deta AS d, sis_maes AS m
			WHERE d.id = $camas[0] AND d.estudio = m.con_estudio AND m.estado = 'A'";
		$model->insertar($sql);
	}
	
	$r = $model->select("medico_egr, diagno_egr, diagno_egr1, diagno_egr2, diagno_egr3, destino_usu, 
		motivo_egr, estado_egr, causa_mte", "sis_maes AS m", "con_estudio = $estudio", NULL, 1);
	
	$datos = "&codigo_medico_alta=".urlencode($r[0])."&codigo_diag_salida=".urlencode($r[1])."&codigo_diag_salida1=".urlencode($r[2]).
		"&codigo_diag_salida2=".urlencode($r[3])."&codigo_diag_salida3=".urlencode($r[4])."&destino_usu=".urlencode($r[5])."&motivo_egr=".urlencode($r[6]).
		"&estado_egr=".urlencode($r[7])."&codigo_causa_muerte=".urlencode($r[8]);
	
	$r = $model->limitSelect("cod_diap, cod_diagn1, cod_diagn2, cod_diagn3", "sis_deta", 
		"estudio = $estudio AND (codigo_paquete IS NULL OR LTRIM(codigo_paquete) = '')
		AND (codigo_cirugia IS NULL OR LTRIM(codigo_cirugia) = '')", NULL, 1, 1);
	
	$datos .= "&codigo_diagnostico_principal=".urlencode($r[0])."&codigo_diagnostico_relacionado1=".urlencode($r[1])."&codigo_diagnostico_relacionado2=".urlencode($r[2])."&codigo_diagnostico_relacionado3=".urlencode($r[3]);
	
	return obtenerDetalles($estudio).$datos;
}

function obtenerPagos($estudio, &$lista = NULL ) {
	global $model;
	
	$rePagos = $model->select("*", "pagos", "con_estudio = $estudio and activo = 1 order by recibo, fecha");
	$lista = array();
	$pagos = 0;
	$devs  = 0;
	$desc  = 0;
	
	if ($model->numRows($rePagos) >= 1) {
		$pos = 0;
		while (($rowPagos = $model->nextRow($rePagos))) {
			if ($rowPagos['recibo'] != 0) {
				
				switch ($rowPagos["tipo"]) {
					case 1:
						//switch ($rowPagos['codigo_tipopago']) {
							/*case 10:
								$pagos += 0;
								break;*/
							//default:
								$pagos += $rowPagos['valor'];
								//break;
					//	}
					break;
					case 5:
					$pagos += $rowPagos['valor'];							
					break;
					case 2:
					$devs += $rowPagos['valor'];
					break;
					case 3:
					$desc += $rowPagos['valor'];
					break;
					default:
					break;
				}
				
			}
			
			if (!is_null($lista)) {
				$res = $model->nextRow($model->select("nombre", "sis_tipo", "fuente='{$rowPagos['fuente']}'"));
				$res2 = $model->nextRow($model->select("descripcion", "sis_tipopago", "id={$rowPagos['codigo_tipopago']}"));
				
				$lista[$pos]["id"] = $rowPagos['id'];
				$lista[$pos]["recibo"] = $rowPagos['recibo'];
				$lista[$pos]["fecha"] = convertirFecha($rowPagos['fecha']);
				$lista[$pos]["descripcion"] = $rowPagos['descripcion'];
				$lista[$pos]["valor"] = $rowPagos['valor'];
				$lista[$pos]["cajero"] = $rowPagos['nom_usuario'];
				$lista[$pos]["codigo_servicio"] = $rowPagos['fuente'];
				$lista[$pos]["servicio"] = $res['nombre'];
				$lista[$pos]["tipo"] = $res2['descripcion'];
				$lista[$pos]["tipo_pago"] = $rowPagos['tipo'];
				$lista[$pos]["cajero"] = $rowPagos['nom_usuario'];
				$lista[$pos]["codtpago"] = $rowPagos['codigo_tipopago'];
				$lista[$pos]["NumeroFactura"] = $rowPagos['NumeroFactura'];
				$lista[$pos]["IdAnticipo"] = $rowPagos['IdAnticipo'];
				$lista[$pos]["Contabilizable"] = $rowPagos['Contabilizable'];
				$observacion = $rowPagos['obs'];
				$pos++;
			}
		}
	}
	
	$pagos=number_format($pagos,2,'.','');
	$observacion = urlencode(str_replace("\r\n", " ", $observacion));	
	return "&pagos=$pagos&devs=$devs&desc=$desc&observacion=$observacion";
}

/*function revertirAdmision($codigo_estudio, $nro_factura = 0) {
	global $model;
	
	$existe = $model->getDato("COUNT(*)", "relgrup, sis_maes", "con_estudio = $codigo_estudio AND nro_factura = factura");

	if ($existe == 0) {
		if ($model->getDato("COUNT(*)", "sis_maes", "con_estudio = $codigo_estudio AND contabilizado = 1") != 0) {
			return -1;
		}
		
		$sql = "UPDATE sis_maes
			SET estado = 'A',
			vlr_factura = 0,
			vlr_coopago = 0,
			vlr_descto = 0,
			vlr_neto = 0
			WHERE con_estudio = $codigo_estudio AND status_regis = 0";
		
		if ($model->insertar($sql) == 0) {
			return -2;
		}
		
		if ($nro_factura != 0) {
			$sql = "DELETE FROM movdia WHERE documento = $nro_factura";
			$model->insertar($sql);
		}
		
		return 1;
	} else {
		return -2;
	}
}*/

function guardarObservacion($model, $codigo_estudio, $observacion) {
	$sql = "UPDATE sis_maes
		SET	obs = '$observacion'
		WHERE con_estudio = $codigo_estudio";
	$model->insertar($sql);
}

function obtenerPaciente($estudio) {
	global $model;
	$r = $model->datosPaciente(NULL, NULL, 
		"tipo_id, num_id, primer_nom, segundo_nom, primer_ape, segundo_ape, fecha_naci, sexo, tipo_usuario, m.autoid,tipo_afilia,tipo_sangre",
		$estudio);
	
	if ($r) {
		$tipo = urlencode($r[0]);
		$ident = urlencode($r[1]);
		$nombre = urlencode("$r[2] $r[3] $r[4] $r[5]");
		$fecha_naci = urlencode(convertirFecha($r[6]));
		$sexo = urlencode($r[7]);
		$tipo_sangre = urlencode($r[11]);
		$regimen = urlencode($model->getDato("nombre", "sismaelm", "tabla = 'GRL' AND tipo = 'RGMN' AND valor = {$r[8]}"));
		$autoid = urlencode($r[9]);
		$tipoUsuario = urlencode($model->getDato("nombre", "sismaelm", "tabla = 'GRL' AND tipo = 'AFLDO' AND valor = {$r[10]}"));

		$datos = "&ident=$ident&tipo=$tipo&nombre=$nombre&fecha_naci=$fecha_naci&sexo=$sexo&tipo_sangre=$tipo_sangre&regimen=$regimen&autoid=$autoid&tipoUsuario=$tipoUsuario";
			
		return $datos;
	} else {
		return false;
	}
}

function obtenerServicio($fuente, $serv = NULL, $ufFacturar = NULL) {
	global $model;	
	global $ufFacturar;
	//echo $ufFacturar ."UF";
	if (!is_null($fuente)) {
		$r = $model->select("nombre, filerips, ccosto", "sis_tipo", "fuente = $fuente", NULL, 1);
		$servicio = urlencode($r[0]);
		$filerips = urlencode($r[1]);
		$ccosto = urlencode($model->getDato("ufuncional","sis_maes", 
			"con_estudio = '{$_REQUEST['estudio']}'"));		
		$ccosto = empty($ufFacturar)? urlencode($ccosto) : urlencode($ufFacturar);	
		
		return "&fuente=".$fuente."&servicio=".$servicio."&filerips=".$filerips."&ccosto=".$ccosto;//&numero_servicio=$numero_servicio";
	} else {
		$rs = $model->limitQuery("
			SELECT t.nombre, t.filerips, d.ufuncional, t.fuente
			FROM sis_deta AS d, sis_tipo AS t 
			WHERE num_servicio = $serv AND d.fuente_tips = t.fuente", 1);
		$r = $model->nextRow($rs);
		$model->freeResult($rs);
		$servicio = urlencode($r[0]);
		$filerips = urlencode($r[1]);
		$ccosto = empty($ufFacturar)? urlencode($r[2]) : urlencode($ufFacturar);		
		$fuente = urlencode($r[3]);
		
		return "&fuente=$fuente&servicio=$servicio&filerips=$filerips&ccosto=$ccosto&num_servicio=$serv";
	}
}

function obtenerProcs($serv, &$next = NULL) {
	global $model;
	
	$rs = $model->procedimientos(
		"num_servicio = $serv", 
		"id, cod_servicio, descripcion, cantidad, vlr_servicio, total, codigo_paquete, codigo_cirugia,cod_usuario");
	if ($rs) {
		$i = 0;
		$lista = array();
		$sw = true;
		
		while (($row = $model->nextRow($rs))) {
			$lista[$i]["id"] = $row[0];
			$lista[$i]["codigo"] = $row[1];
			$lista[$i]["nombre"] = $row[2];
			$lista[$i]["cantidad"] = $row[3];
			$lista[$i]["valor"] = $row[4];
			$lista[$i]["total"] = $row[5];
			$lista[$i]["codigo_paquete"] = $row[6];
			$lista[$i]["codigo_cirugia"] = $row[7];
			$lista[$i]["cod_usuario"] = $row["cod_usuario"];
			$i++;
			if ($sw == true) {
				if (!empty($row[6]) || !empty($row[7])) {
					$next = 1;
					$sw = false;
				}
			}
		}
		return $lista;
	}
	
	return false;
}


function obtenerProcs2($serv, $estudio, &$next = NULL) {
	global $model;
	
	$rs = $model->procedimientos(
		"num_servicio = $serv AND estudio = $estudio", 
		"id, CASE WHEN proce.cups is not null THEN proce.cups ELSE medic.codigo END as cups, descripcion, cantidad, vlr_servicio, total, codigo_paquete, codigo_cirugia, cod_usuario");
	if ($rs) {
		$i = 0;
		$lista = array();
		$sw = true;
		
		while (($row = $model->nextRow($rs))) {
			$lista[$i]["id"] = $row[0];
			$lista[$i]["codigo"] = $row[1];
			$lista[$i]["nombre"] = $row[2];
			$lista[$i]["cantidad"] = $row[3];
			$lista[$i]["valor"] = $row[4];
			$lista[$i]["total"] = $row[5];
			$lista[$i]["codigo_paquete"] = $row[6];
			$lista[$i]["codigo_cirugia"] = $row[7];
			$lista[$i]["cod_usuario"] = $row["cod_usuario"];
			$i++;
			if ($sw == true) {
				if (!empty($row[6]) || !empty($row[7])) {
					$next = 1;
					$sw = false;
				}
			}
		}
		return $lista;
	}
	
	return false;
}

function calcularTotalVentas($model, $fecha, $codigo_usuario) {
	$rs = $model->select("SUM(valor)", "pagos", "cod_usuario = '$codigo_usuario' AND fecha LIKE '$fecha' AND codigo_tipopago = 10");
	$row = $model->nextRow($rs);
	$model->freeResult($rs);
	return $row[0];
}


function comprimir_2($file_name,$model=NULL,$nameRips=NULL,$nameCarpeta='RIPS') {
	$model=new Model();
	include_once("/Utils/pclzip.lib.php");
	$fileName = "$file_name/$nameRips.zip";
	@unlink($fileName);
		
	$archive = new PclZip($fileName);
	//echo $file_name."<br />";
	//echo $fileName;	
	
	$v_list = $archive->add($file_name,PCLZIP_OPT_REMOVE_PATH, 'dev');
}

function DatosGenerales($estudio){
	$datos  = obtenerPaciente($estudio);
	$datos .= obtenerDetalles($estudio);
	return $datos;
}

function enviarEmail($address, $subject, $body, $from, $fromname, $html = false) {
	require_once $_SERVER['DOCUMENT_ROOT'].'/SismaSalud/ips/App/Model/phpmailer/class.phpmailer.php';
    $mail = new PHPMailer();
	
	$mail->From = $from;
	$mail->FromName = $fromname;
	$mail->AddAddress($address);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->IsHTML($html);
	 
	$mail->IsSMTP();
	$mail->Host = 'smtp.correo.yahoo.es';
	$mail->Port = '587';
	$mail->SMTPAuth = true;
	$mail->Username = 'myemailserversend';
	$mail->Password = 'geronimo';
	
	return $mail->Send();
}

function enviarEmailAVarios($addresses, $subject, $body, $from, $fromname, $adjunto = NULL, $html = false) {
	require_once $_SERVER['DOCUMENT_ROOT'].'/SismaSalud/ips/App/Model/phpmailer/class.phpmailer.php';
    $mail = new PHPMailer();

	$mail->From = $from;
	$mail->FromName = $fromname;
    $max = sizeof($addresses);
    for ($i = 0; $i < $max; $i++) {
        $mail->AddAddress($addresses[$i]["address"], $addresses[$i]["name"]);
    }
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->IsHTML($html);
    if (!empty($adjunto)) {
        $mail->AddAttachment($adjunto);
    }

	$mail->IsSMTP();
	$mail->Host = 'smtp.correo.yahoo.es';
	$mail->Port = '587';
	$mail->SMTPAuth = true;
	$mail->Username = 'myemailserversend';
	$mail->Password = 'geronimo';

	return $mail->Send();
}

function calcularTotales($model, $estudio) {
	$total = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio");
	$resul = $model->select("ex.subsidio, ex.maximo, m.vlr_coopago", 
		"sis_extraxemp AS ex, sis_maes AS m, contratos AS c", 
		"ex.estrato = m.cod_clasi AND ex.empresa = c.empresa AND c.codigo = m.contrato
		AND m.con_estudio = $estudio", NULL, 1);
	$sub = $resul[0];
	$max = $resul[1];
	$vlr_copago = $resul[2];
	
	$total_cobra_copago = $model->getDato("SUM(total)", "sis_deta", "estudio = $estudio AND cobra_copago = 1");
	
	$total = empty($total) ? 0 : $total;
	if ($sub == -1) {
		$copago = $vlr_copago;
	} else 	if ($sub <= 100) {
		$copago = round($total_cobra_copago * (100 - $sub) / 100,0);
		$subsidio = $sub;
	} else {
		$copago = round(($total_cobra_copago == 0) ? 0 : $sub,0);
		$subsidio = $sub;
	}
	
	if ($max != 0 && $copago > $max) {
		$copago = $max;
	}
	
	$datos = array();
	$datos["total"] = $total;
	$datos["copago"] = $copago;
	$datos["descuento"] = $model->getDato("SUM(valor)", "pagos", 
		"con_estudio = $estudio AND tipo = 3");
	$datos["neto"] = round($model->calcularNetoFactura($estudio),0);
	
	return $datos;
}

/*
	Calcula la cantidad del medicamento a descargar dada una dosificacion para este
	$a es la cantidad
	$b es el numero de dosis por unidad
*/
function calcularCantidadConsolidada($a, $b) {
    $cant = bcdiv($a, $b);
	
	if ($a / $b - $cant > 0) {
		$cant++;
	}
	
	return $cant;
}

function exportarCompras($comprobante) {
	global $model;
    $error = false;
    $msg_error = "";

    if ($_SESSION['sisma_conta'] == "1"){
        $fuente = 3;
        $fecha_vence = ""; $fecha = "";
        $tercero = "";
        $usuario = 4;
		$ccosto = 2;

        $contabilizado = $model->getDato("MAX(contabilizado)", "sis_maes", "nro_factura = '$comprobante'");

        if ($contabilizado == 0){
            $model->insertar("DELETE FROM cnt_detalles
                        WHERE id_encabezado = (SELECT id FROM cnt_encabezados WHERE nro_documento = '$comprobante' AND id_fuente = '$fuente'");
            $model->insertar("DELETE FROM cnt_encabezados WHERE nro_documento = '$comprobante' AND id_fuente = '$fuente'");
            $model->insertar("DELETE FROM cnt_facturas_cxc WHERE nro_factura = '$comprobante' AND id_fuente = '$fuente'");

            $total_credito = 0;
            $total_debito = 0;
            $rs_deta = $model->query("EXEC dbo.getCntDetaAgrupado $comprobante"); //CR
            $transacciones = array();
            while ($row_deta = $model->nextRow($rs_deta)){
                $transacciones[] = $row_deta;
                $i = count($transacciones);
                $transacciones[ $i - 1 ]['TIPO_TRAN'] = "CR";
                $transacciones[ $i - 1 ]['fecha'] = convertirFecha( $transacciones[ $i - 1 ]['fecha'] );
                $transacciones[ $i - 1 ]['fecha_vence'] = convertirFecha( $transacciones[ $i - 1 ]['fecha_vence'] );
				$transacciones[ $i - 1 ]['causacion'] = 0;
				$transacciones[ $i - 1 ]['descuento'] = 0;
                $total_credito += $row_deta['valor'];
            }
            $model->freeResult($rs_deta);

            $rs_maes = $model->query("EXEC dbo.getCntMaesAgrupado $comprobante"); // DB
            $total_descuento = 0; $total_maes = 0;
            while ($row_maes = $model->nextRow($rs_maes)){
				$row_maes['ccosto'] = $ccosto;

                $row_maes_total = array("fecha"=>convertirFecha( $row_maes['fecha'] ),
                        "fecha_vence"=>convertirFecha( $row_maes['fecha_vence'] ),
                        "cuenta"=>$row_maes['cuenta'],
                        "descripcion"=>$row_maes['descripcion'],
                        "tercero"=>$row_maes['tercero'],
                        "ccosto"=>$row_maes['ccosto'],
                        "valor"=>$row_maes['valor'],
                        "fuente"=>$row_maes['fuente'],
                        "tipo_factura"=>$row_maes['tipo_factura'],
                        "nro_factura"=>$row_maes['nro_factura'],
                        "TIPO_TRAN"=>"DB",
						"causacion"=>1,
						"descuento"=>0
                        );
                $tercero = $row_maes['tercero'];
                $fecha = $row_maes_total['fecha'];
                $fecha_vence = $row_maes_total['fecha_vence'];
                $total_maes += $row_maes_total['valor'];

                $row_maes_coopago = array("fecha"=>convertirFecha( $row_maes['fecha'] ),
                        "fecha_vence"=>convertirFecha( $row_maes['fecha_vence'] ),
                        "cuenta"=>$row_maes['cuenta_coopago'],
                        "descripcion"=>$row_maes['descripcion_coopago'],
                        "tercero"=>$row_maes['tercero'],
                        "ccosto"=>$row_maes['ccosto'],
                        "valor"=>$row_maes['valor_coopago'],
                        "fuente"=>$row_maes['fuente'],
                        "tipo_factura"=>$row_maes['tipo_factura'],
                        "nro_factura"=>$row_maes['nro_factura'],
                        "TIPO_TRAN"=>"DB",
						"causacion"=>0,
						"descuento"=>0
                        );
                $row_maes_descuento = array("fecha"=>convertirFecha( $row_maes['fecha'] ),
                        "fecha_vence"=>convertirFecha( $row_maes['fecha_vence'] ),
                        "cuenta"=>$row_maes['cuenta_descto'],
                        "descripcion"=>$row_maes['descripcion_descto'],
                        "tercero"=>$row_maes['tercero'],
                        "ccosto"=>$row_maes['ccosto'],
                        "valor"=>$row_maes['valor_descto'],
                        "fuente"=>$row_maes['fuente'],
                        "tipo_factura"=>$row_maes['tipo_factura'],
                        "nro_factura"=>$row_maes['nro_factura'],
                        "TIPO_TRAN"=>"DB",
						"causacion"=>0,
						"descuento"=>1
                        );
                $total_descuento += $row_maes_descuento['valor'];

                $transacciones[] = $row_maes_total;
                $transacciones[] = $row_maes_coopago;
                $transacciones[] = $row_maes_descuento;

                $total_debito += $row_maes['valor'] + $row_maes['valor_coopago'] + $row_maes['valor_descto'];
            }
            $model->freeResult($rs_deta);


            if ($total_credito == $total_debito){
                $sql = "INSERT INTO cnt_encabezados
                            (nro_documento, fecha_documento, total_db, total_cr,
                            descripcion, estado, contabilizado, id_fuente,
                            id_usuario)
                    VALUES ('$comprobante', '$fecha', $total_debito, $total_credito,
                            'FACTURACION App - $comprobante', 'P', 0, '$fuente',
                            '$usuario')";
                $model->insertar($sql);
                $msg_error = "ERROR ENCABEZADO";
                $error = ($model->getLastErrorCode() != 0);

                $max = $model->select("MAX(id)", "cnt_encabezados", "nro_documento = '$comprobante' AND id_fuente = '$fuente'", NULL, 1);
                $id_encabezado = $max[0];
                if ($id_encabezado <= 0){
                    $error = true;
                }


                if (!$error){
                    if (strlen(trim($fecha_vence)) == 0 ){
						$fecha_vence = $fecha;
					}

                    $sql = "INSERT INTO cnt_facturas_cxc
                                (nro_factura, descripcion, fecha, fecha_vencimiento,
                                iva, saldo, descuento, abono, valor, contabilizado,
                                estado, retefuente, reteica, reteiva, nota, ultima_modificacion,
                                id_tercero, id_fuente, id_encabezado, id_usuario )
                            VALUES
                                ('$comprobante', 'FACTURACION App - $comprobante', '$fecha', '$fecha_vence',
                                0, $total_maes, 0, 0, $total_maes, 0,
                                'P', 0, 0, 0, 0, GETDATE(),
                                '$tercero', '$fuente', $id_encabezado, $usuario)";
                    $model->insertar($sql);
                    $msg_error = "ERROR cnt_facturas_cxc";
                    $error = ($model->getLastErrorCode() != 0);


                    for ($i = 0; $i < count($transacciones) && !$error; $i++){
                        $tr = $transacciones[$i];
                        if ($tr['valor'] > 0){
                            $sql = "INSERT INTO cnt_detalles
                                        (fecha, descripcion, valor, tipo_transaccion,
                                        es_causacion, es_reteimpuesto, es_descuento,
                                        id_cuenta, id_ccosto, id_usuario, id_tercero,
                                        id_encabezado)
                                    VALUES
                                        ('{$tr['fecha']}', '{$tr['descripcion']}', '{$tr['valor']}','{$tr['TIPO_TRAN']}',
                                        '{$tr['causacion']}', 0, '{$tr['descuento']}',
                                        '{$tr['cuenta']}', '{$tr['ccosto']}', '$usuario', '{$tr['tercero']}',
                                        '$id_encabezado')";
                            $model->insertar($sql);
                            $msg_error = "ERROR cnt_transacciones";
                            $error = ($model->getLastErrorCode() != 0);
                        }
                    }
                }
            }else{
                $msg_error = "DB != CR";
                $error = true;
            }
        }else{
            $msg_error = "CONTABILIZADO";
            $error = true;
        }
    }
    if ($error){
        echo "<br>$msg_error<br>";
    }
    return $error;
}

function filtrarArray($target, $patron){
	$resultado = array();
	foreach($target as $k => $val){
		if(strpos($k, $patron) !== false){
			$resultado[$k] = $val;
		}
	}
	
	return $resultado;
}

function ArrayDataToUtf8_Encode(array &$target){
	
	foreach($target as $k => $p){
		if(!is_array($p)){
			$target[$k] = utf8_encode($p);
		}else{
			$target[$k] = ArrayDataToUtf8_Encode($p);
		}
	}
	return $target;
}

function GetErroresValidacion($error){
	$validaciones = array();
	// Se valida si el error retornado es de validacion (es decir tiene la palabra "validate:")
	if(strpos($error, "validate:") !== false){
		$error = str_replace("validate:", "", $error);

		//Extraemos los mensajes de error (si son mas e uno se separa por " ; ")
		$errores = explode(" ; ", trim($error));
		foreach ($errores as $key => $value) {
			//se extrae el campo al cual se refier el error y su mensaje (deben estar separados por " - ")
			$mensaje = explode(" - ", $value);
			$validaciones[$mensaje[0]] = $mensaje[1];
		}
	}
	return $validaciones;
}

?>