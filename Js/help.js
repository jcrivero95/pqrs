$(document).ready(function () {
    $('#sidebar .list-unstyled > li > a').click(function () {
        $('#sidebar .list-unstyled > li > a').removeClass("active");
        $(this).addClass('active');
        if ($(this).hasClass('active')) {
            $(this).find('.fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-up')
        } else {
            $(this).find('.fa-angle-up').removeClass('fa-angle-up').addClass('fa-angle-down')
        }
        $(this).parent().find('ul.collapse').toggleClass('show');
    });

    $('#sidebar .list-sub-unstyled > li > a').click(function () {
        $('#sidebar .list-sub-unstyled > li > a').removeClass('active');
        $(this).addClass('active');
    });

})

function cerrarSesion() {
    procesador.procesar = function (response) {
        if ($.trim(response.ERRORMSG) == '') {
            reload()
            $('#logout').submit();
           
        } else {
            msgDanger(resp.ERRORMSG);
        }

    };
    getJSONAjax({ operacion: 'Logout' }, "Controlador/login.php", procesador, "POST", false, "", true);

}

function ModalCambiarPsw() {
    limpiarFormulario("formActPsw");
    $('#modalActPsw').modal('toggle');
}

function fnActualizarPsw() {
    LimpiarErrorValidacion();
    url = 'Controlador/login.php?operacion=actualizarPsw';
    if (!validarPassword('#pwsCambio')) {
        return false;
    }
    if (!validarConfirmacionPassword('#pwsCambio', '#pwsCambioConfirm')) {
        return false;
    }
    procesador.procesar = function (response) {
        console.log(response);
        if (response.ERRORMSG == null) {
            ModalCambiarPsw();
        } else {
            ErrorValidacion(response);
            if (response.ERRORMSG != true) {
                msgDanger(response.ERRORMSG);
            }
        }
    };
    getJSONAjaxFormData('formActPsw', url, procesador, "POST", false, "");
}

function ErrorValidacion(response) {
    for (var key in response) {
        if (key != "ERRORMSG") {
            $('#' + key).addClass("is-invalid");
            $('#' + key).siblings('.invalid-feedback').html(response[key]);
        }
    }
}

function LimpiarErrorValidacion() {
    $("input").removeClass('is-invalid');
    $("select").removeClass('is-invalid');
    $("textarea").removeClass('is-invalid');
    $(".invalid-feedback").html("");
}

function _loading(estado) {
    if (estado) {
        $('#imgSimbolo').attr('src', 'Imagenes/loadingBtn.gif')
        $('.btn').attr('disabled', true);
    } else {
        $('#imgSimbolo').attr('src', 'Imagenes/simbolo.png')
        $('.btn').attr('disabled', false);

    }
}

function validarPassword(pws) {
    var psw = $(pws).val();
    if (psw.length < 5) {
        $(pws).addClass("is-invalid");
        $(pws).next().html('La contraseña debe tener minimo 5 caracteres');
        return false;
    } else {
        $(pws).removeClass("is-invalid");
        $(pws).next().html('');
        return true;
    }
}

function validarConfirmacionPassword(pws1, pws2) {
    var psw = $(pws1).val();
    var pswConfirm = $(pws2).val();

    if (psw != pswConfirm) {
        $(pws2).addClass("is-invalid");
        $(pws2).next().html('Las contraseñas no coinciden');
        return false;
    } else {
        $(pws2).removeClass("is-invalid");
        $(pws2).next().html('');
        return true;

    }
}

function limpiarFormulario(id) {
    $("#" + id)[0].reset();
    $("input[type='hidden']").val('');
}

function inactivarLinksMenu() {
    $('#sidebar a').attr('disabled', true)
}

function activarLinksMenu() {
    $('#sidebar a').attr('disabled', false)
}

function inactivarTabs(tab, div) {
    var tabs = $(tab).parent().parent().find('.nav-link');
    tabs.attr('disabled', true);
    tabs.attr('data-toggle', '');
    tabs.removeClass('active');
    $(tab).addClass('active');
    $('.tab-pane').removeClass('show active');
    $(div).addClass('show active');
}
function activarTabs(tab) {
    var tabs = $(tab).parent().parent().find('.nav-link');
    tabs.attr('disabled', false);
    tabs.attr('data-toggle', 'tab');
}
function cargarInfo(datos, div) {

    var columns = [];
    table = $('<table id="tablaDatos" class="table table-hover table-bordered table-striped" style="width: 100%;" role="grid" aria-describedby="example_info">')
    thead = $('<thead>')
    tr = $('<tr>');

    if (datos.length > 0) {
        botones = '';
        Object.keys(datos[0]).forEach(function (item) {
            var th = $('<th>');
            th.addClass('text-capitalize')

            if (!item.match(/hidden.*/)) {
                columns.push({ data: item })
                th.html(item);
                th.appendTo(tr);
            }

        })
        columns.push({
            data: null
            , defaultContent: ''
        })


        var th = $('<th>');
        th.html('');
        th.appendTo(tr);

        tr.appendTo(thead)
        thead.appendTo(table);
        table.appendTo(div);


        var table = $("#tablaDatos").DataTable({
            data: datos,
            columns: columns,
            order: [[1, "desc"]],
            columnDefs: [
                {
                    "render": function (data, type, row) {
                        var botones = '';

                        if (data["visualizar_archivo_hidden"] == 1) {
                            botones += '<button class="btn-table" title="Visualizar Ordenamiento" id="btnGenerarPdf" onclick="generarPdf(this)">'
                                + '<i class="far fa-file-pdf"></i>'
                                + '</button>';
                        }
                        if (data["enviar_email_hidden"] == 1) {
                            botones += '<button class="btn-table" title="Enviar a Email" id="btnEnviarPorEmail">'
                                + '<i class="far fa-envelope-open"></i>'
                                + '</button>';
                        }
                        if (data["es_video_consulta_hidden"] == 1) {
                            botones += '<button class="btn-table" title="VideoConsulta" onclick="iniciarVideoConsulta(this)" >'
                                + '<i class="fas fa-video"></i>'
                                + '</button>';
                        }
                        if (data["archivo_recodatorio_hidden"] != undefined && $.trim(data["archivo_recodatorio_hidden"]) != '') {
                            botones += '<button class="btn-table" title="Recordatorio de cita" onclick="recordatorioCita(this)" >'
                                + '<i class="far fa-sticky-note"></i>'
                                + '</button>';
                        }
                        if (data["opciones_gestion_hidden"] != undefined && $.trim(data["opciones_gestion_hidden"]) == 1) {
                            botones += '<button id="btnGestionSolicitud' + data["id_hidden"] + '" class="btn-table" title="Ver gestiones de cita" onclick="visualizarDevolucion(this)" >'
                                + '<i class="fa fa-list" aria-hidden="true"></i>'
                                + '</button>';
                        }
                        return botones;


                    },
                    "targets": -1
                }
            ],

            "lengthMenu": [[6, 25, 50, -1], [6, 25, 50, "All"]],
            scrollY: '25vh',
            scrollCollapse: true,
            paging: true,
            processing: true,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Registros",
                "infoEmpty": "Mostrando 0 to 0 of 0 Registros",
                "infoFiltered": "(Filtrado de _MAX_ total registros)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Registros",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        });

        $('#tablaDatos tbody td').on('click', 'button#btnEnviarPorEmail', function () {
            var data = table.row($(this).parents('tr')).data();
            var email = '<?php echo $_SESSION["email_pcte_portal"];?>';
            $('#archivoReporte').val(data['archivo_reporte_hidden']);
            $('#archivoAdjunto').val(data['archivo_adjunto_hidden']);
            $('#parametrosReporte').val(data['parametros_reporte_hidden']);
            $('#emailInput').val(email);
            $('#modalEnvioEmail').modal('toggle');
        });
    } else {
        div.addClass('text-center pt-3')
        h4 = $('<h4 class="text-center">');
        i = $('<i class="far fa-file-excel fa-10x">')
        h4.html('No se encontraron resultados')
        i.appendTo(div)
        h4.appendTo(div)


    }

}

function generarPdf(aux) {
    var tabla = $('#tablaDatos').DataTable();
    var data = tabla.row($(aux).parents('tr')).data();
    console.log(data)
    var ruta = '../portalUsuarios/Reportes/' + data['archivo_reporte_hidden'] + data['parametros_reporte_hidden'] + '&visualizar=1';
    window.open(ruta)
}

function recordatorioCita(aux) {
    var tabla = $('#tablaDatos').DataTable();
    var data = tabla.row($(aux).parents('tr')).data();
    var ruta = '../portalUsuarios/Reportes/' + data['archivo_recodatorio_hidden'];
    window.open(ruta, 'Continue_to_Application', 'width=500')
    // window.open(ruta)
}

function iniciarVideoConsulta(aux) {
    var tabla = $('#tablaDatos').DataTable();
    var data = tabla.row($(aux).parents('tr')).data();
    var ruta = data['servidor_videoconsultas_hidden'] + '?id=' + data['id_hidden'];
    window.open(ruta)

}

function visualizarDevolucion(aux) {
    var tabla = $('#tablaDatos').DataTable();
    var data = tabla.row($(aux).parents('tr')).data();
    abrirModalDevolucion(data['id_hidden']);
}

function abrirModalDevolucion(idSolicitud) {
    var url = "../portalUsuarios/controlador/ctrlPortal.php";
    var data = { operacion: "visualizarDevolucionCita", idSolicitud: idSolicitud }

    procesador.procesar = function (response) {
        $('#gestionesCitas').find('.modal-body').html(response.payload)

        $('#btnGestionSolicitud' + idSolicitud).find('i').removeClass('fa-spinner');
        $('#btnGestionSolicitud' + idSolicitud).find('i').addClass('fa-list');
        $('button[id ^= btnGestionSolicitud]').attr('disabled', false);
        $('#gestionesCitas').modal('toggle')
    };
    $('#btnGestionSolicitud' + idSolicitud).find('i').removeClass('fa-list');
    $('#btnGestionSolicitud' + idSolicitud).find('i').addClass('fa-spinner');
    $('button[id ^= btnGestionSolicitud]').attr('disabled', true);
    getJSONAjax(data, url, procesador, "GET", false, "");
}

function validarArchivosSolicitudes(idSolicitud) {
    var noValidar = '';
    var url = "Controlador/ctrlPortal.php?operacion=adjuntarArchivosSolicitud&idSolicitud=" + idSolicitud;
    procesador.procesar = function (response) {
        console.log(response)
        if (response.subida_adjuntos == true) {
            // limpiarFormulario('formSolicitud');
            toastr.remove();
            toastr.success('Archivos subidos con Exito');
        } else {
            msgDanger('Error al cargar adjuntos \n' + response.mensaje_adjuntos);
        }
    };
    getJSONAjaxFormData('form_' + idSolicitud, url, procesador, "POST", false, noValidar);
}

function cancelarCitaVirtual(idSolicitud,resumen) {
    var noValidar = '';
    var url = "Controlador/ctrlAtencionUsuario.php";
    var data = {
        operacion: 'cancelarCitaVirtual',
        id: idSolicitud,
        resumen: resumen
    };
    procesador.procesar = function (response) {
        if (response.datos.con == 2) {
            // limpiarFormulario('formSolicitud');
            toastr.remove();
            toastr.success('Cita cancelada con exito');
            // $('#cancelarcitavirtul').modal('close')
            $("#cancelarcitavirtul .close").click()
        } else {
            msgDanger('Error al cancelar la cita');
        }
    };
    getJSONAjaxFormData(data, url, procesador, "POST", false, noValidar);
}

function verArchivosSolicitud(idSolicitud) {
    $('#filesSol').html('');
    var noValidar = '';
    var data = { operacion: "verArchivosSolicitud", idSolicitud: idSolicitud }
    var url = "Controlador/ctrlPortal.php";
    procesador.procesar = function (response) {
        archivos = response.archivos;
        console.log(archivos)
        if (archivos.length > 0) {
            var html = '';

            archivos.forEach(function (e) {

                html += '<div class="col-md-4" id="adj' + e.nombre + '">' +
                    '<div class="card"> <center>' +
                    '<img src="imagenes/file.png" width="40%" >' +
                    '</center>' +
                    '<div class="card-body">' +
                    ' <small class="card-text">' + e.nombre + '</small> <br>' +
                    '<a href="../portalUsuarios/Vistas/citas medicas/adjuntos/' + e.nombre + '" download="' + e.nombre + '" class="btn btn-default" title="Descargar"><i class="fa fa-download"></i></a>' +
                    '<a  onclick="eliminarArchivoSolicitud(' + e.id + ',' + idSolicitud + ')" class="btn btn-default" title="Eliminar"><i class="fa fa-times"></i></a>' +
                    '</div>  </center>  </div> </div>';

            });
            $('#filesSol').html(html);

        } else {
            msgDanger('No existen archivos para visualizar');
        }
    };
    getJSONAjax(data, url, procesador, "GET", false, noValidar);

}

function verArchivosResumen(idSolicitud) {
    $('#filesSol').html('');
    var noValidar = '';
    var data = { operacion: "verArchivosResumen", idSolicitud: idSolicitud }
    var url = "Controlador/ctrlAtencionUsuario.php";
    procesador.procesar = function (response) {
        archivos = response.archivos;
        console.log(archivos)
        if (archivos.length > 0) {
            var html = '';

            archivos.forEach(function (e) {
             
                html += '<div class="col-md-4" id="adj' + e.nombre + '">' +
                    '<div class="card"> <center>' +
                    '<img src="imagenes/file.png" width="40%" >' +
                    '</center>' +
                    '<div class="card-body"><center> ' +
                    ' <small class="card-text">' + e.nombre + '</small> <br>' +
                    '<a href="../portalUsuarios/Archivos/adjuntos_citasV/' + e.nombre + '" download="' + e.nombre + '" class="btn btn-default" title="Descargar"><i class="fa fa-download"></i></a>' +
                    '</div>  </center>  </div> </div>';

            });
            $('#filesSol').html(html);

        } else {
            msgDanger('No existen archivos para visualizar');
        }
    };
    getJSONAjax(data, url, procesador, "GET", false, noValidar);
}

function Calificar(id,califi) {
    var noValidar = '';
    var data = { operacion: "calificar", idSolicitud: id, califica: califi}
    var url = "Controlador/ctrlAtencionUsuario.php";
    procesador.procesar = function (response) {
        if (response.datos[0].con >= 1) {
            // limpiarFormulario('formSolicitud');
            toastr.remove();
            toastr.success('Calificacion guardada');
            $("#divhide2").attr('hidden',true)
            $("#divhide").attr('hidden',false)
            // $("#resumenCita .close").click()
        } else {
            msgDanger('Error al calificar');
        }
    };
    getJSONAjax(data, url, procesador, "GET", false, noValidar);
}

function eliminarArchivoSolicitud(archivo, idSolicitud) {
    var data = { operacion: "eliminarArchivoSolicitud", archivo: archivo, idSolicitud: idSolicitud }
    var url = "../portalUsuarios/controlador/ctrlPortal.php";

    procesador.procesar = function (response) {
        if (response.stauts) {
            archivos = response.archivos;
            console.log(archivos)
            html = '';
            archivos.forEach(function (e) {

                html += '<div class="col-md-3" id="adj' + e.nombre + '">' +
                    '<div class="card"> <center>' +
                    '<img src="imagenes/file.png" width="40%" >' +
                    '</center>' +
                    '<div class="card-body">' +
                    ' <small class="card-text">' + e.nombre + '</small> <br>' +
                    '<a href="../portalUsuarios/Vistas/citas medicas/adjuntos/' + e.nombre + '" download="' + e.nombre + '" class="btn btn-default" title="Descargar"><i class="fa fa-download"></i></a>' +
                    '<a  onclick="eliminarArchivoSolicitud(' + e.id + ',' + idSolicitud + ')" class="btn btn-default" title="Eliminar"><i class="fa fa-times"></i></a>' +
                    '</div>  </center>  </div> </div>';

            });
            $('#filesSol').html(html);
        }

    };
    getJSONAjax(data, url, procesador, "GET", false, "");
}