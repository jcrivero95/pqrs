<?php include("_nav.php"); ?>


<?php $PacienteSolicita = $model->getParametroGeneral('SolicitarCitaPortal', 'PORTAL USUARIO');
$PacienteAparta = $model->getParametroGeneral('ApartarCitaPortal', 'PORTAL USUARIO');
$idpaciente = explode(':', $_SESSION["id_pcte_portal"]);
$datosp = $model->RSAsociativo("EXEC spPortalUsuarios @op = 'datosPaciente2', @autoid='" . $_SESSION["IdUsuario"] . "'");

?>

    <script>
        $(document).ready(function () {
            $('#posicion').html('RADICAR PQRS');

            alertaModal('Estimado Usuario(a), a través de este espacio podrás radicar todas tus' +
                'felicitaciones y/o cualquier tipo de petición. Diligencia toda la información requerida y ' +
                'recibirás un número de radicado con el cual podrás consultar el estado de tu PQRS. Recibirás tu respuesta ' +
                'a través del correo electrónico registrado y también se reflejará en Portal del Usuario.');


        })
    </script>

    <script>
        function muestradatos() {
            var val = $("#igualAfectado").val();
            if (val == 1) {
                $("#dt_afectado").addClass("dt_af");
            } else {
                $("#dt_afectado").removeClass("dt_af");
            }
        }


        function guardar() {

            if ($('#id_tipo').val() == '') {
                toastr["warning"]("Seleccione un tipo de identificación", "");
                $("#id_tipo").focus();
                return false;
            }

            if ($('#id').val() == '') {
                toastr["warning"]("Numero de identificacion requerido", "");
                $("#id").focus();
                return false;
            }
            if ($('#nombres').val() == '') {
                toastr["warning"]("Nombres del solicitante Requerido", "");
                $("#nombres").focus();
                return false;
            }
            if ($('#apellidos').val() == '') {
                toastr["warning"]("Apellidos del solicitante Requerido", "");
                $("#apellidos").focus();
                return false;
            }
            if ($('#direccion').val() == '') {
                toastr["warning"]("Dirección del solicitante Requerido", "");
                $("#direccion").focus();
                return false;
            }
            if ($('#correo').val() == '') {
                toastr["warning"]("Correo del solicitante Requerido", "");
                $("#correo").focus();
                return false;
            }
            if ($('#telefono').val() == '') {
                toastr["warning"]("Teléfono del solicitante Requerido", "");
                $("#telefono").focus();
                return false;
            }

            if ($('#igualAfectado').val() == 0 && $("id_tipo_AF").val() == "") {
                toastr["warning"]("Selecciona el tipo de identificacion del afectado requerido", "");
                $("#igualAfectado").focus();
                return false;
            }
            if ($('#igualAfectado').val() == 0 && $('#id_AF').val() == "") {
                toastr["warning"]("Numero de identificacion del afectado requerido", "");
                $("#id_AF").focus();
                return false;
            }
            if ($('#igualAfectado').val() == 0 && $('#nombres_AF').val() == "") {
                toastr["warning"]("Nombre del afectado requerido", "");
                $("#nombres_AF").focus();
                return false;
            }
            if ($('#igualAfectado').val() == 0 && $('#apellidos_AF').val() == "") {
                toastr["warning"]("Apellido del afectado requerido", "");
                $("#apellidos_AF").focus();
                return false;
            }
            if ($('#igualAfectado').val() == 0 && $('#direccion_AF').val() == "") {
                toastr["warning"]("Dirección del afectado requerido", "");
                $("#direccion_AF").focus();
                return false;
            }
            if ($('#igualAfectado').val() == 0 && $('#correo_AF').val() == "") {
                toastr["warning"]("Correo del afectado requerido", "");
                $("#correo_AF").focus();
                return false;
            }
            console.log($('#tipo_req').val());
            if ($('#tipo_req').val() == null) {
                toastr["warning"]("Seleccione un el tipo de la solicitud", "");
                $("#tipo_req").focus();
                return false;
            }

            if ($('#sub_tipo').val() == null) {
                toastr["warning"]("Seleccione un sub tipo", "");
                $("#sub_tipo").focus();
                return false;
            }
            if ($('#descripcion').val() == "") {
                toastr["warning"]("Descripcion de la solicitud requerida", "");
                $("#descripcion").focus();
                return false;
            }


            var url = "Controlador/ctrlPQRS.php?operacion=guardarFormulario";
            /*
            var data = {
                operacion: 'guardarFormulario',
            };
**/

            procesador.procesar = function (response) {

                $('#btn_Solicitar').attr('disable', 'true')
                datos = response.datos
                // console.log(response);

                if (datos[0].Tipo == "success") {
                    Swal.fire(
                        datos[0].Mensaje,
                        "NÚMERO DE RADICADO ASIGNADO: " + datos[0].id_consecutivo,
                        'success'
                    ).then((value) => {
                       // location.reload();
                    });
                    $('#btn_Solicitar').attr('disable', 'false')

                } else {
                    Swal.fire(
                        datos[0].Mensaje,
                        'Error',
                        'error'
                    )

                    $('#btn_Solicitar').attr('disable', 'false')
                }

            };

            getJSONAjaxFormData("formulario", url, procesador, "POST", false, "");


        }


        /**
         function municipios() {
            var url = "controlador/ctrlActualizarDatos.php?operacion=municipio";

            procesador.procesar = function(response) {
                $('#municipio').html(response.muni);
            };
            var data = {
                id_depar: $('#departamento').val()
            };
            getJSONAjax(data, url, procesador, "POST", false, "");
        }*/
    </script>
    <style type="text/css">
        body {
            color: #495057;
            background-color: #ebf5fb !important;
        }

        .tarjeta {
            background-color: white;
            border: 1px solid #dadce0;
            border-radius: 8px !important;
            width: 100%;
        }

        .tarjetaHeader {
            background-color: rgb(124 189 225);
            color: #fff;
            border-radius: 8px 8px 0px 0px !important;
            border-color: rgb(124 189 225) !important;
        }

        .margen {
            margin-left: 0.5rem !important;
            margin-bottom: 1.5rem !important;
            margin-right: 0.5rem !important;
            padding: 0px;
        }


        .sub__title {
            background-color: #0e7eb3;
            width: 100%;
            padding: 5px;
            color: #fff;
            border-radius: 5px;
            text-align: center;
        }

        .afectado {
            background-color: #7cbde3;
            width: 100%;
            padding: 5px;
            color: #fff;
            border-radius: 5px;
            text-align: center;
        }

        #btn_Solicitar {
            background-color: #0e7eb3;
            padding: 5px;
            color: #fff;
            border-radius: 5px;
        }

        .label_content {
            background-color: #7cbde3;
            padding: 4px 20px 4px 20px;
            color: #fff;
            border-radius: 5px;
        }

        .dt_af {
            display: none;
        }

        label {
            font-size: 14px;
        }
    </style>

    <body>
    <div class="wrapper">
        <div id="content">
            <form id="formulario">
                <div id="contenido2" class="">
                    <div class="container" style="background-color: #fff;padding: 20px;border-radius: 10px;">
                        <div class="row margen">
                            <div class="col-md-12">
                                <h6 class="sub__title">DATOS DEL SOLICITANTE</h6>
                            </div>
                            <div class="col-md-12 mt-md-4 mt-2">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6"><label for="igualAfectado" class="afectado">¿La
                                            petición se realiza a nombre propio?</label></div>
                                    <div class="col-lg-2 col-md-2">
                                        <select class="form-control form-control-sm" onchange="muestradatos()"
                                                id="igualAfectado" name="igualAfectado">
                                            <option value="0">No</option>
                                            <option value="1" selected>Si</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <label for="" class="m-0">Tipo ID</label>
                                <input type="hidden" id="entidad" name="entidad">
                                <input type="hidden" id="id_sede" name="id_sede">
                                <input type="hidden" id="PuntoAtencion" name="PuntoAtencion">
                                <input type="hidden" id="autoid" name="autoid">
                                <input type="hidden" id="cod_dep" name="cod_dep">
                                <input type="hidden" id="cod_muni" name="cod_muni">
                                <input type="hidden" id="entidad_AF" name="entidad_AF">
                                <input type="hidden" id="id_sede_AF" name="id_sede_AF">
                                <input type="hidden" id="PuntoAtencion_AF" name="PuntoAtencion_AF">
                                <input type="hidden" id="autoid_AF" name="autoid_AF">
                                <input type="hidden" id="cod_dep_AF" name="cod_dep_AF">
                                <input type="hidden" id="cod_muni_AF" name="cod_muni_AF">
                                <select class="form-control form-control-sm" id="id_tipo" name="id_tipo"
                                        value="<?php echo($datosp[0]["tipo_docu"]) ?>">
                                    <option value="CC">Cedula de Ciudadania</option>
                                    <option value="CE">Cedula de Extranjeria</option>
                                    <option value="PA">Pasaporte</option>
                                    <option value="RC">Registro Civil</option>
                                    <option value="TI">Tarjeta de Identidad</option>
                                    <option value="AS">Adulto sin Identificacion</option>
                                    <option value="MS">Menor sin Identificacion</option>
                                    <option value="NU">Numero Unico de Iden</option>
                                    <option value="NI">Nit</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 mt-md-0 mt-2">
                                <label for="id" class="m-0">No. ID</label>
                                <input type="number" class="form-control form-control-sm" name="id" id="id"
                                       placeholder="" onblur="cargarPaciente()"
                                       value="<?php echo($datosp[0]["documento"]) ?>">
                            </div>
                            <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-2 mt-2">
                                <label for="nombres" class="m-0">Nombres</label>
                                <input type="text" class="form-control form-control-sm" name="nombres" id="nombres"
                                       placeholder="" ">
                            </div>
                            <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-2 mt-2">
                                <label for="apellidos" class="m-0">Apellidos</label>
                                <input type="text" class="form-control form-control-sm" name="apellidos" id="apellidos"
                                       placeholder=""
                                       value="<?php echo(ucwords(strtolower($datosp[0]["apellidos"]))) ?>">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-2">
                                <label for="direccion" class="m-0">Dirección</label>
                                <input type="text" class="form-control form-control-sm" name="direccion" id="direccion"
                                       placeholder=""
                                       value="<?php echo(ucwords(strtolower($datosp[0]["direccion"]))) ?>">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-md-2 mt-2">
                                <label for="correo" class="m-0">Correo electrónico</label>
                                <input type="email" class="form-control form-control-sm" name="correo" id="correo"
                                       placeholder="" value="<?php echo($datosp[0]["email"]) ?>">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">No. Teléfono</label>
                                <input type="number" class="form-control form-control-sm" name="telefono" id="telefono"
                                       placeholder="" value="<?php echo($datosp[0]["telefono"]) ?>">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">No. Teléfono #2</label>
                                <input type="number" class="form-control form-control-sm" name="telefono2"
                                       id="telefono2" placeholder="" value="<?php echo($datosp[0]["telefono"]) ?>">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">Convenio</label>
                                <select class="form-control form-control-sm" id="eps" name="eps">
                                    <option disabled="" selected="" value="0">seleccionar...</option>
                                    <option value="Nueva EPS regimen contributivo">Nueva EPS régimen contributivo
                                    </option>
                                    <option value="Nueva EPS regimen subsidiado">Nueva EPS régimen subsidiado</option>
                                    <option value="Magisterio">Magisterio</option>
                                </select>
                            </div>

                        </div>

                        <div class="row margen dt_af" id="dt_afectado">
                            <div class="col-md-12">
                                <h6 class="sub__title">DATOS DEL PACIENTE</h6>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <label for="" class="m-0">Tipo ID</label>
                                <select class="form-control form-control-sm" id="id_tipo_AF" name="id_tipo_AF">
                                    <option value="CC">Cedula de Ciudadania</option>
                                    <option value="CE">Cedula de Extranjeria</option>
                                    <option value="PA">Pasaporte</option>
                                    <option value="RC">Registro Civil</option>
                                    <option value="TI">Tarjeta de Identidad</option>
                                    <option value="AS">Adulto sin Identificacion</option>
                                    <option value="MS">Menor sin Identificacion</option>
                                    <option value="NU">Numero Unico de Iden</option>
                                    <option value="NI">Nit</option>
                                </select>
                            </div>
                            <div class="col-lg-3 mt-lg-0 col-md-6 mt-2">
                                <label for="id" class="m-0">No. ID</label>
                                <input type="text" class="form-control form-control-sm" onblur="cargarPaciente_AF()"
                                       name="id_AF" id="id_AF">
                            </div>
                            <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-2 mt-2">
                                <label for="nombres_AF" class="m-0">Nombres</label>
                                <input type="text" class="form-control form-control-sm" name="nombres_AF"
                                       id="nombres_AF" placeholder="">
                            </div>
                            <div class="col-lg-3 col-md-6 mt-lg-0 mt-md-2 mt-2">
                                <label for="apellidos_AF" class="m-0">Apellidos</label>
                                <input type="text" class="form-control form-control-sm" name="apellidos_AF"
                                       id="apellidos_AF" placeholder="">
                            </div>

                            <div class="col-lg-6 col-md-6 mt-2">
                                <label for="direccion_AF" class="m-0">Direccion</label>
                                <input type="text" class="form-control form-control-sm" name="direccion_AF"
                                       id="direccion_AF" placeholder="">
                            </div>
                            <div class="col-lg-6 col-md-6 mt-md-2 mt-2">
                                <label for="correo_AF" class="m-0">Correo electrónico</label>
                                <input type="email" class="form-control form-control-sm" name="correo_AF" id="correo_AF"
                                       placeholder="">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">No. Teléfono</label>
                                <input type="number" class="form-control form-control-sm" name="telefono_AF"
                                       id="telefono_AF"
                                       placeholder="" value="">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">No. Teléfono #2</label>
                                <input type="number" class="form-control form-control-sm" name="telefono2_AF"
                                       id="telefono2_AF" placeholder="" value="">
                            </div>
                            <div class="col-lg-4 col-md-6 mt-lg-2 mt-md-2 mt-2">
                                <label for="" class="m-0">Convenio</label>
                                <select class="form-control form-control-sm" id="eps" name="eps_AF">
                                    <option disabled="" selected="" value="0">seleccionar...</option>
                                    <option value="Nueva EPS regimen contributivo">Nueva EPS régimen contributivo
                                    </option>
                                    <option value="Nueva EPS regimen subsidiado">Nueva EPS régimen subsidiado</option>
                                    <option value="Magisterio">Magisterio</option>
                                </select>
                            </div>
                        </div>

                        <div class="row margen">
                            <div class="col-md-12">
                                <h6 class="sub__title">DATOS DE LA SOLICITUD</h6>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6 pr-md-3 mt-2">
                                        <label for="tipo_req" class="m-0 label_content"
                                               style="width: 100%;text-align: center;">Tipo Requerimiento</label>
                                    </div>
                                    <div class="col-lg-6 pr-md-3 mt-md-2 mt-2">
                                        <select class="form-control form-control-sm" id="tipo_req" name="tipo_req">
                                            <option disabled="" selected="" value="0">seleccionar...</option>
                                            <option value="Felicitacion">Felicitación</option>
                                            <option value="Derecho peticion">Derecho petición</option>
                                            <option value="Queja">Queja</option>
                                            <option value="Reclamo">Reclamo</option>
                                            <option value="Solicitud">Solicitud</option>
                                            <option value="Sugerencia">Sugerencia</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4 pr-md-3 mt-md-2 mt-2">
                                        <label for="sub_tipo" class="m-0 label_content"
                                               style="width: 100%;text-align: center;">SubTipo</label>
                                    </div>
                                    <div class="col-lg-6 pr-lg-3 pr-md-3 mt-md-2 mt-2">
                                        <select class="form-control form-control-sm" id="sub_tipo" name="sub_tipo">
                                            <option disabled="" selected="" value="0">seleccionar...</option>
                                            <option value="Acreditacion de derechos">Acreditación de derechos</option>
                                            <option value="Autorizaciones">Autorizaciones</option>
                                            <option value="Asignacion de citas">Asignación de citas</option>
                                            <option value="Cirugia">Cirugía</option>
                                            <option value="Consulta externa">Consulta externa</option>
                                            <option value="Farmacia">Farmacia</option>
                                            <option value="Hospitalizacion">Hospitalización</option>
                                            <option value="Informacion">Información</option>
                                            <option value="Insumos y suministros">Insumos y suministros</option>
                                            <option value="Medios diagnosticos">Medios diagnósticos</option>
                                            <option value="Oportunidad de citas">Oportunidad de citas</option>
                                            <option value="Odontologia">Odontología</option>
                                            <option value="Promocion y prevencion">Promoción y prevención</option>
                                            <option value="Referencia y contrarreferencia">Referencia y
                                                contrarreferencia
                                            </option>
                                            <option value="Rembolso">Rembolso</option>
                                            <option value="Salud Ocupacional">Salud Ocupacional</option>
                                            <option value="Terapias">Terapias</option>
                                            <option value="Tramites administrativos">Trámites administrativos</option>
                                            <option value="Urgencias">Urgencias</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mt-2">
                                <label for="" class=""
                                       style="text-align: center;width: 240px;margin-top: 20px;padding: 4px 22px 4px 22px;border-radius: 5px;">Descripción
                                    de la solicitud</label>
                                <textarea name="descripcion" id="descripcion" class="form-control form-control-sm"
                                          style="width:100%;max-width:100%;min-height:100px" cols=""
                                          rows="5"></textarea>
                            </div>
                        </div>

                        <div class="row margen">
                            <div class="col-md-12">
                                <h6 class="sub__title">ANEXO</h6>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" style="margin:0px">
                                    <span class="badge" style="font-size: 11px;background-color: #7cbde3;color:#fff;">permitidos archivos PDF, im&aacute;genes con extensi&oacute;n JPG, PNG, JPEG</span>
                                    <div class="custom-file">
                                        <input name="archivos[]" id="archivos" type="file" class="file" multiple   accept="application/pdf, image/png, image/jpg, image/jpeg"
                                               data-show-upload="false" data-show-caption="true"
                                               data-msg-placeholder="Selecciona los archivos..."/>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row margen justify-content-md-center">
                            <div class="col-md-2 p-0 ">
                                <button type="button" id="btn_Solicitar" class="btn btn-block" onclick="guardar()">
                                    SOLICITAR
                                </button>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-md-12" style="text-align: center; font-size: 15px;">
                                <spa>Esta Informacion no cambia ni actualiza su estado de afiliacion</span><br>
                                    <spa>© Sisma corp.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    </body>
    <script type="text/javascript" src="Js/jquery.1.9.1.min.js"></script>
    <script type="text/javascript" src="Js/help.js"></script>
    <script type="text/javascript" src="Js/js.js"></script>
    <script type="text/javascript" src="Js/MyFunciones.js"></script>
    <script type="text/javascript" src="Js/functions.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script type="text/javascript" src="Js/librerias/moment.js"></script>
    <script type="text/javascript" src="Js/librerias/tempusdominus-bootstrap-4.min.js"></script>
    <link rel="stylesheet" href="Css/librerias/tempusdominus-bootstrap-4.min.css"/>
    <script type="text/javascript" src="Js/librerias/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="Js/librerias/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        function mayus(e) {
            e.value = e.value.toUpperCase();
        }
    </script>


    <script>
        function modificar() {

            if ($('#df').val() == 'SI') {
                $('#cualdd').removeClass('d-none')

            } else {
                $('#cualdd').addClass('d-none')
            }

        }
    </script>

<script>
    $(document).ready(function () {
        $('input[type="file"]').on('change', function(){
            var ext = $( this ).val().split('.').pop();
            if ($( this ).val() != '') {
                if(ext == "pdf" || ext == "jpg" || ext == 'png' || ext == 'jpeg'){
                    if($(this)[0].files[0].size > 3276800){
                        alertify.error("El documento excede el tamaño máximo de 25MB");
                        $('#modal-title').text('¡Precaución!');
                        $('#modal-msg').html("Se solicita un archivo no mayor a 1MB. Por favor verifica.");
                        $("#modal-gral").modal();
                        $(this).val('');
                    }else{
                        $("#modal-gral").hide();
                    }
                }
                else
                {
                    $( this ).val('');
                    alertify.error("Extensión no permitida: " + ext);
                }
            }
        });

        const number = document.querySelector('.number');

    });
</script>

    </html>
<?php include("_footer.php"); 