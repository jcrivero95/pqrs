<?php
@session_start();
extract($_REQUEST);
include_once("../Model/Model.php");
include_once("../Model/WebModel.php");
require_once('/mpdf/mpdf.php');
$model = new Model();
$wmodel = new WebModel();
header('Content-Type: text/html; charset=UTF-8');

$html = '';
$mpdf = new Mpdf($mode = '', 
$format = 'letter', 
$default_font_size = 0,
$default_font = '', 
$mgl = 2, $mgr = 2, $mgt = 3, 
$mgb = 16, $mgh = 2, $mgf = 2, 
$orientation = 'L');


ob_start();
$impresa=$model->getDato("impresa","recetas","numero='".$_REQUEST["numero"]."'");
$imprimeOriginalCopia=$model->getParametroGeneral("imprimeOriginalCopiaReceta","CONFIGURACION");

if($impresa=="0"||$imprimeOriginalCopia!='S'){
    $mpdf->showWatermarkImage = false;
    include("reporte_datos_recetas_body.php");
    $html = ob_get_contents();
    $mpdf->WriteHTML($html);
}
if($imprimeOriginalCopia=='S'){
    $mpdf->AddPage();
    $mpdf->SetWatermarkImage($_SESSION["site_name_portal"].'/Imagenes/copia2.png',1,'',array(0,0));
    $mpdf->watermarkImageAlpha = 0.2;
    $mpdf->showWatermarkImage = true;
    include("reporte_datos_recetas_body.php");
    $html = ob_get_contents();
    $mpdf->WriteHTML($html);
}

ob_end_clean();

if($visualizar == 0){
	// $mpdf->Output($_SESSION["root_portal"].'/Reportes/Adjuntos/'.$archivoAdjunto, 'F');
	$mpdf->Output($rutaAdjunto, 'F');
}else{
    $mpdf->SetTitle('Formula Medica');
	$mpdf->Output();
}
//exit;
// echo $html;
?>