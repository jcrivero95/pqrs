<span><strong>DATOS DEL PACIENTE</span>
<?php
	$datos = $model->datosPaciente(0, 0, "*", $estudio);
	$empresa = $model->getDato("e.nombre", "sis_maes AS m, sis_empre AS e", "e.codigo = m.cod_entidad AND m.con_estudio = $estudio");
	$estrato = $model->getDato("e.nombre", "sis_maes AS m, sis_estrato AS e", "e.codigo = m.cod_clasi AND m.con_estudio = $estudio");
	$num_ingreso = $model->getDato("id_ingreso","ingresos_deta","estudio = $estudio");
  $cama = $model->getDato("pabellon + ' - ' + nombre", "infoCensoView", "ingreso='{$num_ingreso}'");
?>


<table border="0" cellpadding="0" cellspacing="0">
 
  <tr>
    <td width="120"><strong>Identificaci&oacute;n:</strong></td>
    <td width="150"><strong><?php echo ((trim($datos['TipoId'])!='')?$datos['TipoId']:$datos['tipo_id']).' - '.((trim($datos['NumId'])!='')?$datos['NumId']:$datos['num_id']); ?></strong></td>
    <td width="120"><strong>Paciente:</strong></td>
    <td nowrap="nowrap"><strong><?php echo "$hijo {$datos['primer_ape']} {$datos['segundo_ape']} {$datos['primer_nom']} {$datos['segundo_nom']}"; 
	?></strong></td>
  </tr>
  <tr>	
 <td><strong>Direcci&oacute;n:</strong></td>
 <td colspan="3"><?php echo $datos['direccion']; ?></td>
 </tr>

 <tr>	
   <td><strong>Fecha Naci:</strong></td>
   <td><?php echo substr($datos['fecha_naci'],0,10);?></td>
    <td><strong>Edad:</strong></td>
    <td ><strong><?php 
    		echo $model->getDato("dbo.fnEdadAproximadaAtencion('".$datos['fecha_naci']."',convert(date,isnull(fecha_ing, GETDATE())))","hcingres","con_estudio = $estudio");
     ?></strong></td>
<td><strong>Sexo:</td>
    <td><?php 
     switch ($datos['sexo']) {
      case 'F':
        $sexo = 'Femenino';
        break;

      case 'M':
        $sexo = 'Masculino';
        break;
      
      default:
        # code...
        break;
    }
    echo $sexo; 
    ?></td>

 </tr>
  <!-- <tr>
  	<td><strong>Fecha Nacimiento:</td>
    <td><?php echo substr($datos['fecha_naci'],0,10);?></td>
  </tr> -->
  <tr>
    <td><strong>Fecha Ingreso: </td>
    <td><?php echo convertirFecha($datos['fecha_ing']); ?></td>
    <td><strong>Num. de Ingreso:</td>
    <td><?php echo $num_ingreso; ?></td>
    <td><strong>Fecha Egreso: </td>
    <td width="101"><?php echo convertirFecha($datos['fecha_egr']); ?></td>
   
  </tr>
  <tr>
  <td><strong>Telefono:</strong></td>
  <td><?php echo $datos['telefono']; ?></td>
    <td><strong>Estrato:</strong></td>
    <td><?php echo $estrato; ?></td>
	<!-- <td width="80" colspan="2"> <strong> Municipio:</strong></td>
  <td  ><?php echo $municipio; ?></td> -->
</tr>
  <?php if(trim($cama) != ''): ?>
  <tr>  
    <td><strong>Cama:</td>
    <td colspan="2"><?php echo $cama; ?></td>    
  </tr>
  <?php endif; ?>
</table>
