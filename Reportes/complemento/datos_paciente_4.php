<span>Datos del Paciente</span>
<?php
	global $datos;
	$datos = $model->datosPaciente(0, 0, "*,m.fecha_ing as FechaIngresoAdmision,m.hora_ing as HoraIngresoAdmision,convert(date,fecha_naci) as fec_naci", $estudio);
	$rs_ = $model->select("e.nombre as empresa,smuni.nombre AS municipio,sp.cargo,con.nombre as NombreContrato",
        "sis_maes AS m, sis_empre AS e,dbo.pacientesView sp ,sis_muni smuni,contratos con",
        "e.codigo = m.cod_entidad AND m.con_estudio = $estudio AND sp.autoid=m.autoid AND smuni.id_dep=sp.cod_dep
AND smuni.codigo=sp.cod_muni and con.codigo=m.contrato");
	$row=$model->nextRow($rs_);
	$empresa=$row["empresa"];
	$NombreContrato=$row["NombreContrato"];
	$municipio=$row["municipio"];
	$cargo=$row["cargo"];
	$celular = $datos["celular"];  
	$estrato = $model->getDato("e.nombre", "sis_maes AS m, sis_estrato AS e", "e.codigo = m.cod_clasi AND m.con_estudio = $estudio");
	$ingreso = $model->getDato("id_ingreso","ingresos_deta","estudio = $estudio");
	$hora = $datos["HoraIngresoAdmision"];//$model->getDato("hora_ing","hcingres","con_estudio = $estudio");
	$horaAten = $model->getDato("fecha_atencion+' '+hora_atencion","hcingres","con_estudio = $estudio");
	$hijo = $model->getDato("hijode","ordenes","num_servicio = (select top 1 num_servicio from sis_deta_temp where id=".$_REQUEST["id"].")");
	$acompanante = $model->getDato("acompanante", "hcingres", "con_estudio = $estudio");
	$telefonoAcompanante = $model->getDato("telefono_acompanante", "hcingres", "con_estudio = $estudio");
	
	$nombrePaciente= "$hijo {$datos['tipo_id']} - {$datos['num_id']}  {$datos['primer_ape']} {$datos['segundo_ape']} {$datos['primer_nom']} {$datos['segundo_nom']}";


 $FechaHoraResultado=$model->getDato("fecha_estado_res","sis_deta_temp","id=".$_REQUEST["id"]);
 
?>
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="120"><strong>Identificaci&oacute;n:</strong></td>
    <td width="150"><strong><?php echo ((trim($datos['TipoId'])!='')?$datos['TipoId']:$datos['tipo_id']).' - '.((trim($datos['NumId'])!='')?$datos['NumId']:$datos['num_id']); ?></strong></td>
    <td width="120"><strong>Paciente:</strong></td>
    <td nowrap="nowrap"><strong><?php echo "$hijo {$datos['primer_ape']} {$datos['segundo_ape']} {$datos['primer_nom']} {$datos['segundo_nom']}"; 
	?></strong></td>
  </tr>
  
  <tr>
    <td><strong>Fecha Ingreso:</strong> </td>
    <td><?php echo convertirFecha($datos['FechaIngresoAdmision']); ?></td>
    <td ><strong>Hora Ing:</strong></td>
    <td><?php echo $hora ?></td>
	<td width="60" colspan="2"><strong>Ingreso:</strong></td>
	<?php 
		if(empty($ingreso) || is_null($ingreso)){
			$ingreso = 0;
		}
		?>
    <td ><?php echo $ingreso == 0 ? "" : $ingreso; ?></td>
	
</tr>
<tr>
	<td colspan="4"><strong>Fecha y Hora Atencion: </strong><?php echo ((trim($horaAten)=='')?$FechaHoraResultado:$horaAten);?></td>
</tr>
<tr>	
   <td><strong>Fecha Naci:</strong></td>
    <td ><strong><?php echo $datos['fec_naci']; ?></strong></td>
    <td><strong>Edad:</strong></td>
    <td ><strong><?php
    $rs_ = $model->RSAsociativo("select dbo.fnEdadAproximada('".$datos['fec_naci']."') as edad");
    $res = $rs_[0];
		echo $res['edad'];
    //echo $model->getDato("dbo.fnEdadAproximadaAtencion('".$datos['fec_naci']."',convert(date,isnull(fecha_estado_res, GETDATE())))","hcingres","con_estudio = $estudio");
     ?></strong></td>
	<td colspan="2">  <strong>Sexo:</strong></td>
    <td ><?php echo $datos['sexo']; ?></td>
	
	
	
	
 </tr>


<tr>
  <td><strong>Telefono:</strong></td>
  <td><?php echo $datos['telefono']; ?></td>
    <td><strong>Estrato:</strong></td>
    <td><?php echo $estrato; ?></td>
	<td width="80" colspan="2"> <strong> Municipio:</strong></td>
  <td  ><?php echo $municipio; ?></td>
</tr>
<tr>	
 <td><strong>Direcci&oacute;n:</strong></td>
 <td colspan="3"><?php echo $datos['direccion']; ?></td>
 <!--<td width="80" colspan="2">  Celular:</td>
  <td  ><strong><?php echo $celular; ?></strong></td>-->
 </tr>
 <?php if($model->getParametroGeneral('EmpresaEnDescripcionQx', 'HISTORIA CLINICA') == 'S'): ?>
<tr>
 <td><strong>Empresa:</strong></td>
 <td  colspan="6"><strong><?php echo $empresa; ?></strong></td>  
<?php endif; ?>   
  </tr>
  <tr>
	<td><strong>Contrato:</strong></td>
	<td colspan="6"><strong><?php echo $NombreContrato; ?></strong></td>
  </tr>
  <!--<tr>
	<td><strong>Cargo:</strong></td>
	<td colspan="2"><?php echo $cargo; ?></td>
  </tr>-->
  <?php if($acompanante != ""){?>
  <tr>
  	<td><strong>Acompa&ntilde;ante:</strong></td>
    <td colspan="2"><strong><?php echo $acompanante; ?></strong></td>  
    <td colspan="2"><strong>Tel. Acompa&ntilde;ante: </strong><strong style="margin-left:6px"><?php echo $telefonoAcompanante; ?></strong></td>  
  </tr>
  <?php }?>
  <?php if(trim($datos['nom_responsable'])!=''){?>
  <tr>
  	<td><strong>Responsable:</strong></td>
    <td><?php echo $datos['nom_responsable'].' '.$datos["ape_responsable"];?></td>
  </tr>
  <tr>
  	<td><strong>Parentesco:</strong></td>
    <td><?php echo $datos['parentesco_responsable'];?></td>
 
  	<td><strong>Tel. Responsable:</strong></td>
    <td><?php echo $datos['telefono_responsable'];?></td>
  </tr>
  <tr>
  	<td><strong>Direccion:</strong></td>
    <td><?php echo $datos['direccion_responsable'];?></td>
  </tr>
  <?php }?>
  <td>&nbsp;</td>
</table>
