<?php
@session_start();
require_once("../template.php");
$id = $_REQUEST['id'];
encabezado("Resultados", "Resultados del Examen de '".
	$model->getDato("a.nombre", 
		"sis_ayudadx AS a, detalles_extra AS d", 
		"d.id_detalle = $id AND d.codigo = a.codigo")."'");

$cod_ayudas_dx = $model->getDato("a.codigo", "sis_ayudadx AS a, detalles_extra AS d", "d.id_detalle = $id AND d.codigo = a.codigo");
	
require_once("../complemento/datos_paciente_4.php"); 

?>
<br/>
<table>
  <tr>
    <td> <strong>Fecha Toma de Muestra : <?php echo convertirFecha($model->getDato("ordenes_detalle.fecha_iniciado","ordenes INNER JOIN
                      ordenes_detalle ON ordenes.numero = ordenes_detalle.numero_orden INNER JOIN
                      sis_deta ON ordenes.num_servicio = sis_deta.num_servicio","sis_deta.id = $id")); ?> </strong></td>

    <td> <strong>Hora Toma de Muestra : <?php echo $model->getDato("ordenes_detalle.hora_iniciado","ordenes INNER JOIN
                      ordenes_detalle ON ordenes.numero = ordenes_detalle.numero_orden INNER JOIN
                      sis_deta ON ordenes.num_servicio = sis_deta.num_servicio","sis_deta.id = $id"); ?> </strong></td>
  </tr>

  <tr>
    <td> <strong>Fecha Realizacion : <?php echo convertirFecha($model->getDato("ordenes_detalle.fecha_finalizado","ordenes INNER JOIN
                      ordenes_detalle ON ordenes.numero = ordenes_detalle.numero_orden INNER JOIN
                      sis_deta ON ordenes.num_servicio = sis_deta.num_servicio","sis_deta.id = $id")); ?> </strong></td>

    <td> <strong>Hora Realizacion : <?php echo $model->getDato("ordenes_detalle.hora_finalizado","ordenes INNER JOIN
                      ordenes_detalle ON ordenes.numero = ordenes_detalle.numero_orden INNER JOIN
                      sis_deta ON ordenes.num_servicio = sis_deta.num_servicio","sis_deta.id = $id"); ?> </strong></td>
  </tr>
  
</table>
<?php 
	$rs=$model->select("top 1 a.nombre,ISNULL(st.descripcion,'Referencia') as referencia", 
		"sis_ayudadx AS a LEFT JOIN servicio_title_ref st ON st.cod_servicio=a.codigo, detalles_extra AS d", 
		"d.id_detalle = $id AND d.codigo = a.codigo");
		
	$row=$model->nextRow($rs);
	$nombreServicio=$row["nombre"];
	$tituloRef=$row["referencia"];
?>
<table>
  <tr>
    <td> Servicio: <strong> <?php echo $nombreServicio;?></strong></td>
  </tr>  
</table>

<?php

$tipo_memo = $model->getDato("valor", "sismaelm", "tabla = 'GRL' AND tipo = 'TIPOD' AND nombre = 'Memo'");


$tabla = "sis_deta_temp";
$rs = $model->select(
	"s.campo, d.resultado, s.observacion, s.nomgrupo, s.grupo,
	(SELECT TOP 1 usuario_estado_res FROM {$tabla} AS dt WHERE dt.id = d.id_detalle) AS cedula", 
	"servicios_titulos AS s, detalles_extra AS d", 
	"d.id_detalle = $id AND d.codigo = s.codigo AND d.posicion = s.posicion AND s.tipo = $tipo_memo AND d.codigo = '{$cod_ayudas_dx}' and dbo.fnTrim(d.resultado) <> ''
	ORDER BY s.posicion,s.subposicion");



if ($model->numRows($rs) > 0) {
?>
<div class="block">
<table  style="width:100%">
<?php
	while (($row = $model->nextRow($rs))) {
		if(is_null($cedula)) $cedula = $row['cedula'];
		$color = $i++ % 2 == 0 ? '#F5F5F5' : '#FFFFFF';
		$texto_mostrar = str_replace("\r\n", "<br>", $row[1]);
		$ref=$row[2];
?>
  <?php if (empty($ref) || $ref == ' ') { ?>
  <tr><td colspan="3">
  <table  class="border" style="width:100%">
  <tr>
    <td class="head" colspan="3"><?php echo $row[0]; ?></td>
  </tr>
  <tr>
    <td class="row2" colspan="3"><br/><?php echo $texto_mostrar?><br/></td>
  </tr>
  </table>
  </td>
  </tr>
  
    <?php } else {
    		if ($ant != $row[4]) {
			$ant = $row[4];
			echo "<tr><td class='head' colspan='3' align='center'><strong>{$row[3]}</strong></td></tr>";
		}
	?>
   <tr>
   <td colspan="3"><table  class="border" style="width:100%">
   
    <td class="row2" width=30%><strong><br/><?php echo $row[0]; ?></strong></td>
    <td class="row2" width=50%><br/><?php echo $texto_mostrar?></td>
    <td class="row2" width=20%><br/><?php echo $ref; ?></td>
    </table>
    </td>
   </tr>
    <?php } ?>
<?php
	}

?>
</table>
</div>
<?php
}
$tabla = "sis_deta_temp";
$rs = $model->select(
	"s.campo, d.resultado, s.observacion, s.nomgrupo, s.grupo, 
	(SELECT TOP 1 usuario_estado_res FROM {$tabla} AS dt WHERE dt.id = d.id_detalle) AS cedula", 
	"servicios_titulos AS s, detalles_extra AS d", 
	"d.id_detalle = $id AND d.codigo = s.codigo AND d.posicion = s.posicion AND s.tipo <> $tipo_memo AND d.codigo = '{$cod_ayudas_dx}' and dbo.fnTrim(d.resultado) <> ''
	ORDER BY s.posicion,s.subposicion");

if ($model->numRows($rs) > 0) {

?>


<div class="block">
  <table class="border" style="width:100%">
  <tr>
    <td width="350" class="head">DESCRIPCI&Oacute;N</td>
	<td width="150" class="head">VALOR</td>
	<td class="head"><?php echo strtoupper($tituloRef);?></td>
	</tr>
<?php
	$i = 1;
	$cedula = NULL;

	while (($row = $model->nextRow($rs))) {
		$color = $i++ % 2 == 0 ? '#F5F5F5' : '#FFFFFF';
		if(is_null($cedula)) $cedula = $row['cedula'];
		if ($ant != $row[4]) {
			$ant = $row[4];
			echo "<tr><td colspan='3' align='center'><strong>{$row[3]}</strong></td></tr>";
		}
?>
    <tr bgcolor='<?php echo $color; ?>' style='cursor:hand' onMouseOver="bgColor='#f4f4f4'" onMouseOut="bgColor='<?php echo $color; ?>'">
      <td class="row2"><?php echo $row[0]; ?></td>
      <td class="row"><?php echo  nl2br($row[1]); ?></td>
      <td class="row"><?php echo  nl2br($row[2]); ?></td>
    </tr>
    <?php
	}
}
?>
</table>
</div>

<table width="47%" class="noborder">
  <tr>
		<td width="45%" style="border-bottom: #000000 2px solid" class="row">
        	<?php 
				include_once("{$_SESSION['site_name_portal']}/Model/WebModel.php");
				$wmodel = new WebModel();
				$firma = $wmodel->getFirmaMedico($cedula,267,63);
				echo empty($firma)? "&nbsp;" : $firma;
			?>
        </td>    
    <td>&nbsp;</td>
  </tr>
  <tr>
  <?php  
	$nombre_med = $model->getDato( "s.nombre +' Reg Medico :'+ s.registro", "sis_medi AS s", "s.cedula = $cedula" );
  ?>
    <td class="row">ELABORADO POR </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="row"><b><?php echo $nombre_med ?></b></td>
    <td>&nbsp;</td>
  </tr>
</table>
<?php


pie();
?>