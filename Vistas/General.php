<?php

	if (!empty($_GET)) {
		while(list($name, $value) = each($_GET)) {
			$$name = $value;
		}
	}

	/*if (!empty($_SESSION)) {
		while(list($name, $value) = each($_SESSION)) {
			$$name = $value;
		}
	}*/

	if (!empty($_POST)) {
		while(list($name, $value) = each($_POST)) {
			$$name = $value;
		}
	}
	
	if (!empty($_FILES)) {
		while(list($name, $value) = each($_FILES)) {
			$nom=$name."_name";
			$$nom=$_FILES[$name]['name'];
			$nom=$name."_type";
			$$nom=$_FILES[$name]['type'];
			$nom=$name."_size";
			$$nom=$_FILES[$name]['size'];
			$nom=$name."_tmp_name";
			$$nom=$_FILES[$name]['tmp_name'];
		}
	}

function fecha_hora_actual()
{
	$fecha_actual=getdate();
	$anno_ac=$fecha_actual["year"];
	$mes_ac=$fecha_actual["mon"];
	$dia_ac=$fecha_actual["mday"];
	$horas_ac=$fecha_actual["hours"];
	if (strlen($horas_ac) < 2)
		$horas_ac="0".$horas_ac;
	$minutos_ac=$fecha_actual["minutes"];
	if (strlen($minutos_ac) < 2)
		$minutos_ac="0".$minutos_ac;
	$segundos_ac=$fecha_actual["seconds"];
	if (strlen($segundos_ac) < 2)
		$segundos_ac="0".$segundos_ac;
	if ($dia_ac < 10)
		$dia_ac="0".$dia_ac;
	if ($mes_ac < 10)
		$mes_ac="0".$mes_ac;
	//$fecha_ac=$anno_ac."-".$mes_ac."-".$dia_ac ." ".$horas_ac.":".$minutos_ac.":".$segundos_ac;
	$fecha_ac=$anno_ac."/".$mes_ac."/".$dia_ac ." ".$horas_ac.":".$minutos_ac;
	return $fecha_ac;
}

function Fecha_Hoy()
{
	$fecha_actual=getdate();
	$anno_ac=$fecha_actual["year"];
	$mes_ac=$fecha_actual["mon"];
	$dia_ac=$fecha_actual["mday"];
	$horas_ac=$fecha_actual["hours"];
	if (strlen($horas_ac) < 2)
		$horas_ac="0".$horas_ac;
	$minutos_ac=$fecha_actual["minutes"];
	if (strlen($minutos_ac) < 2)
		$minutos_ac="0".$minutos_ac;
	$segundos_ac=$fecha_actual["seconds"];
	if (strlen($segundos_ac) < 2)
		$segundos_ac="0".$segundos_ac;
	if ($dia_ac < 10)
		$dia_ac="0".$dia_ac;
	if ($mes_ac < 10)
		$mes_ac="0".$mes_ac;
	$fecha_ac=$anno_ac."/".$mes_ac."/".$dia_ac;
	return $fecha_ac;
}

function fecha_actual()
 {
	$a_fecha=getdate();
	$meses=array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$mes=$a_fecha['mon'];
	$mes_t=$meses[$mes-1];
	$dia=$a_fecha['mday'];
	$anno=$a_fecha['year'];
	$fecha="$mes_t, $dia de $anno";
	return $fecha;
}

class GET_PHP{
	private $params;
	private $keys;
	
	function __construct(){
		$this->params = array();
		$this->keys = array();
	}
	
	function addParam($key, $valor){		
		if (array_search($key, $this->keys) === FALSE) {
			$k = sizeof($this->params);	
			$this->params[$k] = array("key" => $key, "valor" => $valor);
			$this->keys[$k] = $key;
		}
	}
	
	function addArrayToParams($arrayAdd){
		foreach($arrayAdd as $k => $v){
			$this->addParam($k, $v);
		}
	}
	
	function paramsToUrl(){
		$str = "";
		$flag = true;
		foreach($this->params as $k => $p){
			$str .= ($flag? "" : "&") .$p['key'] ."=" .$p["valor"];
			
			if($flag)
				$flag = false;
		}
		return $str;
	}
	
	function resetRequest($excluir = ""){
		$_excluir = explode(",", $excluir);
		foreach($_REQUEST as $k => $param){
			if(strtoupper($k) != "PHPSESSID"){
				unset($_REQUEST[$k]);
			}
		}
	}
	
	function getParams(){
		return $this->params;
	}
	
	protected function setRequestParams($excluir = ""){
		$excluirA = array();
		if(trim($excluir) !== ''){
			$excluir = str_replace(" ", "", $excluir);
			$excluirA = explode(",", $excluir);
		}
		$size = sizeof($excluirA);
		
		foreach($_REQUEST as $k => $param){
			if($size > 0){
				if(array_search($k, $excluirA) === FALSE && strtoupper($k) != "PHPSESSID"){
					$this->addParam($k, $param);
				}
			}else{
				if(strtoupper($k) != "PHPSESSID"){
					$this->addParam($k, $param);
				}
			}
		}
	}
	
	function forward($next, $excluir = "", $show = false){
		$excluirA = array();		
		if(trim($excluir) !== ''){
			$excluir = str_replace(" ", "", $excluir);
			$excluirA = explode(",", $excluir);
		}
		$size = sizeof($excluirA);
		//print_r($excluirA);
		foreach($_REQUEST as $k => $param){
			if($size > 0){
				if(array_search($k, $excluirA) === FALSE && strtoupper($k) != "PHPSESSID"){
					$this->addParam($k, $param);
				}
			}else{				
				if(strtoupper($k) != "PHPSESSID"){
					$this->addParam($k, $param);
				}
			}
		}
		
		$next .= (strpos($next, "?") !== false? $this->paramsToUrl() : "?" .$this->paramsToUrl());
		if(!$show){
			header("Location: $next");
		}else{
			echo $next;
		}
	}
} 

class POST_PHP_V2 extends GET_PHP{
	
	function __construct(){
		parent::__construct();
	}
	
	function forward($next, $excluir = "", $show = false){
		$this->setRequestParams($excluir);
		$params = $this->getParams();
		$str = "";
		
		foreach($params as $k => $p){
			$str .= "<input name=\"{$p['key']}\" type=\"hidden\" value=\"{$p['valor']}\" />";
		}
		
		$html = 
			"<html>".
			"<head>".
			"<script>".
			"function post_forward() {".
				(!$show ? "document.getElementById(\"post_form\").submit();" : "").
			"}".
			"</script>".
			"</head>".
			"<body onload=\"post_forward()\" class=\"ColorFondoSistema\">".
			"<form id=\"post_form\" name=\"post_form\" method=\"post\" action=\"$next\">".
			"$str".
			"</form>".
			"</body>".
			"</html>";
		print $html;
	}
}

class PHP_JSON_AJAX extends GET_PHP{
	
	private $_utf8_encode;
	
	function __construct(){
		parent::__construct();
		$this->_utf8_encode = true;
	}
	
	function utf8_encodeOff(){
		$this->_utf8_encode = false;
	}
	
	function forward($next, $excluir = "", $show = false){
		return "";
	}
	
	function encode($print = true){
		$params = $this->getParams();
		$data = array();
		
		foreach($params as $k => $param){
	
			if($this->_utf8_encode){				
				if(is_array($param["valor"])){
					$param["valor"] = $this->utf8_encode_array($param["valor"]);
				}
			}
			
			if($this->_utf8_encode){
				$data[$param["key"]] = $this->utf8_encode($param["valor"]);
			}else{
				$data[$param["key"]] = $param["valor"];
			}
		}
		
		$jsonData = json_encode( $data );
		
		if ($print) {
			header("Content-Type:application/json");
			die( $jsonData );
		} else {
			return $jsonData;
		}		
	}
	
	function utf8_encode_array(array $target){
		$new = array();
		foreach($target as $k => $value){
			if(is_array($value)){
				$new[$k] = $this->utf8_encode_array($value);
			}else{				
				$new[$k] = $this->utf8_encode($value);								
			}
		}
		
		return $new;
	}

	public function utf8_encode($value)
	{
		if (is_string($value)) {
			$utf8 = mb_detect_encoding($value, 'UTF-8, ISO-8859-1', true) === 'UTF-8' ? true : false;
			return ($utf8 ? $value : utf8_encode($value));
		} else {
			return $value;
		}
	}
	
	function setEstadoTransaccion($estado){
		$this->addParam("statusTran", $estado);
	}
	
	function setInfoTransaccion($info){
		$this->addParam("infoTran", $info);
	}
}

class Autoloader
{
	
	private $ROOT_PATH;
	
	public $showPath;
	
	private $directory_name;

	public function __construct($directory_name)
	{
		if(!isset($this->showPath)){
			$this->showPath = false;
		}
		
		$this->directory_name = $directory_name;
		$this->ROOT_PATH = str_replace('Vistas', 'Model\\', __DIR__);
				
	}
	
	private function _include($path){
		
		if($this->showPath){
			echo $path ."<br />";
		}
		
		if(file_exists($path)){
			include_once $path;
			return true;
		}else{
			return false;
		}
	}
	
	public function autoLoad($class_name){
		if(stripos($class_name, 'Factory') !== FALSE){
			$this->_include( $this->factories($class_name) );
		}else if(stripos($class_name, 'Interface') !== FALSE){
			$this->_include( $this->interfaces($class_name) );
		}else if(stripos($class_name, 'Service') !== FALSE){
			$this->_include( $this->services($class_name) );
		}
	}

	protected function services($class_name) 
	{ 
		$file_name = $class_name.'.class.php';

		$file = $this->ROOT_PATH .$this->directory_name .'\\'.$file_name;

		return ($file);
	}
	
	protected function interfaces($class_name){
		$file_name_1 = $class_name.'.class.php';
		$file_name_2 = $class_name.'.php';

		$file = $this->ROOT_PATH .$this->directory_name .'\\Interfaces\\' .$file_name_1;
		
		if(!file_exists($file)){
			$file = $this->ROOT_PATH .$this->directory_name .'\\Interfaces\\' .$file_name_2;
		}
		
		return ($file);
	}
	
	protected function factories($class_name){
		$file_name = $class_name.'.class.php';

		$file = $this->ROOT_PATH .$this->directory_name .'\\Factories\\' .$file_name;

		return ($file);
	}
	
	public function register(){
		$f = 'autoLoad';
		spl_autoload_register(array($this, $f));
	}
}

?>
